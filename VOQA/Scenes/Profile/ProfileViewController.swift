//
//  ProfileViewController.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import ALCameraViewController
import SDWebImage
import TCPickerView
class ProfileViewController: UIViewController,TCPickerViewDelegate{
    func pickerView(_ pickerView: TCPickerView, didSelectRowAtIndex index: Int) {
        if index == 0 {
            UserDefaults.standard.set("EN", forKey: "LANGUAGE")
            Constants.Settings.lang = "EN"
            UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: "homeTabBarController")

        }
        else
        {
            UserDefaults.standard.set("AR", forKey: "LANGUAGE")
            Constants.Settings.lang = "AR"
            UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: "homeTabBarController")

        }
    }
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!

    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var titleLbl: UILabel!

    @IBOutlet weak var changeLanguage: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var contactUsBtn: UIButton!
    @IBOutlet weak var privacyBtn: UIButton!

    let loadingHud = LoadingHud.shared.loadingHud
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.semanticContentAttribute = .forceRightToLeft
  
        // Do any additional setup after loading the view.
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectPhoto)))
    }

    func getUserImg(image: UIImage) -> String {
        let jpegCompressionQuality: CGFloat = 0.9 // Set this to whatever suits your purpose
        let correctedImage:UIImage = profileImage.image!.fixOrientation()
        if let base64String = UIImageJPEGRepresentation(correctedImage, jpegCompressionQuality)?.base64EncodedString() {
            return base64String
        }
        return ""
    }
    
    @objc func selectPhoto() {
        let cropSettings = CroppingParameters(isEnabled: true, allowResizing: false, allowMoving: true, minimumSize: CGSize(width: 200, height: 350))
        
        let cameraViewController = CameraViewController(croppingParameters: cropSettings, allowsLibraryAccess: true, allowsSwapCameraOrientation: true, allowVolumeButtonCapture: true) { (image, phAsset) in
            
            if image != nil {
                self.profileImage.image = image!
                self.updateImage()
            }
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
        self.present(cameraViewController, animated: true, completion: nil)
    }
    
    func updateImage() {
        let imageBase64 = getUserImg(image: self.profileImage.image!)
        loadingHud.show(in: self.view)
        print("IMG \(imageBase64)")
        UserManager.updateUserImage(body: ["image": imageBase64]) { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                let alert = UIViewHelper.connectionErrorAlert()
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            if (response?.0)! {
                self.profileImage.sd_setImage(with: URL(string: (response?.1)!))
                let defaults = UserDefaults.standard
                defaults.set((response?.1)!, forKey: "UserImage")
                defaults.synchronize()
            } else {
                let alert = UIViewHelper.errorAlert(title: "Update Image Fail", message: "Update profile image failed. please try again later.")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "الملف الشخصي"
            self.changeLanguage.setTitle("تغيير اللغة", for: .normal)
            self.contactUsBtn.setTitle( "تواصل معنا", for: .normal)
            self.logoutBtn.setTitle( "الخروج", for: .normal)
            self.privacyBtn.setTitle("سياسة الخصوصية", for: .normal)

            contactUsBtn.contentHorizontalAlignment = .right
            privacyBtn.contentHorizontalAlignment = .right
            logoutBtn.contentHorizontalAlignment = .right
            changeLanguage.contentHorizontalAlignment = .right

            self.view1.semanticContentAttribute = .forceRightToLeft
            self.view2.semanticContentAttribute = .forceRightToLeft
            self.view3.semanticContentAttribute = .forceRightToLeft
            self.view4.semanticContentAttribute = .forceRightToLeft

            self.imageView1.semanticContentAttribute = .forceRightToLeft
            self.imageView1.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            
            self.imageView2.semanticContentAttribute = .forceRightToLeft
            self.imageView2.rotate(byAngle: 180, ofType: AngleUnit.degrees)

            self.changeLanguage.semanticContentAttribute = .forceRightToLeft
            for view in self.view.subviews
            {
                view.semanticContentAttribute = .forceRightToLeft
                
            }


        }
        else
        {
            self.titleLbl.text = "Profile"
            self.changeLanguage.setTitle("Change Language", for: .normal)
            self.contactUsBtn.setTitle("Contact Us", for: .normal)  
            self.logoutBtn.setTitle("Logout", for: .normal)
            self.privacyBtn.setTitle("Privacy Policy", for: .normal)

            
        }
        setupView()
    }
    
    private func setupView() {
        let prefs = UserDefaults.standard
        var keyValue = prefs.string(forKey:"UserName")
        profileName.text = keyValue
        keyValue = prefs.string(forKey: "UserImage")
        profileImage.sd_setImage(with: URL(string: keyValue ?? "" ), placeholderImage: UIImage(named: "user"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func contactUs(_ sender: UIButton) {
        let email = "Support@voqa.org"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            }
        }
    }
    @IBAction func changeLanguage(_ sender: UIButton) {
        let picker = TCPickerView()
        if Constants.Settings.lang == "AR"
        {
            picker.title = "تغيير اللغة"
            let languages = [
                
                                            "الانجليزي",
                        "العربية"
            ]
            picker.doneText = "موافق"
            picker.closeText = "الغاء"
            let values = languages.map { TCPickerView.Value(title: $0) }
            picker.values = values
            picker.delegate = self
            picker.selection = .single

            picker.show()

        }
        else
        {
            picker.title = "Change Language"
            let languages = [
                "English",
                "Arabic"
            ]
            let values = languages.map { TCPickerView.Value(title: $0) }
            picker.values = values
            picker.delegate = self
            picker.selection = .single
            picker.itemsFont = UIFont.systemFont(ofSize: 15, weight: .bold)
            picker.completion = { (selectedIndexes) in
                for i in selectedIndexes {
                    print(values[i].title)
                }
            }
            picker.show()
            
        }




    }
    @IBAction func privacyPolicy(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showPrivecy_Segue", sender: self)
    }
    
    @IBAction func logout(_ sender: UIButton) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"UserToken")
        prefs.removeObject(forKey:"UserName")
        prefs.removeObject(forKey:"UserPoints")
        prefs.removeObject(forKey:"UserID")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Authuntication", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "mainNavLogin") as! UINavigationController
        self.present(newViewController, animated: true, completion: {
            self.tabBarController?.view.removeFromSuperview()
        })
    }

}
