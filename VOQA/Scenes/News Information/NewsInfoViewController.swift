//
//  NewsInfoViewController.swift
//  Elusive
//
//  Created by Graphic on 3/14/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import SDWebImage

class NewsInfoViewController: UIViewController {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsTime: UILabel!
    @IBOutlet weak var articleBy: UILabel!
    @IBOutlet weak var newsDetails: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var backBtnConstraint: NSLayoutConstraint?

    var newsInfo: NewsObject!
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
    }
    override func viewDidLayoutSubviews() {
   
    }
    override func viewDidAppear(_ animated: Bool) {
        if Constants.Settings.lang == "AR"
        {
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "الاخبار"
            //  self.backBtn.frame.origin.x = 300
//            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)!
//            print("DDDDDDDD \(self.backBtnConstraint?.constant)")
//            //  backBtn.contentHorizontalAlignment = .right
//
//            //self.backBtn.semanticContentAttribute = .forceLeftToRight
            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)

        }
        else
        {
            self.titleLbl.text = "News Feeds"
            
            
        }
    }
    private func setupView() {
        newsImage.sd_setImage(with: URL(string: newsInfo.image!))
        newsTitle.text = newsInfo.title
        newsTime.text = newsInfo.updated_at
        newsDetails.text = newsInfo.description
        articleBy.text = "Article By: \(newsInfo.source ?? "-")"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK:- IBActions
extension NewsInfoViewController {
    
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionShare(_ sender: UIButton) {
        print("Share News")
    }
    
}
