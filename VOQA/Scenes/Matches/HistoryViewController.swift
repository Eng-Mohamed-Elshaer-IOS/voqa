//
//  HistoryViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/20/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NSURLConnectionDelegate{

    let loadingHud = LoadingHud.shared.loadingHud
    var matches = [MatchHistoryObjectt]()
    var matchesResult = [MatchResult]()

    @IBOutlet weak var matchesTableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "المباريات"
        }
        else
        {
            self.titleLbl.text = "Matches"
            
            
        }
        let defaults = UserDefaults.standard
        
        let userId = defaults.object(forKey: "UserID")

        let  userIdStr="\(userId!)"
        loadingHud.show(in: self.view)
        MatchesManager.getAllHistoryMatches(userId: userIdStr) { [unowned self] (matches, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()

            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                
                return
            }
            self.matches.removeAll()
            self.matches = (matches?.data)!
//            for match in self.matches
//            {
//                self.getMatchResult(matchId: match.matchId!){ code in
//                    print(self.matchesResult.count)
//                    self.matchesTableView.performSelector(onMainThread: Selector("reloadData"), with: nil, waitUntilDone: true)
//
//              //      self.matchesTableView.reloadData()
//                }
//
//            }
            print("MATCHES \(self.matches.count)")
            print("MATCHES RESULT \(self.matchesResult.count)")
            
            self.matchesTableView.reloadData()

        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.matches.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return CGFloat(80.0)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchCell", for: indexPath) as! MatchCell
        let object = matches[indexPath.row]

       // if matchesResult.count == matches.count {
       //     let objectResult = matchesResult[indexPath.row]

            if Constants.Settings.lang == "AR"
            {
                cell.expectLabel.text = "النتيجة المتوقعة"
            }
            else
            {
                cell.expectLabel.text = "Expected result"

                
            }
        cell.localTeamName.text = object.userMatchHistory?.localTeam

        cell.localTeamRealScore.text = "\(object.userMatchHistory?.localTeamScore! ?? 0)"

        cell.localTeamImage.sd_setImage(with: URL(string: (object.userMatchHistory?.localTeamLogo) ?? ""), placeholderImage: UIImage(named: "football"))
        cell.localTeamScore.text = "\(object.localTeamScore!)"
        
        cell.visitorTeamName.text = object.userMatchHistory?.visitorTeam
        cell.visitorTeamImage.sd_setImage(with: URL(string: (object.userMatchHistory?.visitorTeamLogo) ?? ""), placeholderImage: UIImage(named: "football"))

        cell.visitorTeamScore.text = "\(object.visitorTeamScore!)"
          //  let visitorTeamScoreStr =
        cell.visitorTeamRealScore.text  = "\(object.userMatchHistory?.visitorTeamScore! ?? 0)"

        
        return cell
    }
    func getMatchResult( matchId:Int,completion: @escaping (Int) -> ())
    {
      //  print("MATCHID \(matchId)  \(token!)")
        let urlPath = "https://soccer.sportmonks.com/api/v2.0/fixtures/\(matchId)?api_token=FydcKbWOx8fZ3YnbbBQMgAB93ddv7LsUjpEvT64wiGm4luXUpsH8Q2laEP7n&include=localTeam,visitorTeam"
        //   https://35.198.81.33/
        print("URL Path \(urlPath)")
        let url1: URL = URL(string: urlPath as String)!
        
        let request = NSMutableURLRequest(url:url1 )
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        let params = ["UserName":"Amr"]
        //            "rawModeData": "{\n\t\"Name\":\"Mahmoud \",\n\t\"Phone\":\"01120546016\",\n\t\"UserName\":\"Mahmoud\",\n\t\"Password\":\"123\",\n\t\"Email\":\"m.sheikh@panexcel.com\",\n\t\"Location\":\"10,10\",\n\t\"Nationality\":\"Egyptian\",\n\t\"DateOfBirth\":\"11-15-1993\",\n\t\"Education\":\"BSC\",\n\t\"Gender\":\"m\"\n\t\n}"
        
        do
        {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        }
        catch
        {}
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            let res = response as? HTTPURLResponse
            
//            print("Response: \(res!.statusCode)")
//            print("Data: \(String(describing: data))")
            
            if res!.statusCode == 200
            {
                do
                {
                    var object = MatchResult()
                    let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                    print("Result ",result!)
                    let dataDict = result!["data"]!
                    
                    let localTeamDict = dataDict["localTeam"]!  as! Dictionary<String, AnyObject>
                    let localTeamDataDict = localTeamDict["data"]!  as! Dictionary<String, AnyObject>
                    let localTeamName = localTeamDataDict["name"]! as! String
                    // let localTeamLogo = localTeamDataDict["logo_path"]! as! String
                    var localTeamLogo = ""
                    if  localTeamDataDict["logo_path"]! as? String != nil
                    {
                        
                        localTeamLogo = localTeamDataDict["logo_path"]! as! String
                    }
                    let localTeamScoreDict = dataDict["scores"]!  as! Dictionary<String, AnyObject>
                    let localTeamScore = localTeamScoreDict["localteam_score"]!  as! Int
                    
                    
                    let visitorTeamDict = dataDict["visitorTeam"]!  as! Dictionary<String, AnyObject>
                    let visitorTeamDataDict = visitorTeamDict["data"]!  as! Dictionary<String, AnyObject>
                    let visitorTeamName = visitorTeamDataDict["name"]! as! String
                    var visitorTeamLogo = ""
                    if  visitorTeamDataDict["logo_path"]! as? String != nil
                    {
                        
                        visitorTeamLogo = visitorTeamDataDict["logo_path"]! as! String
                    }
                    
                    
                    
                    let visitorTeamScoreDict = dataDict["scores"]!  as! Dictionary<String, AnyObject>
                    let visitorTeamScore = visitorTeamScoreDict["visitorteam_score"]!  as! Int
                    
                    object.localTeamName = localTeamName
                    object.visitorTeamName = visitorTeamName
                    object.localLogo = localTeamLogo
                    object.visitorLogo = visitorTeamLogo
                    object.localRealScore = "\(localTeamScore)"
                    object.visitorRealScore = "\(visitorTeamScore)"
                    
                    self.matchesResult.append(object)
                    if self.matchesResult.count == self.matches.count
                    {
                        completion(1)
                        // self.matchesTableView.reloadData()
                        
                        
                    }
                    print("Result \(visitorTeamName)")
                    
                }
                catch
                {}
//                let viewController = self.storyboard!.instantiateViewController(withIdentifier: "verifyCode") as! CheckCodeViewController
//                viewController.phoneNo = self.mobileTextField.text
//                self.navigationController?.pushViewController(viewController, animated: true)
//
            }
            
            //   response.
        })
        task.resume()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
