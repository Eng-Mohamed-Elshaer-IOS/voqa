//
//  MatchesViewController.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import SDWebImage
import IBAnimatable

class MatchesViewController: UIViewController, ShowPidDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    var invitations = [InvitationObject]()

    @IBOutlet weak var localScore: AnimatableTextField!
    @IBOutlet weak var visitorScore: AnimatableTextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var myGroups: UIButton!
    @IBOutlet weak var notificationBtn: SSBadgeButton!

    @IBOutlet weak var groupCollectionView: UICollectionView!

    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var prevBtn: UIButton!

    @IBOutlet weak var yourPointsLbl: UILabel!
    @IBOutlet weak var winPointsBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var viewAll: UIButton!
    @IBOutlet weak var topGroupsLbl: UILabel!

    var groups = [GroupsObject]()

    var matchId: Int!
    
    @IBOutlet weak var datePopupView: UIView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet var popupView: UIView!
    
    
    @IBOutlet weak var noMatchImg: UIImageView!
    @IBOutlet weak var tableCons: NSLayoutConstraint!
    @IBOutlet weak var noMatchLabel: UILabel!
    
    let loadingHud = LoadingHud.shared.loadingHud
    var effect:UIVisualEffect!
    var backGroundBlurView: UIVisualEffectView!
    private let refreshControl = UIRefreshControl()
    
    var callApiDate = ""
    var date = Date()
    var leagues = [League]()
    var count = 0
    
    @IBAction func goToNotifications(_ sender: UIButton)
    {
        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "allNotifications") as! NotificationsViewController
        //viewController.groupId = groups[indexPath.row].groupId!
        //  self.navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if Constants.Settings.lang == "AR" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        notificationBtn.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        // notificationBtn.badge = "4"

        if Constants.Settings.lang == "AR"
        {
            // self.view.semanticContentAttribute = .forceRightToLeft
            dateButton.setTitle("اليوم", for: .normal)
            self.nextBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            self.prevBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            //     self.myGroups.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            self.noMatchLabel.text = "لا توجد مباريات اليوم"
            self.titleLbl.text = "المباريات"
            self.historyBtn.setTitle("التاريخ", for: .normal)
            self.winPointsBtn.setTitle("١٠ نقاط يوميا", for: .normal)
            self.yourPointsLbl.text = "نقاطق"
            self.saveBtn.setTitle("سجل", for: .normal)
            self.myGroups.setTitle("مجموعاتي ", for: .normal)
            self.topGroupsLbl.text = "افضل المجموعات"
            self.viewAll.setTitle("اظهر الكل", for: .normal)
            self.topGroupsLbl.adjustsFontSizeToFitWidth = true
            self.viewAll.contentHorizontalAlignment = .right
            // self.viewAll.backgroundColor = .red
        }
        else
        {
            self.winPointsBtn.setTitle("Daily 10 Points", for: .normal)
            self.yourPointsLbl.text = "Your Points"
            dateButton.setTitle("Today", for: .normal)
            
            self.titleLbl.text = "Matches"
            self.historyBtn.setTitle("History", for: .normal)
            self.saveBtn.setTitle("SAVE", for: .normal)
            
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        count = 0
    //    myGroups.centerImageAndButton(2.0, imageOnTop: false)

    
        refreshControl.tintColor = UIColor.gray
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.gray ]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Data ...", attributes: myAttribute)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        // Do any additional setup after loading the view.
        backGroundBlurView = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
        effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        backGroundBlurView.effect = nil
        
        backGroundBlurView.isUserInteractionEnabled = true
        backGroundBlurView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissPop)))
        
        getMatches()
    }
    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        count = 0
        getMatches()
        self.refreshControl.endRefreshing()
    }
    
    // show no match image.
    func noMatch(theValue:Int){
        if theValue == 0 {
            count = count + 1
            print(" hhhhhhhhhhhhhh \(count)")
            if count >= 5 {
                tableCons.constant = 250
                noMatchImg.isHidden = false
                noMatchLabel.isHidden = false
            } else {
                tableCons.constant = 0
                noMatchImg.isHidden = true
                noMatchLabel.isHidden = true
            }
        }else {
            count = count - 1
        }
    }
    
    private func getMatches() {
        loadingHud.show(in: self.view)
        var datee = ""
        if callApiDate == "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            datee = dateFormatter.string(from: Date())
        } else {
            datee = callApiDate
        }
        print("DATEEEEE \(datee)")
        MatchesManager.getMatchesByDate(body: datee) { [unowned self] (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            self.getAllGroupsAsAdminAPI()
            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                
                return
            }
            if let leagues = response?.data?.leagues {
                self.leagues = leagues
                print("LEAGUES \(leagues)")
                self.tableView.reloadData()
            } else {
                self.leagues = []
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func dismissPop() {
        animateDateOut()
        animateOut()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        animateDateOut()
        animateOut()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showPopup(matchId: Int) {
        self.matchId = matchId
        let prefs = UserDefaults.standard
        let keyValue = prefs.string(forKey:"UserPoints")
        pointsLabel.text = keyValue
        animateIn()
    }
    func getAllNotificationsAPI()
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        //701
        loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "user_id" : userId
        ]
        
        GroupsManager.getAllPendingNotification(body: param){ [unowned self] (invitations, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.invitations.removeAll()
            self.invitations = (invitations?.data)!
            if self.invitations.count>0
            {
                self.notificationBtn.badge = "\(self.invitations.count)"

            }
            self.tableView.reloadData()
            
        }
        
    }
    func getAllGroupsAsAdminAPI()
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        loadingHud.show(in: self.view)
        //        let param: [String: Any] = [
        //            "user_id" : userId
        //            ]
        GroupsManager.getAllGroups{ [unowned self] (groups, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.groups.removeAll()
            self.groups = (groups?.data)!
            
            self.groupCollectionView.reloadData()
            self.getAllNotificationsAPI()
            
        }
        
    }
    @IBAction func viewAllGroups(_ sender: UIButton) {
            let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "allGroups") as! TopGroupsViewController
          //  self.navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
    }
    @IBAction func viewMyGroups(_ sender: UIButton) {
        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "myGroups") as! MyGroupsViewController
        //  self.navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
    }
    @IBAction func save(_ sender: UIButton) {
        // send expect match to api
        guard self.localScore.text != "", self.visitorScore.text != "", let localScoree = Int(self.localScore.text!), let vistorScoree = Int(self.visitorScore.text!) else {
            self.present(UIViewHelper.errorAlert(title: "Wrong data", message: "Please inter correct expectation in correct numbers"), animated: true, completion: nil)
            return
        }
        loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "matchId" : self.matchId,
            "localTeamScore" : localScoree,
            "visitorTeamScore" : vistorScoree
        ]
        MatchesManager.expectMatch(body: param) { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            if response?.status?.code == 200 {
                self.loadingHud.dismiss()
                if Constants.Message.invalidToken == "Invalid Token"
                {
                    self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                    
                    return
                }
                self.present(UIViewHelper.errorAlert(title: "Expect Success", message: (response?.status?.message)!), animated: true, completion: nil)
            } else {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.errorAlert(title: "Expect Failed", message: (response?.status?.message)!), animated: true, completion: nil)
            }
        }
        self.getMatches()
        animateOut()
    }
    
    @IBAction func winDailyPoints(_ sender: UIButton) {
        // call win 10 points api
        loadingHud.show(in: self.view)
        MatchesManager.winTenPoints { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            if response?.status?.code == 200 {
                self.loadingHud.dismiss()
                if Constants.Message.invalidToken == "Invalid Token"
                {
                    self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                    
                    return
                }
                self.present(UIViewHelper.errorAlert(title: "Win Success", message: "You have claimed 10 points for today. Come tomorrow for more points."), animated: true, completion: nil)
            } else {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.errorAlert(title: "Limited Points", message: "You have won 10 points for today before. Come tomorrow for more Points"), animated: true, completion: nil)
            }
        }
        animateOut()
    }
    
    @IBAction func chooseDate(_ sender: UIButton) {
        animateDateIn()
    }
    
    @IBAction func doneChooseDate(_ sender: UIButton) {
        datePickerView.datePickerMode = UIDatePickerMode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.date = datePickerView.date
        let selectedDate = dateFormatter.string(from: datePickerView.date)
        let currentDate = dateFormatter.string(from: Date())
        if selectedDate == currentDate {
            callApiDate = ""
            if Constants.Settings.lang == "AR"
            {
                dateButton.setTitle("اليوم", for: .normal)

            }
            else
            {
                dateButton.setTitle("Today", for: .normal)

            }
        } else {
            callApiDate = selectedDate
            dateButton.setTitle(selectedDate, for: .normal)
        }
        animateDateOut()
        getMatches()
    }
    
    @IBAction func cancelChooseDate(_ sender: UIButton) {
        animateDateOut()
    }
    
    private func animateDateIn() {
        self.view.addSubview(backGroundBlurView)
        self.view.addSubview(datePopupView)
        datePopupView.center = self.view.center
        
        datePopupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        datePopupView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.backGroundBlurView.effect = self.effect
            self.datePopupView.alpha = 1
            self.datePopupView.transform = CGAffineTransform.identity
        }
        
    }
    
    private func animateDateOut () {
        UIView.animate(withDuration: 0.3, animations: {
            self.datePopupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.datePopupView.alpha = 0
            
            self.backGroundBlurView.effect = nil
            
        }) { (success:Bool) in
            self.datePopupView.removeFromSuperview()
            self.backGroundBlurView.removeFromSuperview()
        }
    }
    
    @IBAction func previousDay(_ sender: UIButton) {
        count = 0
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let selectedDate = dateFormatter.string(from: date.yesterday)
        self.date = date.yesterday
        let currentDate = dateFormatter.string(from: Date())
        if selectedDate == currentDate {
            callApiDate = ""
            if Constants.Settings.lang == "AR"
            {
                dateButton.setTitle("اليوم", for: .normal)
            }
            else
            {
                dateButton.setTitle("Today", for: .normal)
            }
            
        } else {
            callApiDate = selectedDate
            dateButton.setTitle(selectedDate, for: .normal)
        }
        getMatches()
    }
    
    @IBAction func nextDay(_ sender: UIButton) {
        count = 0
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let selectedDate = dateFormatter.string(from: date.tomorrow)
        self.date = date.tomorrow
        let currentDate = dateFormatter.string(from: Date())
        if selectedDate == currentDate {
            callApiDate = ""
            if Constants.Settings.lang == "AR"
            {
                dateButton.setTitle("اليوم", for: .normal)
            }
            else
            {
                dateButton.setTitle("Today", for: .normal)
            }
        } else {
            callApiDate = selectedDate
            dateButton.setTitle(selectedDate, for: .normal)
        }
        getMatches()
    }

    private func animateIn() {
        self.view.addSubview(backGroundBlurView)
        self.view.addSubview(popupView)
        popupView.center = self.view.center
        
        popupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        popupView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.backGroundBlurView.effect = self.effect
            self.popupView.alpha = 1
            self.popupView.transform = CGAffineTransform.identity
        }
        
    }
    
    
    private func animateOut () {
        UIView.animate(withDuration: 0.3, animations: {
            self.popupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.popupView.alpha = 0
            
            self.backGroundBlurView.effect = nil
            
        }) { (success:Bool) in
            self.popupView.removeFromSuperview()
            self.backGroundBlurView.removeFromSuperview()
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.groups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "groupCollectionCell", for: indexPath) as! GroupCollectionViewCell
            let object = groups[indexPath.row]
        cell.groupImage.sd_setImage(with: URL(string:"http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public/\(groups[indexPath.row].image!)"),placeholderImage: UIImage(named: "Group -1"))
        cell.groupImage.cornerRadius = cell.groupImage.height/2
        print("out ppppppppppppp\(groups[indexPath.row].image!)")
//        cell.groupImage.image = UIImage(contentsOfFile:"uploads/groups/hhp7aYtxZr8OEnVsjfZxHaCMDRxwtqVpSb7SLTE0.jpeg")
//        cell.groupImage.sd_setImage(with: URL(string: "uploads/groups/hhp7aYtxZr8OEnVsjfZxHaCMDRxwtqVpSb7SLTE0.jpeg"), placeholderImage: UIImage(named: "user"))
        print("yyyyyyyyyyyyyyyyyy\(object.image)")
        cell.groupName.text = object.name
        cell.totalPoint.setTitle("\(object.totalPoints!)", for: .normal)
        
        
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "groupDetails") as! GroupDetailsViewController
        viewController.groupName = "\(groups[indexPath.row].name!)"
        viewController.groupId = groups[indexPath.row].groupId!
        viewController.totalPointsStr = "\(groups[indexPath.row].totalPoints!)"
        //  self.navigationController?.pushViewController(viewController, animated: true)
 
            viewController.editFlag = 1
            
        
        self.present(viewController, animated: true, completion: nil)

    }
    
    
}

extension MatchesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.leagues[section].leagueName
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.leagues[section].matches?.count==0
        {
            return CGFloat(0.0)
        }
        else
        {
        return CGFloat(50.0)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.backgroundColor = UIColor.elusiveGreen
        header.textLabel?.font = UIFont(name: "Montserrat-Medium", size: 14.0)
        header.textLabel?.textAlignment = NSTextAlignment.center
        header.textLabel?.textColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {

        return self.leagues.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let theNum = (self.leagues[section].matches?.count)!
        print("Helooooooooo \(theNum)")
        noMatch(theValue: theNum)
        return theNum
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchCell", for: indexPath) as! MatchCell
        cell.delegate = self
        print("MMMMMM \(self.leagues[indexPath.section].matches![indexPath.row].myExpect?.localScore) \(self.leagues[indexPath.section].matches![indexPath.row].myExpect?.visitorScore)")
        
        if self.leagues[indexPath.section].matches![indexPath.row].myExpect?.checked == "no"
        {
        cell.localExpectedTeamScore.text = "\((self.leagues[indexPath.section].matches![indexPath.row].myExpect?.localScore)!)"
        cell.visitorExpectedTeamScore.text = "\((self.leagues[indexPath.section].matches![indexPath.row].myExpect?.visitorScore)!)"
            cell.localExpectedTeamScore.isHidden = false
            
            cell.visitorExpectedTeamScore.isHidden = false
            cell.localExpectedTeamScoreView.isHidden = false
            cell.visitorExpectedTeamScoreView.isHidden = false

        }
        else
        {
             cell.localExpectedTeamScore.isHidden = true
            cell.visitorExpectedTeamScore.isHidden = true
            cell.localExpectedTeamScoreView.isHidden = true
            cell.visitorExpectedTeamScoreView.isHidden = true
        }
        cell.matchId = self.leagues[indexPath.section].matches![indexPath.row].matchId
        cell.localTeamName.text = self.leagues[indexPath.section].matches![indexPath.row].localTeam!.name
        cell.localTeamImage.sd_setImage(with: URL(string: (self.leagues[indexPath.section].matches![indexPath.row].localTeam!.logo) ?? ""), placeholderImage: UIImage(named: "football"))
        cell.localTeamScore.text = String(describing: self.leagues[indexPath.section].matches![indexPath.row].localTeam!.score!)
        cell.visitorTeamName.text = self.leagues[indexPath.section].matches![indexPath.row].visitorTeam!.name
        cell.visitorTeamImage.sd_setImage(with: URL(string: (self.leagues[indexPath.section].matches![indexPath.row].visitorTeam!.logo) ?? ""), placeholderImage: UIImage(named: "football"))
        cell.visitorTeamScore.text = String(describing: self.leagues[indexPath.section].matches![indexPath.row].visitorTeam!.score!)
        
        cell.matchTime.text = self.leagues[indexPath.section].matches![indexPath.row].startingAt
        
        if self.leagues[indexPath.section].matches![indexPath.row].status == "NS" {
            cell.matchLiveView.alpha = 0.0
            cell.localTeamScoreView.alpha = 0.0
            cell.visitorTeamScoreView.alpha = 0.0
            cell.matchTime.alpha = 1.0
            cell.expectLabel.alpha = 1.0
            if Constants.Settings.lang == "AR"
            {
                cell.expectLabel.text = "توقع النتيجة"
            }
        } else if self.leagues[indexPath.section].matches![indexPath.row].status == "LIVE" || self.leagues[indexPath.section].matches![indexPath.row].status == "HT" {
            cell.matchLiveView.alpha = 1.0
            cell.localTeamScoreView.alpha = 1.0
            cell.visitorTeamScoreView.alpha = 1.0
            cell.matchTime.alpha = 0.0
            cell.expectLabel.alpha = 0.0
            if Constants.Settings.lang == "AR"
            {
                cell.matchLiveLabel.text = "مباشر"
            }
        } else if self.leagues[indexPath.section].matches![indexPath.row].status == "FT" || self.leagues[indexPath.section].matches![indexPath.row].status == "AWARDED" {
            cell.matchLiveView.alpha = 0.0
            cell.localTeamScoreView.alpha = 1.0
            cell.visitorTeamScoreView.alpha = 1.0
            cell.matchTime.alpha = 1.0
            if Constants.Settings.lang == "AR"
            {
                cell.matchTime.text = "انتهت"

                
            }
            else
            {
                cell.matchTime.text = "Ended"

                
            }
            cell.expectLabel.alpha = 0.0
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let tomorrow = dateFormatter.string(from: Date().tomorrow)
        let afterTomorrow = dateFormatter.string(from: Date().tomorrow.tomorrow)
        
        if self.callApiDate != "" && self.callApiDate != tomorrow && self.callApiDate != afterTomorrow {
            cell.expectLabel.alpha = 0.0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
   
    
}
extension UIButton {
    func centerImageAndButton(_ gap: CGFloat, imageOnTop: Bool) {
        
        guard let imageView = self.imageView,
            let titleLabel = self.titleLabel else { return }
        
        let sign: CGFloat = imageOnTop ? 1 : -1;
        let imageSize = imageView.frame.size;
        self.titleEdgeInsets = UIEdgeInsetsMake((imageSize.height+gap)*sign, -imageSize.width, 0, 0);
        
        let titleSize = titleLabel.bounds.size;
        self.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height+gap)*sign, 0, 0, -titleSize.width);
    }
}
