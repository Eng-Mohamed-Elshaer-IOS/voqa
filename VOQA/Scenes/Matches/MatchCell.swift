//
//  MatchCell.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

protocol ShowPidDelegate {
    func showPopup(matchId: Int)
}

class MatchCell: UITableViewCell {

    @IBOutlet weak var localTeamImage: UIImageView!
    @IBOutlet weak var localTeamName: UILabel!
    @IBOutlet weak var localTeamScore: UILabel!
    @IBOutlet weak var localTeamRealScore: UILabel!

    @IBOutlet weak var visitorTeamImage: UIImageView!
    @IBOutlet weak var visitorTeamName: UILabel!
    @IBOutlet weak var visitorTeamScore: UILabel!
    @IBOutlet weak var visitorTeamRealScore: UILabel!

    @IBOutlet weak var matchTime: UILabel!
    
    @IBOutlet weak var matchLiveView: UIView!
    @IBOutlet weak var matchLiveLabel: UILabel!

    @IBOutlet weak var visitorTeamScoreView: UIView!
    @IBOutlet weak var localTeamScoreView: UIView!
    
    @IBOutlet weak var expectLabel: UILabel!
    @IBOutlet weak var localExpectedTeamScore: UILabel!
    @IBOutlet weak var visitorExpectedTeamScore: UILabel!
    @IBOutlet weak var visitorExpectedTeamScoreView: UIView!
    @IBOutlet weak var localExpectedTeamScoreView: UIView!

    var matchId: Int!
    
    var delegate: ShowPidDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        expectLabel.isUserInteractionEnabled = true
        expectLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showPopUp)))
        if (localExpectedTeamScore != nil)
        {
        localExpectedTeamScore.layer.cornerRadius = 4
        visitorExpectedTeamScore.layer.cornerRadius = 4
        }
    }

    @objc func showPopUp() {
        print("FFFFFFF \(self.matchId)")
        delegate.showPopup(matchId: self.matchId!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
