//
//  PlayerInfoViewController.swift
//  Elusive
//
//  Created by Graphic on 3/13/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import SDWebImage

class PlayerInfoViewController: UIViewController {
    
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerInfo: UITextView!
    @IBOutlet weak var playerAge: UILabel!
    @IBOutlet weak var playerPosition: UILabel!
    @IBOutlet weak var playerTeam: UILabel!
    @IBOutlet weak var playerTeamImage: UIImageView!
    
    var playerInformation: Player!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupViews()
    }

    private func setupViews() {
        playerImage.sd_setImage(with: URL(string: playerInformation.image!))
        playerName.text = playerInformation.name
        playerInfo.text = playerInformation.details
        playerAge.text = playerInformation.date_of_birth
        playerPosition.text = playerInformation.position
        playerTeam.text = playerInformation.club?.name
        playerTeamImage.sd_setImage(with: URL(string: (playerInformation.club?.logo!)!))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

// MARK:- IBActions
extension PlayerInfoViewController {
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
