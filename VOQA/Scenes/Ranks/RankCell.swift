//
//  RankCell.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class RankCell: UITableViewCell {

    @IBOutlet weak var rankNumber: UILabel!
    @IBOutlet weak var rankName: UILabel!
    @IBOutlet weak var rankPoints: UILabel!
    @IBOutlet weak var rankProfileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
