//
//  RanksViewController.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftTickerView

class RanksViewController: UIViewController, LabelTrickerDelegate, LabelTrickerViewProvider {
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var pointsLbl: UILabel!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var priceViewHeight: NSLayoutConstraint!

    func tickerView(_ tickerView: LabelTrickerView, prepareSeparator separator: UIView) {
        if let separator = separator as? UILabel {
            separator.textColor = .white
        }
    }
    
    func tickerView(_ tickerView: LabelTrickerView, viewFor: Any) -> (UIView, reuseIdentifier: String?) {
        if let text = viewFor as? String,
            let label = tickerView.dequeReusableNodeView(for: "Karim is testing") as? UILabel {
            label.text = text
            label.sizeToFit()
            label.textColor = .white
            return (label, reuseIdentifier: "Karim is testing")
        }
        return (UIView(), reuseIdentifier: nil)
    }
    
    func tickerView(willResume ticker: LabelTrickerView) {
        
    }
    
    func tickerView(willStart ticker: LabelTrickerView) {
        
    }
    
    func tickerView(willStop ticker: LabelTrickerView) {
        
    }
    
    func tickerView(didPress view: UIView, content: Any?) {
        
    }
    

    @IBOutlet weak var userPointsLabel: UILabel!
    @IBOutlet weak var ranks_TableView: UITableView!
    @IBOutlet weak var prizeLabel: LabelTrickerView!
    
    private let refreshControl = UIRefreshControl()
    let loadingHud = LoadingHud.shared.loadingHud
    
    var ranks: Rankings!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        refreshControl.tintColor = UIColor.gray
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.gray ]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Data ...", attributes: myAttribute)
        
        if #available(iOS 10.0, *) {
            ranks_TableView.refreshControl = refreshControl
        } else {
            ranks_TableView.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        userPointsLabel.text = "-"
        
        prizeLabel.contentProvider = TickerProvider()
        prizeLabel.viewProvider = self
        prizeLabel.separator = "-"
        prizeLabel.direction = .horizontalLeftToRight
        prizeLabel.registerNodeView(UILabel.self, for: "Karim is testing")
        prizeLabel.tickerDelegate = self
        //getRanking()
    }
    
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        getRanking()
        self.refreshControl.endRefreshing()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.headerHeight.constant = 50
        self.priceViewHeight.constant = 0

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "الترتيب"
            self.pointsLbl.text = "جميع النقاط"

        }
        else
        {
            self.titleLbl.text = "Ranks"
            self.pointsLbl.text = "Total Points"

        }
        getRanking()
        //getPrizes()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        prizeLabel.stop()
    }
    
    private func getPrizes() {
        UserManager.getRankPrize { [weak self] (prize, error) in
            if error != nil {
                return
            }
            
            if prize != nil {
                self?.prizeLabel.contentProvider?.setContent(data: prize!)
                self?.prizeLabel.start()
            }
        }
    }
    
    private func getRanking() {
        loadingHud.show(in: self.view)
        UserManager.getUserRanks { (ranks, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                
                return
            }
            guard ranks?.data != nil else {
                return
            }
            self.ranks = ranks
            self.userPointsLabel.text = String(describing: self.ranks.data!.user!.user!.points!)
            let prefs = UserDefaults.standard
            prefs.set(self.userPointsLabel.text, forKey: "UserPoints")
            self.setUserInfo()
            self.ranks_TableView.reloadData()
        }
    }

    func setUserInfo() {
        let prefs = UserDefaults.standard
        let keyValue = prefs.string(forKey:"UserPoints")
        userPointsLabel.text = keyValue
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension RanksViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if ranks != nil {
            return 2
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return ranks != nil ? (ranks.data?.top?.count)! : 0
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.backgroundColor = UIColor.elusiveGreen
        header.textLabel?.font = UIFont(name: "Montserrat-Medium", size: 20.0)
        header.textLabel?.textAlignment = NSTextAlignment.center
        header.textLabel?.textColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            if Constants.Settings.lang == "AR"
            {
                return "افضل 50"
            }
            else
            {
                return "Top 50"
            }
        } else {
            if Constants.Settings.lang == "AR"
            {
                return "ترتيبك"
            }
            else
            {
                return "Your Rank"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ranks != nil {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "rankCell", for: indexPath) as! RankCell
                let rankInfo = ranks.data?.top![indexPath.row]
                cell.rankNumber.text = "\(indexPath.row + 1)"
                cell.rankName.text = rankInfo?.name
                if Constants.Settings.lang == "AR"
                {
                    cell.rankPoints.text = "النقط: " + String(describing: rankInfo!.points!)

                }
                else
                {
                    cell.rankPoints.text = "Points: " + String(describing: rankInfo!.points!)

                    
                }
                cell.rankProfileImage.sd_setImage(with: URL(string: (rankInfo?.image)!), placeholderImage: UIImage(named: "user"))
                print("llllllllllll\(rankInfo?.image)")
                return cell
            } else {
                if indexPath.row == 0 { // before
                    guard let rankInfo = ranks.data?.user?.before, let _ = rankInfo.rank, let _ = rankInfo.name, let _ = rankInfo.points, let _ = rankInfo.image else {
                        return UITableViewCell()
                    }
                    let cell = tableView.dequeueReusableCell(withIdentifier: "rankCell", for: indexPath) as! RankCell
                    cell.rankNumber.text = String(describing: rankInfo.rank!)
                    cell.rankName.text = rankInfo.name
                    cell.rankPoints.text = "Points: " + String(describing: rankInfo.points!)
                    cell.rankProfileImage.sd_setImage(with: URL(string: (rankInfo.image)!), placeholderImage: UIImage(named: "user"))
                    return cell
                } else if indexPath.row == 1 { // user
                    guard let rankInfo = ranks.data?.user?.user, let _ = rankInfo.rank, let _ = rankInfo.name, let _ = rankInfo.points, let _ = rankInfo.image else {
                        return UITableViewCell()
                    }
                    let cell = tableView.dequeueReusableCell(withIdentifier: "rankCell", for: indexPath) as! RankCell
                    cell.rankNumber.text = String(describing: rankInfo.rank!)
                    cell.rankName.text = rankInfo.name
                    cell.rankPoints.text = "Points: " + String(describing: rankInfo.points!)
                    cell.rankProfileImage.sd_setImage(with: URL(string: (rankInfo.image)!), placeholderImage: UIImage(named: "user"))
                    return cell
                } else { // after
                    guard let rankInfo = ranks.data?.user?.after, let _ = rankInfo.rank, let _ = rankInfo.name, let _ = rankInfo.points, let _ = rankInfo.image else {
                        return UITableViewCell()
                    }
                    let cell = tableView.dequeueReusableCell(withIdentifier: "rankCell", for: indexPath) as! RankCell
                    cell.rankNumber.text = String(describing: rankInfo.rank!)
                    cell.rankName.text = rankInfo.name
                    cell.rankPoints.text = "Points: " + String(describing: rankInfo.points!)
                    cell.rankProfileImage.sd_setImage(with: URL(string: (rankInfo.image)!), placeholderImage: UIImage(named: "user"))
                    return cell
                }
            }
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
