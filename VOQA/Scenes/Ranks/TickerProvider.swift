//
//  TickerProvider.swift
//  Elusive
//
//  Created by Graphic on 5/7/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

class TickerProvider: LabelTrickerProviderProtocol {
    private var content = [""]
    private var index = 0
    
    
    var hasContent = true
    var next: Any {
        if index >= self.content.count {
            index = 0
        }
        let next = self.content[index]
        index += 1
        return next
    }
    
    func setContent(data: String) {
        self.content.removeAll()
        self.content.append(data)
    }
}
