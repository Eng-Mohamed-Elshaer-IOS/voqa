//
//  ChatViewController.swift
//  Elusive
//
//  Created by Graphic on 4/3/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import AVFoundation

class ChatViewController: UIViewController {

    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    private var isRecording = false
    var audioStatus: AudioStatus = AudioStatus.Stopped
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupRecorder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
//    @IBAction func record(_ sender: UIButton) {
//        if isRecording {
//            isRecording = false
//            recordButton.setImage(UIImage(named: "rec.png"), for: UIControlState.normal)
//        } else {
//            isRecording = true
//            recordButton.setImage(UIImage(named: "pause.png"), for: UIControlState.normal)
//            // Record Sound
//        }
//    }

}

// MARK:- IBActions
extension ChatViewController {
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Controls
    @IBAction func onRecord(_ sender: UIButton) {
        if appHasMicAccess == true { // Look into why I did this ;]
            if audioStatus != .Playing {
                
                switch audioStatus {
                case .Stopped:
                    recordButton.setImage(UIImage(named: "pause.png"), for: UIControlState.normal)
                    record()
                case .Recording:
                    recordButton.setImage(UIImage(named: "rec.png"), for: UIControlState.normal)
                    stopRecording()
                default:
                    break
                }
            }
        } else {
            recordButton.isEnabled = false
            let theAlert = UIAlertController(title: "Requires Microphone Access",
                                             message: "Go to Settings > PenguinPet > Allow PenguinPet to Access Microphone.\nSet switch to enable.",
                                             preferredStyle: UIAlertControllerStyle.alert)
            
            theAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.view?.window?.rootViewController?.present(theAlert, animated: true, completion:nil)
        }
        
    }
    
    @IBAction func onPlay(_ sender: UIButton) {
        if audioStatus != .Recording {
            
            switch audioStatus {
            case .Stopped:
                play()
            case .Playing:
                stopPlayback()
            default:
                break
            }
        }
        
        MessageManager.uploadFile { (news, error) in
            
        }
    }
    
    func setPlayButtonOn(flag: Bool) {
        if flag == true {
            playButton.setTitle("Pause", for: .normal)
        } else {
            playButton.setTitle("Play", for: .normal)
        }
    }
    
}

// MARK: - AVFoundation Methods
extension ChatViewController: AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    // MARK: Recording
    func setupRecorder() {
        let fileURL = getURLforMemo()
        
        let recordSettings = [
            AVFormatIDKey: Int(kAudioFormatLinearPCM),
            AVSampleRateKey: 44100.0,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ] as [String : Any]
        
        do {
            audioRecorder = try AVAudioRecorder(url: fileURL as URL, settings: recordSettings as [String : AnyObject])
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            print("Error creating audioRecorder.")
        }
    }
    
    func record() {
        audioRecorder.record()
        audioStatus = .Recording
    }
    
    func stopRecording() {
        audioRecorder.stop()
        recordButton.setBackgroundImage(UIImage(named: "button-record"), for: UIControlState.normal)
        audioStatus = .Stopped
    }
    
    // MARK: Playback
    func  play() {
        let fileURL = getURLforMemo()
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: fileURL as URL)
            audioPlayer.delegate = self
            if audioPlayer.duration > 0.0 {
                setPlayButtonOn(flag: true)
                audioPlayer.play()
                audioStatus = .Playing
            }
        } catch {
            print("Error loading audioPlayer.")
        }
    }
    
    func stopPlayback() {
        audioPlayer.stop()
        setPlayButtonOn(flag: false)
        audioStatus = .Stopped
    }
    
    // MARK: Delegates
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        audioStatus = .Stopped
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        setPlayButtonOn(flag: false)
        audioStatus = .Stopped
    }
    
    // MARK: - Helpers
    
    func getURLforMemo() -> NSURL {
        let tempDir = NSTemporaryDirectory()
        let filePath = tempDir + "/UsiveChat.caf"
        
        return NSURL.fileURL(withPath: filePath) as NSURL
    }
}

