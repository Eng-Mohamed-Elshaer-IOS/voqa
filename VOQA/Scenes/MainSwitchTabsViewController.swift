//
//  MainSwitchTabsViewController.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import ESTabBarController_swift

class MainSwitchTabsViewController: ESTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        self.viewControllers?[0].tabBarItem = ESTabBarItem.init(ExampleBackgroundContentView(), title: nil, image: UIImage(named: "news"), selectedImage: UIImage(named: "news_w"))
        
        //self.viewControllers?[1].tabBarItem = ESTabBarItem.init(ExampleBackgroundContentView(), title: nil, image: UIImage(named: "home"), selectedImage: UIImage(named: "home_w"))
        
        self.viewControllers?[0].tabBarItem = ESTabBarItem.init(ExampleBackgroundContentView(), title: nil, image: UIImage(named: "live"), selectedImage: UIImage(named: "live_w"))
        
        self.viewControllers?[1].tabBarItem = ESTabBarItem.init(ExampleBackgroundContentView(), title: nil, image: UIImage(named: "fav"), selectedImage: UIImage(named: "fav_w"))
        self.viewControllers?[2].tabBarItem = ESTabBarItem.init(ExampleBackgroundContentView(), title: nil, image: UIImage(named: "home"), selectedImage: UIImage(named: "home_w"))

        self.viewControllers?[3].tabBarItem = ESTabBarItem.init(ExampleBackgroundContentView(), title: nil, image: UIImage(named: "more"), selectedImage: UIImage(named: "more_w"))
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.tabBar.semanticContentAttribute = .forceRightToLeft
        if Constants.Settings.lang == "AR"
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
