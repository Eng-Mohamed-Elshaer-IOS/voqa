//
//  PointShopViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class PointShopViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var restaurantsCollectionView: UICollectionView!
    @IBOutlet weak var restaurantsDiscountCollectionView: UICollectionView!

    @IBOutlet weak var heightConstraintCollection: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintHeader: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    var restaurants = [RestaurantsObject]()
    var restaurantsDiscounts = [RestaurantDiscount]()

    let loadingHud = LoadingHud.shared.loadingHud

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == restaurantsCollectionView
        {
            return self.restaurants.count
        }
        else
        {
            
            return self.restaurantsDiscounts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == restaurantsCollectionView
        {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restaurantCell", for: indexPath) as! RestaurantCollectionViewCell
        let object = restaurants[indexPath.row]
        
        cell.restaurantImage.sd_setImage(with: URL(string: object.image!))
        cell.restaurantImage.cornerRadius = cell.restaurantImage.height/2
        cell.restaurantImage.clipsToBounds = true
        cell.restaurantImage.layer.masksToBounds = true
//        cell.restaurantImage.borderColor = UIColor(hexString: "27E680")
//        cell.restaurantImage.borderWidth = 2
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.shadowOpacity = 0.7
        cell.layer.shadowRadius = 6.0
      //  cell.restaurantImage.clipsToBounds = true
//        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.restaurantImage.bounds, cornerRadius: 100).cgPath
//        cell.layer.shouldRasterize = true
//        cell.layer.rasterizationScale =  1
      //  cell.restaurantImage.layer.masksToBounds=false
      //  cell.restaurantImage.layer.masksToBounds = true
//        cell.restaurantImage.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
//        cell.restaurantImage.layer.shouldRasterize = true
//        cell.restaurantImage.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
            if Constants.Settings.lang == "AR"
            {
                cell.restaurantTitle.text = object.nameAr
            }
            else
            {
                cell.restaurantTitle.text = object.name

            }

        return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "restaurantDiscountCell", for: indexPath) as! RestaurantDiscountCollectionCellView
            let object = restaurantsDiscounts[indexPath.row]
            
            cell.restaurantImage.sd_setImage(with: URL(string: object.image!))
            cell.restaurantImage.cornerRadius = cell.restaurantImage.height/2
            cell.restaurantImage.clipsToBounds = true
            cell.restaurantImage.layer.masksToBounds = true
            //        cell.restaurantImage.borderColor = UIColor(hexString: "27E680")
            //        cell.restaurantImage.borderWidth = 2
//            cell.layer.shadowColor = UIColor.black.cgColor
//            cell.layer.shadowOffset = CGSize(width: -1, height: 1)
//            cell.layer.shadowOpacity = 0.7
//            cell.layer.shadowRadius = 6.0
            //  cell.restaurantImage.clipsToBounds = true
            //        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.restaurantImage.bounds, cornerRadius: 100).cgPath
            //        cell.layer.shouldRasterize = true
            //        cell.layer.rasterizationScale =  1
            //  cell.restaurantImage.layer.masksToBounds=false
            //  cell.restaurantImage.layer.masksToBounds = true
            //        cell.restaurantImage.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
            //        cell.restaurantImage.layer.shouldRasterize = true
            //        cell.restaurantImage.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
            
            if let temperature = object.awards?.count
            {
                if Constants.Settings.lang == "AR"
                {
                    let noOfVoucherStr = "\(temperature) كوبون"
                    cell.noOfVouchers.text = noOfVoucherStr

                    
                }
                else
                {
                    let noOfVoucherStr = "\(temperature) Coupons"
                    cell.noOfVouchers.text = noOfVoucherStr

                    
                }

            }

            
           // let x = noOfVoucherStr!
          //  cell.noOfVouchers
    

            
            
            
            return cell
            
            
        }

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == restaurantsCollectionView
        {
            let object = restaurants[indexPath.row]

            let storyboard =  UIStoryboard.init(name: "Home", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "restaurantDetails") as! RestaurantDetailsViewController
       // self.navigationController?.pushViewController(viewController, animated: true)
            viewController.restaurant = object
            self.present(viewController, animated: true, completion: nil)
        }
        else
        {
//            let storyboard =  UIStoryboard.init(name: "Home", bundle: nil)
//            let viewController = storyboard.instantiateViewController(withIdentifier: "restaurantDetails") as! RestaurantDetailsViewController
//            // self.navigationController?.pushViewController(viewController, animated: true)
//            viewController.restaurant = object
//            self.present(viewController, animated: true, completion: nil)

            
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", for: indexPath)
        
       // header.headerLabel.text = "YOUR_HEADER_TEXT"
        
        return header
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIView.appearance().semanticContentAttribute = .forceLeftToRight

    }
    override func viewWillAppear(_ animated: Bool) {
        
        if Constants.Settings.lang == "AR"
        {
           // cellIdentifier = "awardCell2"
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.headerLabel.text = "تخفيضاتي"
            self.titleLabel.text = "متجر النقاط"
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard

        let userId = defaults.object(forKey: "UserID")

        restaurantsCollectionView.register(UINib(nibName: "headerView", bundle: nil), forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
        loadingHud.show(in: self.view)

        RestaurantsManager.getAllRestaurants { (restaurants, error) in
            if error != nil {
               self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                
                return
            }
            self.restaurants.removeAll()
            self.restaurants = (restaurants?.data)!
            self.restaurantsCollectionView.reloadData()
            
        }
       let  userIdStr="\(userId!)"
        loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "user_id" : userIdStr
        ]
        RestaurantsManager.getAllRestaurantDiscounts(body: param) { [unowned self] (restaurantsDiscounts, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)

                return
            }
            self.restaurantsDiscounts.removeAll()
            self.restaurantsDiscounts = (restaurantsDiscounts?.data)!
            print("Awards \(self.restaurantsDiscounts.count)")
            self.restaurantsDiscountCollectionView.reloadData()
            
            if(self.restaurantsDiscounts.count==0)
            {
                self.heightConstraintCollection.constant=0
                self.heightConstraintHeader.constant=0
                
            }
    
            
        }


        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        // self.view.semanticContentAttribute = .forceRightToLeft
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
