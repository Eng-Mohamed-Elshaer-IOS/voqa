//
//  MyPointViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/12/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class MyPointViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let loadingHud = LoadingHud.shared.loadingHud
    var backGroundBlurView: UIVisualEffectView!
    var effect:UIVisualEffect!
    var restaurant:RestaurantsObject!
    var restaurantAward:RestaurantAward!
    var points:Int!

    var restaurantId:Int!
    var restaurantName:String!
    var restaurantsDiscounts = [RestaurantDiscount]()
    var restaurantPoints : RestaurantPoints!

    @IBOutlet var popupView: UIView!
    @IBOutlet weak var myPointsView: UIView!
    @IBOutlet weak var myPoints: UILabel!
    @IBOutlet weak var voucherCode: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var letsGoBtn: UIButton!

    @IBOutlet weak var tableWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var tableView: WZYCircularTableView!
    
    var awards = [Award]()
    @IBAction func letsGpTapped(_ sender: UIButton)
    {
        self.dismissPop()
    }
    @objc func viewCode(sender: UIButton){
        for restaurant in restaurantsDiscounts
        {
            if restaurant.restaurantId  == self.restaurantId
            {
                self.voucherCode.text = restaurant.awards?[sender.tag].code

                
            }
            
        }
        UserDefaults.standard.set(self.restaurantId, forKey: "RestaurantID")
        UserDefaults.standard.set(self.restaurantName, forKey: "RestaurantName")
        self.animateIn()

    }
  
    @objc func buyItem(sender: UIButton){
        //...
        var price : Int?
        for award in self.awards
        {
            
            if award.id == sender.tag
            {
                price = award.price
                
            }
        }
        let defaults = UserDefaults.standard
        let userId = defaults.object(forKey: "UserID")
      loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "award_id" : sender.tag,
            "user_id" : userId!,
        ]
       // print("USERID \(userId!)")
        AwardManager.buyAward(body: param) { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
 
            if let voucherC = response?.1
            {
                self.loadingHud.dismiss()

                if response?.0 == true
                {
                    self.voucherCode.text = voucherC
                    UserDefaults.standard.set(self.restaurantId, forKey: "RestaurantID")
                    UserDefaults.standard.set(self.restaurantName, forKey: "RestaurantName")
//                    let deductedPrice =   UserDefaults.standard.value(forKey: "UserPoints") as! Int - price!
//                    UserDefaults.standard.set(deductedPrice, forKey: "UserPoints")

                    
                    self.animateIn()
                }
                else
                {
                    self.loadingHud.dismiss()

                    self.present(UIViewHelper.errorAlert(title: "Error", message: (response?.1)!), animated: true, completion: nil)
                    return
                }
            }
            else
            {
                self.loadingHud.dismiss()

            }
            
        /*    if response?.status?.code == 200 {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.errorAlert(title: "Expect Success", message: (response?.status?.message)!), animated: true, completion: nil)
            } else {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.errorAlert(title: "Expect Failed", message: (response?.status?.message)!), animated: true, completion: nil)
            }*/
        }

    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "awardCell1", for: indexPath) as! AwardTableViewCell

      //  cell.getButton.x = self.view.width-20
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return awards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       /* if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "awardCell2", for: indexPath) as! AwardTableViewCell
            let object = awards[indexPath.row]
            cell.logo.sd_setImage(with: URL(string: object.image!))
            cell.logo.cornerRadius = cell.logo.height/2
            //cell.logo.borderColor = UIColor(hexString: "27E680")
            cell.logo.clipsToBounds = true
          //  cell.logo.borderWidth = 2

//            cell.layer.shadowColor = UIColor.black.cgColor
//            cell.layer.shadowOffset = CGSize(width: -1, height: 1)
//            cell.layer.shadowOpacity = 0.7
//            cell.layer.shadowRadius = 6.0
            cell.itemTitle.text = object.name
            cell.getButton.tag = object.id!
            cell.getButton.addTarget(self, action:#selector(MyPointViewController.buyItem(sender:)), for: .touchUpInside)
            var pointStr =  "\(object.price!) Point"

            cell.points.text = pointStr

            return cell

        }
//        else if indexPath.row == 4
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "awardCell3", for: indexPath) as! AwardTableViewCell
//            let object = awards[indexPath.row]
//            cell.logo.sd_setImage(with: URL(string: object.image!))
//            cell.logo.cornerRadius = cell.logo.height/2
//            cell.logo.borderColor = UIColor(hexString: "27E680")
//
//            cell.layer.shadowColor = UIColor.black.cgColor
//            cell.layer.shadowOffset = CGSize(width: -1, height: 1)
//            cell.layer.shadowOpacity = 0.7
//            cell.layer.shadowRadius = 6.0
//            cell.logo.borderWidth = 2
//
//            cell.itemTitle.text = object.name
//            cell.getButton.tag = object.id!
//            cell.getButton.addTarget(self, action:#selector(MyPointViewController.buyItem(sender:)), for: .touchUpInside)
//            cell.logo.clipsToBounds = true
//
//            var pointStr =  "\(object.price!) Point"
//
//            cell.points.text = pointStr
//
//            return cell
//
//        }
        
        else
        {*/
        
        var cellIdentifier = ""
        if Constants.Settings.lang == "AR"
        {
            cellIdentifier = "awardCell2"
        }
        else
        {
            cellIdentifier = "awardCell1"
        }

        
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AwardTableViewCell
        cell.semanticContentAttribute = .forceRightToLeft
            let object = awards[indexPath.row]
            cell.logo.sd_setImage(with: URL(string: object.image!))
            cell.logo.cornerRadius = cell.logo.height/2
          //  cell.logo.borderColor = UIColor(hexString: "27E680")
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: -1, height: 1)
            cell.layer.shadowOpacity = 0.7
            cell.layer.shadowRadius = 6.0
        var found = false
        var ss = Int()
        var dateDiff = Int()

        
        for restaurant in restaurantsDiscounts
        {
            if restaurant.restaurantId  == self.restaurantId
            {
                var index = 0
                for voucher in restaurant.awards!
                {
                    if voucher.awardId == object.id
                    {
                        
                        found = true
                      //  restaurantAward = voucher
                        ss = index
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

                        let now = dateFormatter.date(from: voucher.createdDate!)
                        let endDate = Date()
                       // print("SSSSS \(now)   \(endDate)")
                        let formatter = DateComponentsFormatter()
                        formatter.allowedUnits = [.day]
                        formatter.unitsStyle = .positional
                        let string = formatter.string(from: now!, to: Date())!
                        let commaIndex = string.characters.index(of: "d") ?? string.endIndex
                        var outputString = string.substring(to: commaIndex)
                        
                        // Result
                         dateDiff = 7 - Int(outputString)!
                      //  print("\(dateDiff) \(object.name)" )

                    //    print(string) // 2 weeks, 3 days
                      //  voucherStr = voucher.code!
                    }
                    index = index + 1
                }
                
            }
            
        }
        if Constants.Settings.lang == "AR"
        {
            
            cell.getButton.setImage(UIImage(named: "Group 28"), for: .normal)
        }
        if found == true
        {
            if(dateDiff>0)
            {
                cell.viewCodeView.isHidden = false
                cell.getButton.isHidden = true
            
                cell.viewCodeBtn.tag = ss
                cell.viewCodeBtn.addTarget(self, action:#selector(MyPointViewController.viewCode(sender:)), for: .touchUpInside)
                if Constants.Settings.lang == "AR"
                {
                    cell.viewCodeLbl.text = "ينتهي في \(dateDiff) ايام"

                }
                else
                {
                    cell.viewCodeLbl.text = "Expires in \(dateDiff) days"

                    
                }
            }
            else
            {

                cell.viewCodeView.isHidden = true
                cell.getButton.isHidden = false

            }
            

        
        }
        else
        {
            cell.viewCodeView.isHidden = true
            cell.getButton.isHidden = false


        }
           // cell.logo.borderWidth = 2

        
            cell.getButton.tag = object.id!
        if object.isAvailable == 0 {
            cell.getButton.isEnabled = false
        }
       //cell.getButton.bounds.origin.x = screenWidth

         //   cell.getBtnTrailingConstrainet.constant = 20

            cell.getButton.addTarget(self, action:#selector(MyPointViewController.buyItem(sender:)), for: .touchUpInside)
            cell.logo.clipsToBounds = true
        if Constants.Settings.lang == "AR"{
            cell.itemTitle.text = object.nameAr

            let pointStr =  "\(object.price!) نقطة"
            cell.points.text = pointStr

        }
        else
        {
            cell.itemTitle.text = object.name

            let pointStr =  "\(object.price!) Point"
            cell.points.text = pointStr

            
        }
        

            return cell

       // }

        
    }
    

    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    func getPointsAPI()
        //completion: @escaping (_ result: Int) -> ())
    {
        
       /* loadingHud.show(in: self.view)
        let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        //  print("MATCHID \(matchId)  \(token!)")
        let param: [String: Any] = [:]
        //     print("PARAM \(param)")
        
        
        let urlPath = "http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public/api/user/mypoints/\(userId)"
        //   https://35.198.81.33/
        let url1: URL = URL(string: urlPath as String)!
        
        let request = NSMutableURLRequest(url:url1 )
        let session = URLSession.shared
        
        request.httpMethod = "GET"
        
        do
        {
            request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            
        }
        catch
        {}
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            let res = response as? HTTPURLResponse
            if res!.statusCode == 200
            {
                
                do
                {
                    let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                    //  let json = JSON(data)
                    //   print("RESULT \(result)")
                    
                    DispatchQueue.main.async
                        {
                         //   self.loadingHud.dismiss()

                            var points1 = result!["points"] as! Int
                            completion(points1)
                            //self.getAwardsAPI()
                        }
                }
                catch
                {}
            }
            else
            {
                DispatchQueue.main.async
                    {
               // self.loadingHud.dismiss()
                }
            }
            
            //   response.
        })
        task.resume()*/
 
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)

        loadingHud.show(in: self.view)
        let ss = "\(userId)"
        RestaurantsManager.getMyPoints(userId: ss){ [unowned self] (restaurantPoints, error) in
            //.getAllAwardsByRestaurant(body: param) { [unowned self] (awards, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            
            
//            self.awards.removeAll()
//            self.awards = (awards?.data)!
         //   print("POINTS \((restaurantPoints?.points)!)")
                        if Constants.Settings.lang == "AR"
                        {
                            self.myPoints.text = "\((restaurantPoints?.points)!)\nمجموع النقاط"
            
                        }
                        else
                        {
                            self.myPoints.text = "\((restaurantPoints?.points)!)\nTotal Points"
            
                            //    self.titleLbl.text = "News Feeds"
            
            
                        }
            self.getAwardsAPI()
            //     print("Awards \(self.awards.count)")
         //   self.getCodesAPI()
            // self.tableView.reloadData()
            
        }
    }
    private func getAwardsAPI()
    {
        
        //   print("RESTID \(restaurant.restaurantId!)")
        //var restIdStr = res
        let param: [String: Any] = [
            "restaurant_id" : restaurant.restaurantId!
        ]
        loadingHud.show(in: self.view)
        AwardManager.getAllAwardsByRestaurant(body: param) { [unowned self] (awards, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.awards.removeAll()
            self.awards = (awards?.data)!
            print("AWARDS \(awards?.data)")
            
            //     print("Awards \(self.awards.count)")
            self.getCodesAPI()
            // self.tableView.reloadData()
            
        }
        
    }
    func getCodesAPI()
    {
        let defaults = UserDefaults.standard
        
        let userId = defaults.object(forKey: "UserID")
        let  userIdStr = "\(userId!)"
        print("USERID \(userIdStr)")
        let param: [String: Any] = [
            "user_id" : userIdStr
        ]
        loadingHud.show(in: self.view)
        //([String:Any])
        RestaurantsManager.getAllRestaurantDiscounts(body: param) { [unowned self] (restaurantsDiscounts, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.restaurantsDiscounts.removeAll()
            self.restaurantsDiscounts = (restaurantsDiscounts?.data)!
          //  print("Awards \(self.restaurantsDiscounts.count)")
            self.tableView.reloadData()
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if Constants.Settings.lang == "AR"
        {
            myPointsView.semanticContentAttribute = .forceRightToLeft
            myPointsView.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            tableView.semanticContentAttribute = .forceRightToLeft
            //  tableView.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            myPoints.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            tableView.contentAlignment = .left
            self.view.semanticContentAttribute = .forceRightToLeft
            //UIView.appearance().semanticContentAttribute = .forceRightToLeft
            tableView.reloadData({
                self.tableView.scrollFirstCellToCenter()
                
            })
        }
        
    self.getPointsAPI()
//        self.getPointsAPI(){(value) in
//            self.loadingHud.dismiss()
//            if Constants.Settings.lang == "AR"
//            {
//                self.myPoints.text = "\(value)\nمجموع النقاط"
//
//            }
//            else
//            {
//                self.myPoints.text = "\(value)\nTotal Points"
//
//                //    self.titleLbl.text = "News Feeds"
//
//
//            }
//        }



//        self.tableView.semanticContentAttribute = .forceRightToLeft

 

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restaurantName = restaurant.name
        self.restaurantId = restaurant.restaurantId
        tableView.radius = 120
        tableView.isInfiniteScrollingEnabled = false
        tableView.contentAlignment = .right;
        let defaults = UserDefaults.standard

        let userId = defaults.object(forKey: "UserID")
        backGroundBlurView = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: self.screenWidth, height: self.screenHeight))
        effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        backGroundBlurView.effect = nil
        
        backGroundBlurView.isUserInteractionEnabled = true
        backGroundBlurView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissPop)))

        myPointsView.clipsToBounds = true
        myPointsView.layer.masksToBounds = true
        if Constants.Settings.lang == "AR"
        {
            self.letsGoBtn.setTitle("هيا بنا", for: .normal)
            self.msgLbl.text = "تهانينا ، لقد تلقيت رمز الخصم"
        }
        else
        {
            self.letsGoBtn.setTitle("Let's go", for: .normal)
            self.msgLbl.text = "Congratulations you have received the discount code"
        }
    }
 
    private func animateIn() {
        self.view.window?.addSubview(backGroundBlurView)
   //     self.view.addSubview(backGroundBlurView)
        self.view.window?.addSubview(popupView)
//        let topConstraint1 = secondContainer.topAnchor.constraint(
//            equalTo: view.topAnchor,
//            constant: 10
//        )
//        let bottomConst = popupView.bottomAnchor.constraint(
//            equalTo: (self.view.window?.bottomAnchor)!,
//            constant: 100
//        )
//        NSLayoutConstraint.activate([ bottomConst])

        //popupView.frame = CGRect(x: (self.view.window?.width)!/2, y: (self.view.window?.height)!/4, width: popupView.width, height: popupView.height)
        popupView.center = (self.view.window?.center)!
       // popupView.y = (self.view.window?.height)!/2-50
        popupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        popupView.roundCorners([ .topLeft,.topRight,.bottomRight,.bottomLeft], radius: 50)

        popupView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.backGroundBlurView.effect = self.effect
            self.popupView.alpha = 1
            self.popupView.transform = CGAffineTransform.identity
        }
        
    }
    
    
    private func animateOut () {
        UIView.animate(withDuration: 0.3, animations: {
            self.popupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.popupView.alpha = 0
            
            self.backGroundBlurView.effect = nil
            
        }) { (success:Bool) in
            self.popupView.removeFromSuperview()
            self.backGroundBlurView.removeFromSuperview()
            self.getPointsAPI()
//            let myPointsStr = UserDefaults.standard.object(forKey: "UserPoints")
//            print("POINTS \(myPointsStr)")

            
        }
    }

    override func viewDidLayoutSubviews() {
        myPointsView.roundCorners([ .bottomRight,.topRight], radius: 50)

    }
 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func dismissPop() {
     //   animateDateOut()
        animateOut()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
