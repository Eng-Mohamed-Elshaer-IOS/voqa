//
//  RestaurantDetailsViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/12/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class RestaurantDetailsViewController: UIViewController,UIViewControllerTransitioningDelegate {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var myPointBtn: UIButton!
    @IBOutlet weak var aboutBtn: UIButton!
    @IBOutlet weak var restTitle: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var aboutContainer: UIView!
    @IBOutlet weak var myPointContainer: UIView!
    @IBOutlet weak var backBtn: UIButton!
     @IBOutlet weak var backBtnConstraint: NSLayoutConstraint?

    var restaurant : RestaurantsObject?

    @IBAction func myPointClicked(_ sender: UIButton) {
       // self.dismiss(animated: true, completion: nil)
        self.aboutBtn.backgroundColor = UIColor.clear
        self.aboutBtn.titleLabel?.textColor = UIColor.white
        self.myPointBtn.backgroundColor = UIColor.white
        self.myPointBtn.titleLabel?.textColor = UIColor.black
        aboutContainer.isHidden = true
        myPointContainer.isHidden = false

    }
    @IBAction func aboutClicked(_ sender: UIButton) {
        self.aboutBtn.backgroundColor = UIColor.white
        self.myPointBtn.backgroundColor = UIColor.clear
        self.myPointBtn.titleLabel?.textColor = UIColor.white
        self.aboutBtn.titleLabel?.textColor = UIColor.black
        self.aboutBtn.setTitleColor(UIColor.black, for: .normal)
        aboutContainer.isHidden = false
        myPointContainer.isHidden = true

//        self.aboutBtn.tintColor = UIColor.black

       // self.dismiss(animated: true, completion: nil)
    }
    @IBAction func backClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    override func viewWillAppear(_ animated: Bool) {
        if Constants.Settings.lang == "AR"
        {
            self.aboutBtn.setTitle("حول", for: .normal)
            self.myPointBtn.setTitle("نقاطي", for: .normal)
        
       //    backBtn.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
          //  backBtn.transform = CGAffineTransform(translationX: -1.0, y: 1.0)
          //  self.backBtn.frame.origin.x = 300
            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
          //  backBtn.contentHorizontalAlignment = .right

            //self.backBtn.semanticContentAttribute = .forceLeftToRight
            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)

        }
    }

    override func viewDidLoad() {
        self.transitioningDelegate = self
        super.viewDidLoad()
    //    UIView.appearance().semanticContentAttribute = .forceRightToLeft
        headerView.clipsToBounds = true
        headerView.roundCorners([ .bottomLeft], radius: 100)
        buttonsView.clipsToBounds = true
        buttonsView.roundCorners([ .bottomLeft,.topLeft], radius: 15)
      
        myPointBtn.clipsToBounds = true
        myPointBtn.roundCorners([ .bottomLeft,.topLeft,.topRight,.bottomRight], radius: 15)
        aboutBtn.clipsToBounds = true
        aboutBtn.roundCorners([ .bottomLeft,.topLeft,.topRight,.bottomRight], radius: 15)

        self.logo.sd_setImage(with: URL(string: (restaurant?.image!)!))
        if Constants.Settings.lang == "AR"
        {
            self.restTitle.text = restaurant?.nameAr
        }
        else
        {
            self.restTitle.text = restaurant?.name
        }
        self.logo.layer.borderWidth = 1
        self.logo.layer.borderColor = UIColor.white.cgColor
        self.logo.layer.cornerRadius = self.logo.height/2
        self.logo.clipsToBounds = true

       // headerView.layer.addSublayer(rectShape)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 
        if (segue.identifier == "RestaurantInfoViewController") {
            let secondViewController = segue.destination  as! RestaurantInfoViewController
            secondViewController.restaurant = self.restaurant
            // Pass data to secondViewController before the transition
        }
        if (segue.identifier == "MyPointViewController") {
            let secondViewController = segue.destination  as! MyPointViewController
            secondViewController.restaurant = self.restaurant
            // Pass data to secondViewController before the transition
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

