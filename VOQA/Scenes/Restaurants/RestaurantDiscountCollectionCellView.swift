//
//  RestaurantDiscountCollectionCellView.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/20/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class RestaurantDiscountCollectionCellView: UICollectionViewCell {
    @IBOutlet weak var restaurantImage: UIImageView!
    @IBOutlet weak var noOfVouchers: UILabel!
    
}
