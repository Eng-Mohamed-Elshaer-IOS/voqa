//
//  RestaurantInfoViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/12/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import UnderLineTextField
class RestaurantInfoViewController: UIViewController,UITextFieldDelegate {
    var restaurant : RestaurantsObject?
    @IBOutlet weak var phoneTextField: UnderLineTextField!
    @IBOutlet weak var addressTextField: UnderLineTextField!
    @IBOutlet weak var descTextView: UITextView!

    @objc func call(sender: UIButton) {
        guard let number = URL(string: "tel://" + phoneTextField.text!) else { return }
        UIApplication.shared.openURL(number)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    override func viewWillAppear(_ animated: Bool) {
//        self.phoneTextField.semanticContentAttribute = .forceRightToLeft
//        self.phoneTextField.textAlignment = .right
        if Constants.Settings.lang == "AR"
        {
            self.phoneTextField.placeholder = "رقم المحمول"
            self.addressTextField.placeholder = "العنوان"
        }
        
        
       // UIView.app.semanticContentAttribute = .forceRightToLeft
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneTextField.attributedPlaceholder = NSAttributedString(string: "Phone Number", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        addressTextField.attributedPlaceholder = NSAttributedString(string: "Address", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        phoneTextField.text = restaurant?.phone
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "call"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 10, 0)
        button.frame = CGRect(x: CGFloat(phoneTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(80), height: CGFloat(80))
        button.addTarget(self, action: #selector(RestaurantInfoViewController.call(sender:)), for: .touchUpInside)
        phoneTextField.rightView = button
        phoneTextField.rightViewMode = .always
        phoneTextField.delegate = self
       
        
        if Constants.Settings.lang == "AR"
        {
            if let desc = restaurant?.descriptionAr
            {
                descTextView.text = desc
            }
        }
        else
        {
            if let desc = restaurant?.description
            {
                descTextView.text = desc
            }
            
        }

        if let address = restaurant?.addresses![0]
        {
            self.addressTextField.text = address.address
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
