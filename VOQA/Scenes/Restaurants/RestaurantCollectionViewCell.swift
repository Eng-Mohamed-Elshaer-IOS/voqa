//
//  RestaurantCollectionViewCell.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class RestaurantCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var restaurantImage: UIImageView!
    @IBOutlet weak var restaurantTitle: UILabel!

}
