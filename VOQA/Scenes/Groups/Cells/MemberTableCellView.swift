//
//  MemberTableCellView.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/8/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

class MemberTableCellView: UITableViewCell {
    @IBOutlet weak var totalPointsLbl: UILabel!
    @IBOutlet weak var adminLbl: UILabel!

    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var totalPoint: UIButton!
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var add: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
