//
//  GroupCollectionViewCell.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/17/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class GroupCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var totalPoint: UIButton!
    @IBOutlet weak var groupName: UILabel!

}
