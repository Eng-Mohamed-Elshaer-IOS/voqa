//
//  GroupHeaderCellView.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/13/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
class GroupHeaderCellView: UITableViewCell {
    
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var addGroupBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
