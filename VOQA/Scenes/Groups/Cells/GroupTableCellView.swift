//
//  GroupTableCellViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/8/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//
import BubblePictures
class GroupTableCellView: UITableViewCell {
    @IBOutlet weak var onlyPics: UICollectionView!
    var bubblePictures: BubblePictures!

    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var totalPoint: UILabel!
    @IBOutlet weak var totalMember: UILabel!
    @IBOutlet weak var celNo: UILabel!
    @IBOutlet weak var totalPointsLbl: UILabel!

    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var arrow: UIImageView!

    @IBOutlet weak var groupCollection: UICollectionView!
    override func prepareForReuse()
    {
        super.prepareForReuse()
        // Reset the cell for new row's data
        self.groupName.text = ""
        self.totalPoint.text = ""
        self.totalMember.text = ""
        self.celNo.text = ""
        self.groupImage = nil
//        self.groupCollection = nil

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
extension GroupTableCellView: BPDelegate {
    func didSelectTruncatedBubble() {
        print("Selected truncated bubble")
    }
    
    func didSelectBubble(at index: Int) {
        print(index)
    }
}
