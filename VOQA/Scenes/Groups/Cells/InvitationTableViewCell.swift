//
//  InvitationTableViewCell.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/25/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class InvitationTableViewCell: UITableViewCell {
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var totalPoints: UILabel!
    @IBOutlet weak var totalPointsLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
