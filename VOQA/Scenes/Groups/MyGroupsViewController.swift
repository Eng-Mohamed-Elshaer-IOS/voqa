//
//  MyGroupsViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/13/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import BubblePictures

class MyGroupsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addGroupBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var backBtn: UIButton!

    var myMembers = [MyMember]()
    var myMembers1 = [MyMember]()
    var myIndex = Int()
    var indexFlag = 0

    let loadingHud = LoadingHud.shared.loadingHud
    var groups = [GroupsObject]()
    var groups1 = [GroupsObject]()
//    var group = Group()
//    var group1 = Group()
    var pictures: [String]  = [
        "https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/3/000/076/07f/0d01ca8.jpg",
        "http://static3.businessinsider.com/image/51700e6b69beddc119000015/this-31-year-old-advisor-wants-to-change-the-way-young-people-invest.jpg",
        "https://i.pinimg.com/736x/41/17/5b/41175b8393965fbac6d31e5b495065ee--pretty-people-beautiful-people.jpg",
        "https://www.getaround.com/img/public/rent/owner_bruno.jpg",
        "https://static.pexels.com/photos/355164/pexels-photo-355164.jpeg",
        "https://onehdwallpaper.com/wp-content/uploads/2016/11/Beautiful-Girls-Photos-Free-For-Download.jpg",
        "https://pbs.twimg.com/profile_images/1717956431/BP-headshot-fb-profile-photo_400x400.jpg"
    ]
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableView?.tableFooterView = UIView()
        self.getAllGroupsAsAdAPI()
        
        if Constants.Settings.lang == "AR"
        {
            //  self.addGroupBtn.setTitle("اضافة جديدة ", for: .normal)
            self.titleLbl.text = "مجموعاتي"
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            
            //            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
            //            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getAllGroupsAsAnyAPI()
    {
            let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
    //220
            loadingHud.show(in: self.view)
            let param: [String: Any] = [
                "user_id" : userId
    ]
            GroupsManager.getAllGroupsAsAny(body: param){ [unowned self] (groups1, error) in
                if error != nil {
                    self.loadingHud.dismiss()
                    self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                    return
                }
                self.loadingHud.dismiss()
    
                self.groups1.removeAll()
                self.groups1 = (groups1?.data)!
//                for group in self.groups
//                {
//                    // g
//                    self.getTotalMembersInGroupAPI1(groupId: group.groupId!)
//
//                }
                self.tableView.reloadData()
            }
    }
    func getAllGroupsAsAdAPI()
    {
        self.groups.removeAll()
        self.groups1.removeAll()
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        print("USER ID \(userId)")
        loadingHud.show(in: self.view)
        //220
        let param: [String: Any] = [
            "user_id" : userId
        ]
        GroupsManager.getAllGroupsAsAdmin(body: param){ [unowned self] (groups, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
           // self.groups.removeAll()
            self.groups = (groups?.data)!
//            for group in self.groups
//            {
//                // g
//                self.getTotalMembersInGroupAPI(groupId: group.groupId!)
//
//            }
            self.getAllGroupsAsAnyAPI()
          //  self.tableView.reloadData()
        }
    }
    func getAllGroupsAsAdminAPI()
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        loadingHud.show(in: self.view)
        //        let param: [String: Any] = [
        //            "user_id" : userId
        //            ]
        GroupsManager.getAllGroups{ [unowned self] (groups, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.groups.removeAll()
            self.groups = (groups?.data)!
            
            self.tableView.reloadData()
            
        }
        
    }
    @objc func addNewGroup(sender:UIButton)
    {
        
        let totalGroupsCount = self.myMembers.count + self.myMembers1.count
        if totalGroupsCount > 5
        {
            if Constants.Settings.lang == "AR"
            {
                
                let theAlert = UIAlertController(title: "اضافة مجموعة",
                                                 message: "لا يمكن اضافة اكتر من ٥ مجموعات ",
                                                 preferredStyle: UIAlertControllerStyle.alert)
                
                theAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(theAlert, animated: true, completion:nil)
                
            }
            else
            {
                let theAlert = UIAlertController(title: "Create Group",
                                                 message: "You can't add more than 5 groups",
                                                 preferredStyle: UIAlertControllerStyle.alert)
                
                theAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(theAlert, animated: true, completion:nil)
                
                
            }
        }
        else
        {
            let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "addGroup") as! CreateGroupViewController
            viewController.editOrCreate = "CREATE"
            self.present(viewController, animated: true, completion: nil)
        }

    }
    func getTotalMembersInGroupAPI(groupId:Int)
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        //loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "group_id" : groupId
        ]
        GroupsManager.getMembersInGroup(body: param) { [unowned self] (members, error) in
            if error != nil {
                // self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            let mem = MyMember()
            mem.totalMembers = members?.data?.count
            print("DATA \(members?.data)")
            for mems in (members?.data)!
            {
                if mems.image != ""
                {
                    let url = URL(string: mems.image!)
                    print("SSSSSSS \(url!)")
                    if let data = try? Data(contentsOf: url!)
                    {
                        let image: UIImage = UIImage(data: data)!
                        mem.imageUrl.append(image)
                        
                    }
                }
                else
                {
                    
                    let image: UIImage = #imageLiteral(resourceName: "user")
                    mem.imageUrl.append(image)
                    
                    
                }
                
            }
            
            self.myMembers.append(mem)
            //  mem.imageUrls =
            //            self.groups.removeAll()
            //            self.groups = (groups?.data)!
            
            self.tableView.reloadData()
            
            //    self.tableView.reloadData()
            
        }
        
    }
    func getTotalMembersInGroupAPI1(groupId:Int)
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        //loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "group_id" : groupId
        ]
        GroupsManager.getMembersInGroup(body: param) { [unowned self] (members, error) in
            if error != nil {
                // self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            let mem = MyMember()
            mem.totalMembers = members?.data?.count
            print("DATA \(members?.data)")
            for mems in (members?.data)!
            {
                if mems.image != ""
                {
                    let url = URL(string: mems.image!)
                    print("SSSSSSS \(url!)")
                    if let data = try? Data(contentsOf: url!)
                    {
                        let image: UIImage = UIImage(data: data)!
                        mem.imageUrl.append(image)
                        
                    }
                }
                else
                {
                    
                    let image: UIImage = #imageLiteral(resourceName: "user")
                    mem.imageUrl.append(image)
                    
                    
                }
                
            }
            
            self.myMembers1.append(mem)
            //  mem.imageUrls =
            //            self.groups.removeAll()
            //            self.groups = (groups?.data)!
            
            self.tableView.reloadData()
            
            //    self.tableView.reloadData()
            
        }
        
    }
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! GroupHeaderCellView
        
        switch (section) {
        case 0:
            headerCell.titleName.text = "Groups You Manage"
            headerCell.addGroupBtn.isHidden = false
            headerCell.addGroupBtn.addTarget(self, action:#selector(MyGroupsViewController.addNewGroup(sender:)), for: .touchUpInside)
            if Constants.Settings.lang == "AR"
            {
                headerCell.addGroupBtn.setTitle("اضف ", for: .normal)
                headerCell.titleName.text = "ادارة المجموعات"

              //  self.titleLbl.text = "مجموعاتي"
                //            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
                //            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            }
        //return sectionHeaderView
        case 1:
            headerCell.titleName.text = "Your Groups"
            headerCell.addGroupBtn.isHidden = true
            if Constants.Settings.lang == "AR"
            {
                headerCell.titleName.text = "مجموعاتي"
            }
        //return sectionHeaderView
        default:
            headerCell.titleName.text = ""
        }
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return groups.count
        }
        else
        {
            return groups1.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupCell", for: indexPath) as! GroupTableCellView

        if indexPath.section == 0
        {

            if Constants.Settings.lang == "AR"
            {
                // self.view.semanticContentAttribute = .forceRightToLeft
                cell.arrow.rotate(byAngle: 180, ofType: AngleUnit.degrees)
                cell.totalMember.text = "\((self.groups[indexPath.row].users?.count)!) اعضاء "
                cell.totalMember.textAlignment = .right
                cell.groupName.textAlignment = .right
                cell.totalPointsLbl.text = "مجموع النقاط"

            }
            else
            {
                cell.totalMember.text = "\((self.groups[indexPath.row].users?.count)!) Members"
                
            }
                myIndex = indexPath.row
                indexFlag = 0
                cell.groupName.text = groups[indexPath.row].name
//              cell.groupImage.sd_setImage(with: URL(string:groups[indexPath.row].image!))
           
            if  groups[indexPath.row].image == nil {
                
            } else {
                 cell.groupImage.sd_setImage(with: URL(string:"http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public/\(groups[indexPath.row].image!)"),placeholderImage: UIImage(named: "Group 3599"))
                cell.groupImage.cornerRadius = cell.groupImage.height/2
            }
            
                cell.celNo.text="\(indexPath.row+1)"

                cell.totalPoint.text = "\(groups[indexPath.row].totalPoints!)"
          //  cell.totalMember.text = "\((groups[indexPath.row].users?.count)!) Members"
            
            let layoutConfigurator = BPLayoutConfigurator(
                backgroundColorForTruncatedBubble: UIColor.lightGray,
                fontForBubbleTitles: UIFont(name: "HelveticaNeue-Light", size: 16.0)!,
                colorForBubbleBorders: UIColor.darkGray,
                colorForBubbleTitles: UIColor.white,
                maxCharactersForBubbleTitles: 2,
                maxNumberOfBubbles: 4,
                direction: .leftToRight,
                alignment: .center
            )
            var array = [BPCellConfigFile]()
            for item in self.groups[indexPath.row].users!
            {
                if let memberImage = item.image
                {
                    if memberImage != ""
                    {
                        print("IMAAAAAAAAA \(memberImage)")
                        let im = BPCellConfigFile(imageType: BPImageType.URL(URL(string:memberImage)!),title: "")
                        array.append(im)
                    }
                    else
                    {
                        let im = BPCellConfigFile(imageType: BPImageType.image(#imageLiteral(resourceName: "user")),title: "")
                        array.append(im)
                    }
                }
                else
                {
                    let im = BPCellConfigFile(imageType: BPImageType.image(#imageLiteral(resourceName: "user")),title: "")
                    array.append(im)
                    
                }
                
            }
            print("IMAAAAAAAAA COUNT \(array.count)   \(cell.onlyPics)")
            
            cell.bubblePictures = BubblePictures(collectionView: cell.onlyPics, configFiles: array, layoutConfigurator: layoutConfigurator)
            cell.bubblePictures.delegate = cell


            
        
        }
        else
        {
 
                myIndex = indexPath.row
                indexFlag = 1
            if Constants.Settings.lang == "AR"
            {
                // self.view.semanticContentAttribute = .forceRightToLeft
                cell.arrow.rotate(byAngle: 180, ofType: AngleUnit.degrees)
                cell.totalMember.text = "\((self.groups1[indexPath.row].users?.count)!) اعضاء "
                cell.totalMember.textAlignment = .right
                cell.groupName.textAlignment = .right
                cell.totalPointsLbl.text = "مجموع النقاط"

            }
            else
            {
                cell.totalMember.text = "\((self.groups1[indexPath.row].users?.count)!) Members"
                
            }
            cell.groupName.text = groups1[indexPath.row].name
            // cell.groupImage.sd_setImage(with: URL(string:groups[indexPath.row].image!))
            cell.groupImage.cornerRadius = cell.groupImage.height/2
            cell.celNo.text="\(indexPath.row+1)"
            
            cell.totalPoint.text = "\(groups1[indexPath.row].totalPoints!)"
            //    cell.totalMember.text = "\((groups1[indexPath.row].users?.count)!) Members"
            
            let layoutConfigurator = BPLayoutConfigurator(
                backgroundColorForTruncatedBubble: UIColor.gray,
                fontForBubbleTitles: UIFont(name: "HelveticaNeue-Light", size: 16.0)!,
                colorForBubbleBorders: UIColor.darkGray,
                colorForBubbleTitles: UIColor.white,
                maxCharactersForBubbleTitles: 2,
                maxNumberOfBubbles: 4,
                direction: .leftToRight,
                alignment: .center
            )
            var array = [BPCellConfigFile]()
            for item in self.groups1[indexPath.row].users!
            {
                if let memberImage = item.image
                {
                    if memberImage != ""
                    {
                        print("IMAAAAAAAAA \(memberImage)")
                        let im = BPCellConfigFile(imageType: BPImageType.URL(URL(string:memberImage)!),title: "")
                        array.append(im)
                    }
                    else
                    {
                        let im = BPCellConfigFile(imageType: BPImageType.image(#imageLiteral(resourceName: "user")),title: "")
                        array.append(im)
                    }
                }
                else
                {
                    let im = BPCellConfigFile(imageType: BPImageType.image(#imageLiteral(resourceName: "user")),title: "")
                    array.append(im)
                    
                }
                
            }
            print("IMAAAAAAAAA COUNT \(array.count)   \(cell.onlyPics)")
            
            cell.bubblePictures = BubblePictures(collectionView: cell.onlyPics, configFiles: array, layoutConfigurator: layoutConfigurator)
            cell.bubblePictures.delegate = cell
        }
        return cell

        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "groupDetails") as! GroupDetailsViewController
        //  self.navigationController?.pushViewController(viewController, animated: true)
        if indexPath.section == 0
        {
            viewController.editFlag = 0
            viewController.groupName = "\(groups[indexPath.row].name!)"
            viewController.groupId = groups[indexPath.row].groupId!
            viewController.totalPointsStr = "\(groups[indexPath.row].totalPoints!)"

        }
        else
        {
            viewController.editFlag = 1
            viewController.groupName = "\(groups1[indexPath.row].name!)"
            viewController.groupId = groups1[indexPath.row].groupId!
            viewController.totalPointsStr = "\(groups1[indexPath.row].totalPoints!)"


        }
        self.present(viewController, animated: true, completion: nil)
        
    }
    //    func getAllGroupsAsAnyAPI()
    //    {
    //        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
    //
    //        loadingHud.show(in: self.view)
    //        let param: [String: Any] = [
    //            "user_id" : userId
    //        ]
    //        GroupsManager.getAllGroupsAsAny(body: param){ [unowned self] (groups, error) in
    //            if error != nil {
    //                self.loadingHud.dismiss()
    //                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
    //                return
    //            }
    //            self.loadingHud.dismiss()
    //
    //            self.groups1.removeAll()
    //            self.groups1 = (groups?.data)!
    //
    //
    //            self.tableView.reloadData()
    //
    //        }
    //
    //    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
