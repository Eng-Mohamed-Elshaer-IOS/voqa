//
//  GroupDetailsViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class GroupDetailsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var backBtn: UIButton!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!

    @IBOutlet weak var totalPoints: UILabel!
    @IBOutlet weak var totalPointsLbl: UILabel!

    @IBOutlet weak var groupTitle: UILabel!
    var membersList = [String: Int]()

    @IBOutlet weak var groupImageView: UIImageView!

    let loadingHud = LoadingHud.shared.loadingHud
    var groupId = Int()
    var editFlag = Int()
    var totalPointsStr = String()
    var groupName = String()

    var members = [MemberObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.tableFooterView = UIView()
        self.groupTitle.text = self.groupName
        if editFlag == 0
        {
            editBtn.isHidden = false
          //  deleteBtn.isHidden = false
            deleteBtn.setTitle(" Delete", for: .normal)
        }
        else if editFlag == 1
        {
            editBtn.isHidden = true
            deleteBtn.setTitle(" Leave", for: .normal)

         //    deleteBtn.isHidden = true

        }
        self.totalPoints.text = totalPointsStr
        self.getMembersInAGroupAPI()
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "تفاصيل المجموعة"
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            self.totalPoints.textAlignment = .right
           self.totalPointsLbl.text = "مجموع النقاط"
            //            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
            //            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func deleteBtn(_ sender: UIButton) {
        if self.editFlag == 0
        {
            self.deleteAPI()

        }
        else
        {
            self.leaveGroupAPI()

        }

    }
    func deleteAPI()
    {
        loadingHud.show(in: self.view)
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)

        let param: [String: Any] = [
            "user_id" : userId,
            "group_id": self.groupId
        ]
        
        
        GroupsManager.deleteGroup(body: param) { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.dismiss(animated: true, completion: nil)
            
            
            return
        }
    }
    func leaveGroupAPI()
    {
        loadingHud.show(in: self.view)
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)

        let param: [String: Any] = [
            "group_id" : groupId,
            "user_id" : userId
            
        ]
        
        
        GroupsManager.leaveGroup(body: param) { (response, error) in
            if error != nil {
                
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            
            self.loadingHud.dismiss()
            self.dismiss(animated: true, completion: nil)

            //  self.groupId = Int((response?.1)!)!
            // self.inviteMembersToGroupAPI()
            //   self.dismiss(animated: true, completion: nil)
            
            return
        }
        
        
    }
    @IBAction func back(_ sender: UIButton) {
        self.membersList.removeAll()
        UserDefaults.standard.set(self.membersList, forKey: "MEMBERS")

        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func editBtnClicked(_ sender: UIButton) {
        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "addGroup") as! CreateGroupViewController
        viewController.editOrCreate = "EDIT"
        viewController.groupName = self.groupName
        viewController.groupId = self.groupId
        self.present(viewController, animated: true, completion: nil)
        
    }
    func getMembersInAGroupAPI()
    {
        
        loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "group_id" : groupId
        ]
        GroupsManager.getMembersInGroup(body: param) { [unowned self] (members, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.members = (members?.data)!
            self.loadingHud.dismiss()

            for member in self.members
            {
                self.membersList[member.name!] = member.memberId
//                self.membersList[members[sender.tag].name!] = members[sender.tag].memberId

            }
            UserDefaults.standard.set(self.membersList, forKey: "MEMBERS")

            self.tableView.reloadData()
            
            //    self.tableView.reloadData()
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView
    {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! GroupHeaderCellView
        if Constants.Settings.lang == "AR"
        {
            headerCell.titleName.text = "الاعضاء"
        }
        
        return headerCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)

        let cell = tableView.dequeueReusableCell(withIdentifier: "memberCell", for: indexPath) as! MemberTableCellView

        let object = members[indexPath.row]
        cell.memberName.text = object.name
        
        // Show the leave button if our member or admin.
        let inMember = object.name
        let prefs = UserDefaults.standard
        let keyValue = prefs.string(forKey:"UserName")
        print("\(String(describing: inMember))")
        print("\(String(describing: keyValue))")
        if inMember == keyValue {
            deleteBtn.isHidden = false
        }
    
        cell.groupImage.cornerRadius = cell.groupImage.height/2
        cell.totalPoint.setTitle("\(object.totalPoints!)", for: .normal)
        if userId.intValue == object.memberId!
        {
            cell.adminLbl.isHidden = true
        }
        else
        {
            cell.adminLbl.isHidden = true
        }
        if Constants.Settings.lang == "AR"
        {
            cell.totalPointsLbl.textAlignment = .right
            cell.memberName.textAlignment = .right
            cell.totalPointsLbl.text = "مجموع النقاط"
        }
        if object.image != ""
        {
            let url = URL(string: object.image!)
            print("SSSSSSS \(url!)")
            if let data = try? Data(contentsOf: url!)
            {
                let image: UIImage = UIImage(data: data)!
               // mem.imageUrl.append(image)
                cell.groupImage.image = image
                
            }
        }
        
        
        
        return cell
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
