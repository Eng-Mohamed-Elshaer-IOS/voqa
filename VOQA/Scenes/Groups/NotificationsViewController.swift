//
//  NotificationsViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/25/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var backBtn: UIButton!

    let loadingHud = LoadingHud.shared.loadingHud
    var invitations = [InvitationObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.tableFooterView = UIView()
        self.getAllNotificationsAPI()
        if Constants.Settings.lang == "AR"
        {
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)

            self.titleLbl.text = "الاشعارات"
            //            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
            //            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func getAllNotificationsAPI()
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        //701
        print("USER \(userId)")
        loadingHud.show(in: self.view)
                let param: [String: Any] = [
                    "user_id" : userId
                    ]

        GroupsManager.getAllPendingNotification(body: param){ [unowned self] (invitations, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.invitations.removeAll()
            self.invitations = (invitations?.data)!
            
            self.tableView.reloadData()
            
        }
        
    }
    func acceptInviteAPI(inviteId:Int)
    {
      //  let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "group_user_id" : inviteId
        ]
        
        
        GroupsManager.accpetInvite(body: param) { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()

            if response?.0 == false
            {
                self.present(UIViewHelper.errorAlert(title: "Error", message: (response?.1)!), animated: true, completion: nil)
            }
            else
            {
                self.loadingHud.dismiss()
                self.tableView.reloadData()
                self.loadingHud.dismiss()
            }
            
            
            return
        }
        self.getAllNotificationsAPI()
    }
    
    func rejectInviteAPI(inviteId:Int)
    {
        //  let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "group_user_id" : inviteId
        ]
        
        
        GroupsManager.rejectInvite(body: param) { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }

            self.getAllNotificationsAPI()
            self.tableView.reloadData()
            self.loadingHud.dismiss()
            return
        }
        self.getAllNotificationsAPI()
    }
    
    @objc func accceptBtnClicked(sender:UIButton)
    {
        self.acceptInviteAPI(inviteId: sender.tag)
    }
    @objc func rejectBtnClicked(sender:UIButton)
    {
        self.rejectInviteAPI(inviteId: sender.tag)
//        let point = sender.convert(CGPoint.zero, to: tableView)
//        guard let indexPath = tableView.indexPathForRow(at: point) else {
//            return
//        }
//        tableView.deleteRows(at: [indexPath], with: .left)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return invitations.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.05 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
            let cell = tableView.dequeueReusableCell(withIdentifier: "invitationCell", for: indexPath) as! InvitationTableViewCell
            print(invitations[indexPath.row].group)
        cell.groupName.text = invitations[indexPath.row].group?.name
        cell.acceptBtn.tag =  invitations[indexPath.row].invitationId!
        cell.rejectBtn.tag =  invitations[indexPath.row].invitationId!
        tableView.deleteRows(at: [indexPath], with: .left)

        cell.acceptBtn.addTarget(self, action:#selector(NotificationsViewController.accceptBtnClicked(sender:)), for: .touchUpInside)
        cell.rejectBtn.addTarget(self, action:#selector(NotificationsViewController.rejectBtnClicked(sender:)), for: .touchUpInside)

            // cell.groupImage.sd_setImage(with: URL(string:groups[indexPath.row].image!))
        cell.groupImage.cornerRadius = cell.groupImage.height/2
       // cell.totalPoints.text = "\( (invitations[indexPath.row].group?.totalPoints)!)"
          //  cell.celNo.text="\(indexPath.row+1)"
        if Constants.Settings.lang == "AR"
        {
            // self.view.semanticContentAttribute = .forceRightToLeft
            cell.totalPointsLbl.text = "مجموع النقاط"
            
        }
            //cell.totalPoint.text = "\(groups[indexPath.row].totalPoints!)"
            
            return cell
      
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
