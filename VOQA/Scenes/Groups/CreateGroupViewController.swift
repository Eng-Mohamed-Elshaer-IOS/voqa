//
//  CreateGroupViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import ALCameraViewController
import SDWebImage

class CreateGroupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var groupHeader: UILabel!
    @IBOutlet weak var inviteHeader: UILabel!
    @IBOutlet weak var searchHeader: UILabel!
    @IBOutlet weak var imageHeader: UILabel!

    @IBOutlet var backBtn: UIButton!

    @IBOutlet weak var groupNameTextField: UITextField!
    
    @IBOutlet weak var profileImage: UIButton!
    @IBOutlet weak var generateLinkBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var searchMemberBtn: UIButton!

    @IBOutlet weak var table1Height1: NSLayoutConstraint!
    @IBOutlet weak var table1Height2: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var tableView2: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var myView: UIView!
    var groupId = Int()
    var editOrCreate = String()
    var groupName = String()

    let loadingHud = LoadingHud.shared.loadingHud
    
    var membersList = [String: Int]()
    var userIds = String()
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tableView2
        {
            return self.membersList.count
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let object = Array(self.membersList.keys)[indexPath.row]
            let object1 = Array(self.membersList.values)[indexPath.row]

            if self.editOrCreate == "CREATE"
            {
                let result = self.membersList.removeValue(forKey: object)

            }
            else
            {
                self.leaveGroupAPI(userId: object1,userName:object)
            }
            
            print("RESULT \(self.membersList)")
            self.tableView2.reloadData()
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "memberCell", for: indexPath) as! MemberTableCellView
        
        let object = Array(self.membersList.keys)[indexPath.row]
        cell.memberName.text = object
  //      cell.groupImage.sd_setImage(with: URL(string:members[indexPath.row].image!))
        cell.groupImage.cornerRadius = cell.groupImage.height/2
        
        //cell.totalPoint.text = "\(groups[indexPath.row].totalPoints!)"
        
        return cell

    }

    override func viewDidLayoutSubviews() {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: self.view.height)

    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
  
        if editOrCreate == "EDIT"
        {
            self.generateLinkBtn.isEnabled = true
            self.groupNameTextField.text = self.groupName
        }
        else
        {
            self.generateLinkBtn.isEnabled = false
        }
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "انشاء المجموعة"
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            self.groupNameTextField.textAlignment = .right
            self.groupHeader.textAlignment = .right
            self.inviteHeader.textAlignment = .right
            self.searchHeader.textAlignment = .right
            self.imageHeader.textAlignment = .right
            
            self.groupHeader.text = "اسم المجموعة"
            self.inviteHeader.text = "اضف صديق"
            self.searchHeader.text = "بحث عن مستخدم"
            self.imageHeader.text = "صورة المجموعة"
            self.saveBtn.setTitle("حفظ", for: .normal)
            self.searchMemberBtn.setTitle("   البحث عن مستدخدم ", for: .normal)
            self.generateLinkBtn.setTitle("   اضافة صديق     ", for: .normal)
            //  self.searchMemberBtn.setTitle(" ffffffff ", for: .normal)
            self.generateLinkBtn.contentHorizontalAlignment = .right
            self.searchMemberBtn.contentHorizontalAlignment = .right
            
            //self.searchMemberBtn.setTitle(" fffffffff", for: <#T##UIControlState#>)
            //            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
            //            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
        }

        self.table1Height1.constant = 0
        self.profileImage.cornerRadius = self.profileImage.height/2
        self.groupNameTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.contains(key: "MEMBERS")
        {
            self.membersList = UserDefaults.standard.dictionary(forKey: "MEMBERS") as! [String : Int]
            tableView2.reloadData()

           // if(self.tableView2.contentSize.height < self.tableView2.frame.height){
//                var frame: CGRect = self.tableView2.frame
//                frame.size.height = 500// self.tableView2.contentSize.height
//                self.tableView2.frame = frame
                            var frame: CGRect = self.myView.frame
            var frame1: CGRect = self.view.frame

                            frame.size.height =  self.myView.height - 500
                            frame1.size.height =  self.view.height - 500
//            myView.frame = CGRect(x: self.myView.x,y: self.myView.y,width: myView.width,height: myView.height+500)

               //             self.myView.frame = frame
         //   self.view.frame = frame1

            self.table1Height2.constant = CGFloat(44 * self.membersList.count)
            self.viewHeight.constant = myView.height + CGFloat(44 * self.membersList.count)
                    //self.tableView2.contentSize.height
        //    }
            scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height:myView.height + CGFloat(44 * self.membersList.count))

        }
        else
        {
            self.table1Height2.constant = 0
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: UIButton) {
        self.membersList.removeAll()
        UserDefaults.standard.set(self.membersList, forKey: "MEMBERS")

        self.dismiss(animated: true, completion: nil)
     //   UserDefaults.standard()

    }
    @IBAction func searchMember() {

        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "searchMember") as! SearchMemberViewController
        self.present(viewController, animated: true, completion: nil)

    }
    @IBAction func createGroup() {
        if (self.groupNameTextField.text?.count)!>20 || (self.groupNameTextField.text?.count)!==0
        {
            if Constants.Settings.lang == "AR"
            {
                
                let theAlert = UIAlertController(title: "اضافة مجموعة",
                                                 message: "الرجاء ادخال اسم مجموعة لا تزيد عن ٢٠ حرف",
                                                 preferredStyle: UIAlertControllerStyle.alert)
                
                theAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(theAlert, animated: true, completion:nil)
                
            }
            else
            {
                let theAlert = UIAlertController(title: "Create Group",
                                                 message: "The group name should be less than 20 characters",
                                                 preferredStyle: UIAlertControllerStyle.alert)
                
                theAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(theAlert, animated: true, completion:nil)
                
                
            }
        }
        else
        {
            if self.editOrCreate == "CREATE"
            {
                self.createGroupAPI()
            }
            else
            {
                self.editGroupAPI()
            }
        }
    }
    @IBAction func createLink()
    {
        if self.editOrCreate == "EDIT"
        {
            self.generateLinkAPI()
        }
        else
        {
            if Constants.Settings.lang == "AR"
            {
                
                let theAlert = UIAlertController(title: "اضافة مجموعة",
                                                 message: "يجب اضافة مجموعة اولا لكي تستطيع دعوة اصدقائك",
                                                 preferredStyle: UIAlertControllerStyle.alert)
                
                theAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(theAlert, animated: true, completion:nil)

            }
            else
            {
                let theAlert = UIAlertController(title: "Create Group",
                                                 message: "You need to create group first in order to invite friends",
                                                 preferredStyle: UIAlertControllerStyle.alert)
                
                theAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(theAlert, animated: true, completion:nil)

                
            }
        }
    }
    @IBAction func selectPhoto() {
        let cropSettings = CroppingParameters(isEnabled: true, allowResizing: false, allowMoving: true, minimumSize: CGSize(width: 200, height: 350))
        
        let cameraViewController = CameraViewController(croppingParameters: cropSettings, allowsLibraryAccess: true, allowsSwapCameraOrientation: true, allowVolumeButtonCapture: true) { (image, phAsset) in
            
            if image != nil {
                self.profileImage.setImage(image, for: .normal)
            //    self.updateImage()
            }
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
        self.present(cameraViewController, animated: true, completion: nil)
    }
    func getUserImg(image: UIImage) -> String {
        let jpegCompressionQuality: CGFloat = 0.9 // Set this to whatever suits your purpose
        let correctedImage:UIImage = profileImage.imageView!.image!.fixOrientation()
        if let base64String = UIImageJPEGRepresentation(correctedImage, jpegCompressionQuality)?.base64EncodedString() {
            return base64String
        }
        return ""
    }
    func shareLink(link:String) {
        let vc = UIActivityViewController(activityItems: [link], applicationActivities: [])
        present(vc, animated: true)
    }
    func createGroupAPI()
    {
        loadingHud.show(in: self.view)
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        let imageBase64 = getUserImg(image: (self.profileImage.imageView?.image!)!)

        let param: [String: String] = [
            "name" : self.groupNameTextField.text!,
            "user_id" : "\(userId)"
        ]
       print("PARAM \(param)")
       // UIImagePNGRepresentation(<#T##image: UIImage##UIImage#>)
        let imgData = UIImageJPEGRepresentation((self.profileImage.imageView?.image)!, 0.5)
        GroupsManager.createGroup(imgData:imgData!,body: param) { (response, error) in
            if error != nil {
                print("ERROR \(error)")
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            self.groupId = Int((response?.1)!)!
            
            if self.membersList.count>0
            {
             self.inviteMembersToGroupAPI()
            }
            else
            {
                if self.editOrCreate == "CREATE"
                {
                   self.dismiss(animated: true, completion: nil)
                }
                else  if self.editOrCreate == "EDIT"
                {
                    //                let viewController = self.presentingViewController as! GroupDetailsViewController
                    //                viewController.titleLbl.text = self.groupNameTextField.text!
                    //                viewController.groupImageView = self.profileImage.imageView
                    //  self.dismiss(animated: true, completion: nil)
                    var presentingViewController: GroupDetailsViewController! = self.presentingViewController as! GroupDetailsViewController
                    
                    self.dismiss(animated: false) {
                        // go back to MainMenuView as the eyes of the user
                        presentingViewController?.dismiss(animated: false, completion: nil)
                    }
                }
            }
         //   self.dismiss(animated: true, completion: nil)
            
          //  self.tableView.reloadData()
            return
        }
            
            
    }
    func editGroupAPI()
    {
        loadingHud.show(in: self.view)
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        let imageBase64 = getUserImg(image: (self.profileImage.imageView?.image!)!)
        
        let param: [String: String] = [
            "name" : self.groupNameTextField.text!,
            "user_id" : "\(userId)",
            "group_id" : "\(groupId)"

        ]
        let imgData = UIImageJPEGRepresentation((self.profileImage.imageView?.image)!, 0.5)

        
        GroupsManager.editGroup(imgData:imgData!,body: param) { (response, error) in
            if error != nil {
                
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
         //   self.groupId = Int((response?.1)!)!
            if self.membersList.count>0
            {
                self.inviteMembersToGroupAPI()
            }
            else
            {
                if self.editOrCreate == "CREATE"
                {
                    self.dismiss(animated: true, completion: nil)
                }
                else  if self.editOrCreate == "EDIT"
                {
                    //                let viewController = self.presentingViewController as! GroupDetailsViewController
                    //                viewController.titleLbl.text = self.groupNameTextField.text!
                    //                viewController.groupImageView = self.profileImage.imageView
                    //  self.dismiss(animated: true, completion: nil)
                    var presentingViewController: GroupDetailsViewController! = self.presentingViewController as! GroupDetailsViewController
                    
                    self.dismiss(animated: false) {
                        // go back to MainMenuView as the eyes of the user
                        presentingViewController?.dismiss(animated: false, completion: nil)
                    }
                }
            }
            //   self.dismiss(animated: true, completion: nil)
            
            //  self.tableView.reloadData()
            return
        }
        
        
    }
    func generateLinkAPI()
    {
        loadingHud.show(in: self.view)
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)

        let param: [String: Any] = [
            "group_id" : groupId,
            "user_id" : 982
            
        ]
        
        
        GroupsManager.generateLink(body: param) { (response, error) in
            if error != nil {
                
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
          //  let result = self.membersList.removeValue(forKey: userName)

            self.loadingHud.dismiss()
            self.shareLink(link: (response?.1)!)
          //  self.groupId = Int((response?.1)!)!
           // self.inviteMembersToGroupAPI()
            //   self.dismiss(animated: true, completion: nil)
            
              //self.tableView2.reloadData()
            return
        }
        
        
    }
    func leaveGroupAPI(userId:Int,userName:String)
    {
        loadingHud.show(in: self.view)
        
        let param: [String: Any] = [
            "group_id" : groupId,
            "user_id" : userId
            
        ]
        
        
        GroupsManager.leaveGroup(body: param) { (response, error) in
            if error != nil {
                
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            let result = self.membersList.removeValue(forKey: userName)
            
            self.loadingHud.dismiss()
            //  self.groupId = Int((response?.1)!)!
            // self.inviteMembersToGroupAPI()
            //   self.dismiss(animated: true, completion: nil)
            
            self.tableView2.reloadData()
            return
        }
        
        
    }
    func inviteMembersToGroupAPI()
    {
        loadingHud.show(in: self.view)
        
        for item in self.membersList
        {
            self.userIds.append("\(item.value),")
            
        }

        print("INVITE \(self.userIds)   \(self.groupId)")
        let imageBase64 = getUserImg(image: (self.profileImage.imageView?.image!)!)
        
        let param: [String: Any] = [
            "user_ids" : userIds,
            "group_id": self.groupId
            
        ]
        
        
        GroupsManager.inviteMembersToGroup(body: param) { (response, error) in
            if error != nil {
                
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            self.membersList.removeAll()
            UserDefaults.standard.set(self.membersList, forKey: "MEMBERS")
            if self.editOrCreate == "CREATE"
            {
                self.dismiss(animated: true, completion: nil)

            }
            else  if self.editOrCreate == "EDIT"
            {
//                let viewController = self.presentingViewController as! GroupDetailsViewController
//                viewController.titleLbl.text = self.groupNameTextField.text!
//                viewController.groupImageView = self.profileImage.imageView
              //  self.dismiss(animated: true, completion: nil)
                var presentingViewController: GroupDetailsViewController! = self.presentingViewController as! GroupDetailsViewController

                self.dismiss(animated: false) {
                    // go back to MainMenuView as the eyes of the user
                    presentingViewController?.dismiss(animated: false, completion: nil)
                }
            }
            //  self.tableView.reloadData()
            return
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UserDefaults {
    func contains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}
