//
//  SearchMemberViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/27/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class SearchMemberViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var saveBtn: UIButton!

    let loadingHud = LoadingHud.shared.loadingHud
    var members = [MemberObject]()
    var membersList = [String: Int]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.tableView?.tableFooterView = UIView()
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "البحث"
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            self.searchTextField.placeholder = "البحث بالبريد الالكتروني او اسم المستخدم "
            self.searchTextField.adjustsFontSizeToFitWidth = true
            self.searchTextField.textAlignment = .right
           self.saveBtn.setTitle("حفظ", for: .normal)
            //            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
            //            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
        }
  //      self.getSearchResults()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveMemebers(sender:UIButton) {

        UserDefaults.standard.set(self.membersList, forKey: "MEMBERS")
        self.dismiss(animated: true, completion: nil)
    }
    @objc func addMember(sender:UIButton)
    {
        self.membersList[members[sender.tag].name!] = members[sender.tag].memberId
        self.tableView.reloadData()
      //  self.membersList.val
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "memberCell", for: indexPath) as! MemberTableCellView

        print("CELL \(members[indexPath.row].name!)   \(self.membersList[members[indexPath.row].name!])  \(indexPath.row)")
        if let _ = self.membersList[members[indexPath.row].name!]  {
            cell.add.alpha = 0.5
            cell.add.isEnabled = false
            // now val is not nil and the Optional has been unwrapped, so use it
        }
        else
        {
            cell.add.alpha = 1
            cell.add.isEnabled = true
        }
        if Constants.Settings.lang == "AR"
        {
                cell.add.setTitle("اضف", for: .normal)
            cell.memberName.textAlignment = .right
            
        }
        cell.memberName.text = members[indexPath.row].name
         cell.groupImage.sd_setImage(with: URL(string:members[indexPath.row].image!))
        cell.groupImage.cornerRadius = cell.groupImage.height/2
        cell.add.tag = indexPath.row
        //  cell.celNo.text="\(indexPath.row+1)"
        cell.add.addTarget(self, action:#selector(SearchMemberViewController.addMember(sender:)), for: .touchUpInside)

        //cell.totalPoint.text = "\(groups[indexPath.row].totalPoints!)"
        
        return cell
    }
    @objc func getSearchResults()
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        loadingHud.show(in: self.view)
        let param: [String: Any] = [
            "user_id" : 31,
            "group_id" : -1,
            "keyword" : self.searchTextField.text!
        ]
        print("PARAM \(param)")
        GroupsManager.searchMemeber(body: param) { [unowned self] (members, error) in
            if error != nil {
                print("ERRROR \(error?.localizedDescription)")
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            if let mem = members?.data
            {
                self.members = mem
            }
            print("DATA \(members?.data)")
       
            
            
            self.tableView.reloadData()
            
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    @objc func textFieldDidChange(textField: UITextField){
        
           // self.getSearchResults()
        var timer = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(SearchMemberViewController.getSearchResults),
            userInfo: ["textField": textField],
            repeats: false)
        print("Text changed")
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
