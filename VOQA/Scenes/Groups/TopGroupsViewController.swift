//
//  TopGroupsViewController.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/12/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import OnlyPictures
import BubblePictures

class TopGroupsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var backBtn: UIButton!

    let loadingHud = LoadingHud.shared.loadingHud
    var myIndex = Int()
    var groups = [GroupsObject]()
    var members = [MemberObject]()
   // var membersObj = MemberObject()
//    private var bubblePictures: BubblePictures!

    var myMembers = [MyMember]()

    var pictures: [String]  = [
        "https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/3/000/076/07f/0d01ca8.jpg",
        "http://static3.businessinsider.com/image/51700e6b69beddc119000015/this-31-year-old-advisor-wants-to-change-the-way-young-people-invest.jpg",
        "https://i.pinimg.com/736x/41/17/5b/41175b8393965fbac6d31e5b495065ee--pretty-people-beautiful-people.jpg",
        "https://www.getaround.com/img/public/rent/owner_bruno.jpg",
        "https://static.pexels.com/photos/355164/pexels-photo-355164.jpeg",
        "https://onehdwallpaper.com/wp-content/uploads/2016/11/Beautiful-Girls-Photos-Free-For-Download.jpg",
        "https://pbs.twimg.com/profile_images/1717956431/BP-headshot-fb-profile-photo_400x400.jpg"
    ]
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.tableFooterView = UIView()
        self.getAllGroupsAsAdminAPI()
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "افضل مجموعة"

                // self.view.semanticContentAttribute = .forceRightToLeft
                self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
        
//            self.backBtnConstraint?.constant = screenWidth -  (self.backBtnConstraint?.constant)! - 20
//            backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getAllGroupsAsAdminAPI()
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        loadingHud.show(in: self.view)
//        let param: [String: Any] = [
//            "user_id" : userId
//            ]
        GroupsManager.getAllGroups{ [unowned self] (groups, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            
            self.groups.removeAll()
            self.groups = (groups?.data)!

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
    }
    func getTotalMembersInGroupAPI(groupId:Int)
    {
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
        
        //loadingHud.show(in: self.view)
                let param: [String: Any] = [
                    "group_id" : groupId
                    ]
        GroupsManager.getMembersInGroup(body: param) { [unowned self] (members, error) in
            if error != nil {
                print("ERROR \(error.debugDescription)")
               // self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            self.loadingHud.dismiss()
            let mem = MyMember()
            mem.totalMembers = members?.data?.count
            print("DATA \(members?.data)")
            for mems in (members?.data)!
            {
                if mems.image != ""
                {
                    let url = URL(string: mems.image!)
                    print("SSSSSSS \(url!)")
                    if let data = try? Data(contentsOf: url!)
                    {
                        let image: UIImage = UIImage(data: data)!
                        mem.imageUrl.append(image)

                    }
                }
                else
                {
        
                        let image: UIImage = #imageLiteral(resourceName: "user")
                        mem.imageUrl.append(image)
                        
                    
                }
                
            }
    
            self.myMembers.append(mem)
            
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupCell", for: indexPath) as! GroupTableCellView
  
        if Constants.Settings.lang == "AR"
        {
            // self.view.semanticContentAttribute = .forceRightToLeft
            cell.arrow.rotate(byAngle: 180, ofType: AngleUnit.degrees)
            cell.totalMember.text = "\((self.groups[indexPath.row].users?.count)!) اعضاء "
            cell.totalMember.textAlignment = .right
            cell.groupName.textAlignment = .right
            cell.totalPointsLbl.text = "مجموع النقاط"
        }
        else
        {
            cell.totalMember.text = "\((self.groups[indexPath.row].users?.count)!) Members"

        }
//        if groups.count == myMembers.count
//        {
//        DispatchQueue.main.async
//            {
                self.myIndex = indexPath.row
                cell.groupName.text = self.groups[indexPath.row].name
        
        cell.groupImage.sd_setImage(with: URL(string:"http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public/\(groups[indexPath.row].image!)"),placeholderImage: UIImage(named: "Group 3599"))
        print("ppppppppppppp\(groups[indexPath.row].image!)")
            cell.groupImage.cornerRadius = cell.groupImage.height/2
            cell.celNo.text="\(indexPath.row+1)"
            cell.totalPoint.text = "\(self.groups[indexPath.row].totalPoints!)"
        
        let layoutConfigurator = BPLayoutConfigurator(
            backgroundColorForTruncatedBubble: UIColor.gray,
            fontForBubbleTitles: UIFont(name: "HelveticaNeue-Light", size: 16.0)!,
            colorForBubbleBorders: UIColor.darkGray,
            colorForBubbleTitles: UIColor.white,
            maxCharactersForBubbleTitles: 2,
            maxNumberOfBubbles: 4,
            direction: .leftToRight,
            alignment: .center
        )
        var array = [BPCellConfigFile]()
        for item in self.groups[indexPath.row].users!
        {
            if let memberImage = item.image
            {
                if memberImage != ""
                {
                    print("IMAAAAAAAAA \(memberImage)")
                    let im = BPCellConfigFile(imageType: BPImageType.URL(URL(string:memberImage)!),title: "")
                    array.append(im)
                }
                else
                {
                    let im = BPCellConfigFile(imageType: BPImageType.image(#imageLiteral(resourceName: "user")),title: "")
                    array.append(im)
                }
            }
            else
            {
                let im = BPCellConfigFile(imageType: BPImageType.image(#imageLiteral(resourceName: "user")),title: "")
                array.append(im)
                
            }

        }
        print("IMAAAAAAAAA COUNT \(array.count)   \(cell.onlyPics)")

        cell.bubblePictures = BubblePictures(collectionView: cell.onlyPics, configFiles: array, layoutConfigurator: layoutConfigurator)
        cell.bubblePictures.delegate = cell

          /*  cell.onlyPics.layer.cornerRadius = 20.0
            cell.onlyPics.layer.masksToBounds = true
            cell.onlyPics.defaultPicture = #imageLiteral(resourceName: "defaultProfilePicture")
           cell.onlyPics.dataSource = self
            cell.onlyPics.order = .descending
            cell.onlyPics.tag = indexPath.row
        cell.onlyPics.isHiddenVisibleCount = false*/
 
      //  var d = 0

//        for user in (self.groups[indexPath.row].users)!
//        {
//            let url = URL(string: user.image!)
//            print("URL \(url)")
//            cell.onlyPics.insertPicture(atIndex: d, withAnimation: .popup) { (imageView) in
//            imageView.image = #imageLiteral(resourceName: "defaultProfilePicture")
//                if url != nil
//                {
//                    imageView.sd_setImage(with: url!, completed: nil)
//                }
//            }
//            d = d + 1
//        }
        
//        DispatchQueue.main.async
//        {
////
//
//       // cell.onlyPics.reloadData()
//       }

        
        return cell

//         cell.onlyPics.alignment = .center
//         cell.onlyPics.countPosition = .right
//         cell.onlyPics.recentAt = .left

        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "groupDetails") as! GroupDetailsViewController
        viewController.groupId = groups[indexPath.row].groupId!
        viewController.totalPointsStr = "\(groups[indexPath.row].totalPoints!)"
        viewController.groupName = "\(groups[indexPath.row].name!)"
//        let url2 = URL(string:(self.groups[indexPath.row].image)!)
//        viewController.groupImageView.image = UIImage(named: "Group 3599")
//        viewController.groupImageView.sd_setImage(with: url2, completed: nil)
            //  self.navigationController?.pushViewController(viewController, animated: true)
        viewController.editFlag = 1
            self.present(viewController, animated: true, completion: nil)
        
    }
//    func getAllGroupsAsAnyAPI()
//    {
//        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
//
//        loadingHud.show(in: self.view)
//        let param: [String: Any] = [
//            "user_id" : userId
//        ]
//        GroupsManager.getAllGroupsAsAny(body: param){ [unowned self] (groups, error) in
//            if error != nil {
//                self.loadingHud.dismiss()
//                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
//                return
//            }
//            self.loadingHud.dismiss()
//
//            self.groups1.removeAll()
//            self.groups1 = (groups?.data)!
//
//
//            self.tableView.reloadData()
//
//        }
//
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TopGroupsViewController: OnlyPicturesDataSource
{
    
    func numberOfPictures() -> Int {
        
        return (self.groups[myIndex].users?.count)!
    }
    

    
//    func numberOfPictures(onlyPictureView: OnlyPictures) -> Int {
//        return self.myMembers[myIndex].imageUrl.count
//    }
    func visiblePictures(onlyPictureView: OnlyPictures) -> Int {

        return 3
    }
//    func visiblePictures() -> Int {
//        return (self.groups[myIndex].users?.count)!
//
//    }
  /*  func pictureViews(index: Int) -> UIImage{
        var im = UIImage()
        
        if let url = URL( string:(self.groups[myIndex].users?[index].image)!)
        {
            
            if let data = try? Data( contentsOf:url)
            {
//                DispatchQueue.main.async
//                    {
                im = UIImage( data:data)!
               // }
                
            }
            else
            {
//                DispatchQueue.main.async
//                    {
                        im =  UIImage(named: "defaultProfilePicture")!
             //   }
                
            }
            
        }
        
        return im
    }*/
    func pictureViews(_ imageView: UIImageView, index: Int) {
        
        // Use 'index' to receive specific url from your collection. It's similar to indexPath.row in UITableView.
        print("INDEX \(myIndex)")
        let url = URL(string:(self.groups[myIndex].users?[index].image)!)

        imageView.image = #imageLiteral(resourceName: "user")   // placeholder image
        imageView.sd_setImage(with: url, completed: nil)
       
    }
}

