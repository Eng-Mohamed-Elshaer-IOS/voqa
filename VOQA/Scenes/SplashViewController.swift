//
//  SplashViewController.swift
//  Elusive
//
//  Created by Graphic on 5/7/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let prefs = UserDefaults.standard
        let keyValue = prefs.string(forKey:"UserToken")
        if keyValue != nil && keyValue != "" {
            // Home
            let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "homeTabBarController") as! MainSwitchTabsViewController
            self.present(newViewController, animated: true, completion: nil)
        } else {
            // Login
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
