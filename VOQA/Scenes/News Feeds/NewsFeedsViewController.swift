//
//  NewsFeedsViewController.swift
//  Elusive
//
//  Created by Graphic on 3/14/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import SDWebImage
import Crashlytics
import UILoadControl
import SwiftyJSON
class NewsFeedsViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var newsFeeds_TableView: UITableView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var cosmos: CosmosView!
    @IBOutlet weak var rateMsg: UILabel!
    @IBOutlet weak var rateMsg1: UILabel!

    private let refreshControl = UIRefreshControl()
    let loadingHud = LoadingHud.shared.loadingHud
    var news = [NewsObject]()
    var newsInfo: NewsObject!
    var backGroundBlurView: UIVisualEffectView!
    var effect:UIVisualEffect!
    var userAward:Int?
    var restId:Int?

    @IBOutlet var popupView: UIView!
    @IBOutlet var titleLbl: UILabel!

    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    @IBAction func rateRestaurant(_ sender: UIButton)
    {
        
        self.rateRest()
        self.animateOut()
        
        
        //self.dismissPop()
    }
    @IBAction func goToNotifications(_ sender: UIButton)
    {
        let storyboard =  UIStoryboard.init(name: "Groups", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "allNotifications") as! NotificationsViewController
        //viewController.groupId = groups[indexPath.row].groupId!
        //  self.navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)

    }
    @objc func dismissPop() {
        //   animateDateOut()
        animateOut()
    }
    private func animateIn() {
     //   UIViewController *vc = self.window.rootViewController
        if (UIApplication.shared.keyWindow?.visibleViewController?.isKind(of: LoginViewController.self))!
        {
    
        }
        else
        {
            UIApplication.shared.keyWindow?.addSubview(backGroundBlurView)
            UIApplication.shared.keyWindow?.addSubview(popupView)
            popupView.center = (   UIApplication.shared.keyWindow?.center)!
            popupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            popupView.roundCorners([ .topLeft,.topRight,.bottomRight,.bottomLeft], radius: 50)
            
            popupView.alpha = 0
            
            UIView.animate(withDuration: 0.4)
            {
                self.backGroundBlurView.effect = self.effect
                self.popupView.alpha = 1
                self.popupView.transform = CGAffineTransform.identity
            }
            
        }
        
    }
    private func animateOut () {
        UIView.animate(withDuration: 0.3, animations: {
            self.popupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.popupView.alpha = 0
            
            self.backGroundBlurView.effect = nil
            
        }) { (success:Bool) in
            self.popupView.removeFromSuperview()
            self.backGroundBlurView.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newsFeeds_TableView.loadControl = UILoadControl(target: self, action: #selector(loadMore(sender:)))

      //  cosmos.color
        commentTextView.placeholder = "Your comment"
       
        self.cosmos.emptyImage = self.cosmos.emptyImage?.tintColor(with: UIColor.orange)
        

        backGroundBlurView = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: self.screenWidth, height: self.screenHeight))
        effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        backGroundBlurView.effect = nil
        
        backGroundBlurView.isUserInteractionEnabled = true
        backGroundBlurView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissPop)))
        
//        myPointsView.clipsToBounds = true
//        myPointsView.layer.masksToBounds = true

        // Do any additional setup after loading the view.
        refreshControl.tintColor = UIColor.gray
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.gray ]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Data ...", attributes: myAttribute)
        
        if #available(iOS 10.0, *) {
            newsFeeds_TableView.refreshControl = refreshControl
        } else {
            newsFeeds_TableView.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
   
        
    }
    
    @objc private func refreshData(_ sender: Any) {
    //    fetchNews()
        self.refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.news.removeAll()
        Constants.headers.pageIndex = 1
        if Constants.Settings.lang == "AR" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight

            
        }
        if Constants.Settings.lang == "AR"
        {
            self.titleLbl.text = "الاخبار"
        }
        else
        {
            self.titleLbl.text = "News Feeds"
            
            
        }
        fetchNews()

    }
//    override func viewDidAppear(_ animated: Bool) {
//        self.fetchNews()
//
//    }
    func checkRatings()
    {
        loadingHud.show(in: self.view)
        let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
        let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)

        //  print("MATCHID \(matchId)  \(token!)")
        let param: [String: Any] = [:]
        print("PARAM \(param)")
        

        let urlPath = "http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public/api/awards/needs_rating/\(userId)"
        //   https://35.198.81.33/
        let url1: URL = URL(string: urlPath as String)!
        
        let request = NSMutableURLRequest(url:url1 )
        let session = URLSession.shared
        request.httpMethod = "GET"

        do
        {
            request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            
        }
        catch
        {}
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            let res = response as? HTTPURLResponse
            if res!.statusCode == 200
            {
                self.loadingHud.dismiss()

                do
                {
                                let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                                //  let json = JSON(data)
                    if result!["data"] != nil{
                               if let rep = result!["data"]!["user_award_id"] as? Int
                               {
                                self.restId = result!["data"]!["restaurant_id"] as? Int
                                self.userAward = rep as? Int
                                if Constants.Settings.lang == "AR"
                                {
                                    DispatchQueue.main.async
                                        {
                                //    self.rateMsg.text = "يرجى تقييم كافيه \(UserDefaults.standard.string(forKey: "RestaurantName")!)"
                                    self.rateMsg.textAlignment = .right
                                    self.rateMsg1.text = "يساعدنا تقييمك على تقديم خدمة أفضل"
                                    self.rateMsg.text = "يرجى تقييم \((result!["data"]!["restaurant_name"]!)!)"

                                    self.rateMsg1.textAlignment = .right
                                            self.animateIn()

                                    }
                                }
                                else
                                {
                                    DispatchQueue.main.async
                                        {
                                            self.rateMsg.text = "Please rate \((result!["data"]!["restaurant_name"]!)!)"

                                            //    self.rateMsg.text = "يرجى تقييم كافيه \(UserDefaults.standard.string(forKey: "RestaurantName")!)"
//                                            self.rateMsg.textAlignment = .right
//                                            self.rateMsg1.text = "يساعدنا تقييمك على تقديم خدمة أفضل"
//                                            self.rateMsg1.textAlignment = .right
                                            self.animateIn()

                                            
                                    }
                           //         self.rateMsg.text = "Please Rate \(UserDefaults.standard.string(forKey: "RestaurantName")!)"
                                }
                                print("Result ",rep)
                    }
                    }
                
                }
                catch
                {}
            }
            else
            {
                self.loadingHud.dismiss()

            }
         
            //   response.
        })
        task.resume()
    }
    
    func rateRest( )
    {
        //  print("MATCHID \(matchId)  \(token!)")
        let userId = UserDefaults.standard.object(forKey: "UserID")
        let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)

        let param: [String: Any] =
            ["restaurant_id":restId!,
             "rating":self.cosmos.rating,
             "comment":self.commentTextView.text!,
             "user_id":userId!,
             "user_award_id":userAward!
        ]
        print("PARAM \(param)")
        

        let urlPath = "http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public/api/restaurants/rate"
        //   https://35.198.81.33/
        let url1: URL = URL(string: urlPath as String)!
        
        let request = NSMutableURLRequest(url:url1 )
        let session = URLSession.shared
        request.httpMethod = "POST"
        
        do
        {
            request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)

        }
        catch
        {}
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            let res = response as? HTTPURLResponse
            
            //            print("Response: \(res!.statusCode)")
            //            print("Data: \(String(describing: data))")
            
            if res!.statusCode == 200
            {

            }
            do
            {
                let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                print("Result ",result!)
  
//                UserDefaults.standard.set(0, forKey: "RestaurantID")
//                UserDefaults.standard.set(nil, forKey: "RestaurantName")
            }
            catch
            {}
            //   response.
        })
        task.resume()
    }
    private func fetchNews() {
//        if self.loadingHud.isVisible
//        {
//            self.loadingHud.dismiss()
//        }
        let param: [String: Any] = [:]
        loadingHud.show(in: self.view)
        NewsManager.getAllNews (body: param) { [unowned self] (news, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }

            self.loadingHud.dismiss()
         //   self.news.removeAll()
            self.newsFeeds_TableView.loadControl?.endLoading() //Update UILoadControl frame to the new UIScrollView bottom.
          //  self.tableView.reloadData()
            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                
                return
            }
            self.news = self.news + (news?.data)!
            self.newsFeeds_TableView.loadControl?.endLoading() //Update UILoadControl frame to the new UIScrollView bottom.

            self.newsFeeds_TableView.reloadData()
          //  self.animateIn()
        //    Crashlytics.sharedInstance().crash()
            self.checkRatings()


        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //update loadControl when user scrolls de tableView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.loadControl?.update()
    }
    
    //load more tableView data
    @objc func loadMore(sender: AnyObject?) {
        print("FGGGGGGGGGGGG")
        Constants.headers.pageIndex = Constants.headers.pageIndex + 1
        fetchNews()
//        AnyAPIManager.defaultManager.giveMoreData() { (response, error) in
//            //... Manage response
//            self.tableView.loadControl?.endLoading() //Update UILoadControl frame to the new UIScrollView bottom.
//            self.tableView.reloadData()
//        }
    }

}

// MARK:- UITableView Delegate and DataSource
extension NewsFeedsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsTableViewCell
        let object = news[indexPath.row]
        cell.newsTitle.text = object.title
        cell.newsTime.text = object.updated_at
        cell.newsImage.sd_setImage(with: URL(string: object.image!))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.newsInfo = news[indexPath.row]
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "newsInfo_Segue", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newsInfo_Segue" {
            let dist = segue.destination as! NewsInfoViewController
            dist.newsInfo = self.newsInfo
        }
    }
    
}

public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}
extension UIImage {
    
    func tintColor(with color: UIColor) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        
        // flip the image
        context.scaleBy(x: 1.0, y: -1.0)
        context.translateBy(x: 0.0, y: -self.size.height)
        
        // multiply blend mode
        context.setBlendMode(.multiply)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context.clip(to: rect, mask: self.cgImage!)
        color.setFill()
        context.fill(rect)
        
        // create UIImage
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
