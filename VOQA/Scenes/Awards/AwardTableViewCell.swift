//
//  AwardTableViewCell.swift
//  Elusive
//
//  Created by Graphic on 3/15/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class AwardTableViewCell: UITableViewCell {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var getBtnTrailingConstrainet: NSLayoutConstraint!
    @IBOutlet weak var viewCodeView: UIView!
    @IBOutlet weak var viewCodeBtn: UIButton!
    @IBOutlet weak var viewCodeLbl: UILabel!

    @IBOutlet weak var getButton: UIButton!
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        print("SCREEN WIDTH \(screenWidth)")
        getButton.x = screenWidth - 100

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
    }

}
