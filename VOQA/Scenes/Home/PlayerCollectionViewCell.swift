//
//  PlayerCollectionViewCell.swift
//  Elusive
//
//  Created by Graphic on 3/13/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class PlayerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playerView: UIImageView!
    
}
