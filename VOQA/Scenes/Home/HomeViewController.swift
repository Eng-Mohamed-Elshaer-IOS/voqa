//
//  HomeViewController.swift
//  Elusive
//
//  Created by Graphic on 3/13/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import SDWebImage

class HomeViewController: UIViewController {

    @IBOutlet weak var companies_CollectionView: UICollectionView!
    @IBOutlet weak var players_CollectionView: UICollectionView!
    
    let loadingHud = LoadingHud.shared.loadingHud
    var itemNumberPresented = 0
    var scrollTimer = Timer()
//    let colorsArray: [UIColor] = [.red, .gray, .black, .green, .brown, .yellow,
//                                  .red, .gray, .black, .green, .brown, .yellow,
//                                  .red, .gray, .black, .green, .brown, .yellow,
//                                  .red, .gray, .black, .green, .brown, .yellow,
//                                  .red, .gray, .black, .green, .brown, .yellow]
    var sponsers = [Sponser]()
    var players = [Player]()
    var playerInfo: Player!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    private func fetchingData() {
        loadingHud.show(in: self.view)
        HomeManager.getAllSponsers { [unowned self] (sponsers, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                
                return
            }
            self.sponsers = (sponsers?.sponsors)!
            self.getAllPlayers()
            
        }
    }
    
    private func getAllPlayers() {
        HomeManager.getAllPlayers(body: ("0", "1000")) { [unowned self] (players, error) in
            if error != nil {
                self.loadingHud.dismiss()
                self.present(UIViewHelper.connectionErrorAlert(), animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            if Constants.Message.invalidToken == "Invalid Token"
            {
                self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                
                return
            }
            self.players = (players?.players)!
            
            self.companies_CollectionView.reloadData()
            self.players_CollectionView.reloadData()
            self.scrollTimer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        itemNumberPresented = 0
        fetchingData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        scrollTimer.invalidate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK:- UICollectionView Delegates and DataSource
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == companies_CollectionView {
            return sponsers.count
        } else {
            return players.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == companies_CollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "companyCell", for: indexPath) as! CompanyCollectionViewCell
            cell.companyLogo.sd_setImage(with: URL(string: sponsers[indexPath.row].image!))
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playerCell", for: indexPath) as! PlayerCollectionViewCell
            cell.playerView.sd_setImage(with: URL(string: players[indexPath.row].image!))
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == players_CollectionView {
            self.playerInfo = players[indexPath.row]
            self.performSegue(withIdentifier: "playerInfo_Segue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "playerInfo_Segue" {
            let dist = segue.destination as! UINavigationController
            let vc = dist.viewControllers.first as! PlayerInfoViewController
            vc.playerInformation = playerInfo
        }
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = companies_CollectionView {
            if itemNumberPresented < sponsers.count - 1 {
                coll.scrollToItem(at: IndexPath(row: self.itemNumberPresented, section: 0), at: .left, animated: true)
                itemNumberPresented = (itemNumberPresented + 1)
            } else {
                itemNumberPresented = 0
                coll.scrollToItem(at: IndexPath(row: self.itemNumberPresented, section: 0), at: .left, animated: true)
            }
        }
        
    }
    
}
