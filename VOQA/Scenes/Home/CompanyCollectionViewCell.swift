//
//  CompanyCollectionViewCell.swift
//  Elusive
//
//  Created by Graphic on 3/13/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class CompanyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var companyLogo: UIImageView!
    
}
