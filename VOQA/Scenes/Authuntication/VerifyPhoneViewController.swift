//
//  VerifyPhoneViewController.swift
//  Elusive
//
//  Created by Graphic on 5/5/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//
import UIKit
import FirebaseAuth

class VerifyPhoneViewController: UIViewController {
    
    @IBOutlet weak var codeView: ActivationCodeView!
    @IBOutlet weak var codeView_TextField: UITextField!
    @IBOutlet weak var mobileNumber_Label: UILabel!
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!

    @IBOutlet weak var resendBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!

    var loadingHud = LoadingHud.shared.loadingHud
    var registerBody: RegisterBody!
    var registerSocialBody: CreateSocialBody!
    var signupType: String!
    
    var currentCode = ""
    var countryCode: String!
    var codeCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if signupType == "social" {
            self.mobileNumber_Label.text = registerSocialBody.mobileNumber
        } else if signupType == "normal" {
            self.mobileNumber_Label.text = registerBody.mobile_number
        }
        codeView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Constants.Settings.lang == "AR"
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.backBtn.semanticContentAttribute = .forceRightToLeft
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)

            self.label1.text = "قم بتأكيد رقم هاتفك المحمول"
            self.label2.text = "أرسلنا رمز التأكيد إلى هاتفك المحمول"
            self.label3.text = "رمز التأكيد هنا"
            self.resendBtn.setTitle("أعد إرسال الرمز", for: .normal)
        }
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
}

// MARK:- IBActions
extension VerifyPhoneViewController {
    
    @IBAction func backToSignupScreen(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendCodeAction(_ sender: UIButton) {
        //print("resendCodeAction Button")
        let alert = UIAlertController(title: "Resend Code", message: "Do you want to resend code to your number ?", preferredStyle: .alert)
        if codeCounter < 2 {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [unowned self] (action) in
                // --------- Resend Verify Code ----------- //
                self.loadingHud.show(in: self.view)
                if self.signupType == "social" {
                    PhoneAuthProvider.provider().verifyPhoneNumber(("+20" + self.registerSocialBody.mobileNumber!), uiDelegate: nil, completion: { (verificationID, error) in
                        if error != nil {
                            self.loadingHud.dismiss()
                            let alert = UIViewHelper.connectionErrorAlert()
                            self.present(alert, animated: true, completion: nil)
                            return
                        } else {
                            self.loadingHud.dismiss()
                            let defaults = UserDefaults.standard
                            defaults.set(verificationID, forKey: "authVID")
                        }
                    })
                } else if self.signupType == "normal" {
                    PhoneAuthProvider.provider().verifyPhoneNumber(("+20" + self.registerBody.mobile_number!), uiDelegate: nil, completion: { (verificationID, error) in
                        if error != nil {
                            self.loadingHud.dismiss()
                            let alert = UIViewHelper.connectionErrorAlert()
                            self.present(alert, animated: true, completion: nil)
                            return
                        } else {
                            self.loadingHud.dismiss()
                            let defaults = UserDefaults.standard
                            defaults.set(verificationID, forKey: "authVID")
                        }
                    })
                }
                // ---------------------------------------- //
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIViewHelper.errorAlert(title: "Resend Code", message: "Please make sure of your phone number and try again later.")
            self.present(alert, animated: true, completion: nil)
        }
        codeCounter += 1
        
    }
    
    @IBAction func textEditingChanged(_ sender: UITextField) {
        if (sender.text?.count)! >= self.codeView.nextTag && (sender.text?.count)! < 7 {
            currentCode = sender.text!
            self.codeView.insertText(sender.text!)
        } else if (sender.text?.count)! < self.currentCode.count {
            self.codeView.deleteBackward()
            currentCode = sender.text!
        } else {
            self.codeView_TextField.text = currentCode
        }
    }
}

extension VerifyPhoneViewController: EnterDigits {
    func didFinishEnterDigits(code: String) {
        self.codeView_TextField.endEditing(true)
        loadingHud.show(in: self.view)
        
        let defaults = UserDefaults.standard
        let cardential: PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: defaults.string(forKey: "authVID")!, verificationCode: code)
        Auth.auth().signIn(with: cardential) { [unowned self] (user, error) in
            if let errorMessage = error {
                self.loadingHud.dismiss()
                let errorString = String(describing: errorMessage.localizedDescription)
                let alert = UIViewHelper.errorAlert(title: "Error", message: errorString)
                self.present(alert, animated: true, completion: nil)
            } else {
                self.loadingHud.dismiss()
                self.signup()
            }
        }
        
    }
    
    private func signup() {
        if signupType == "social" {
            loadingHud.show(in: self.view)
            guard let registerBody = self.registerSocialBody else {
                self.loadingHud.dismiss()
                return
            }

            AuthManager.createSocial(body: registerBody) { [unowned self] (response, error) in
                if error != nil {
                    self.loadingHud.dismiss()
                    let alert = UIViewHelper.connectionErrorAlert()
                    self.present(alert, animated: true, completion: nil)
                    return
                }

                self.loadingHud.dismiss()
                if response?.status?.code == 200 {
                    // Handle Success signup Here
                    //print("Signup Success")
                    let user = response!.data!
                    let defaults = UserDefaults.standard
                    defaults.set(user.token, forKey: "UserToken")
                    defaults.set(user.fullName, forKey: "UserName")
                    defaults.set(user.totalPoints, forKey: "UserPoints")
                    defaults.set(user.image, forKey: "UserImage")
                    defaults.set(user.userId, forKey: "UserID")

                    defaults.synchronize()
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "homeTabBarController") as! MainSwitchTabsViewController
                    self.present(newViewController, animated: true, completion: nil)
                    
                } else {
                    // Handle Failuer Login Here
                    let alert = UIViewHelper.connectionErrorAlert()
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            loadingHud.show(in: self.view)
            guard let registerBody = self.registerBody else {
                self.loadingHud.dismiss()
                return
            }

            AuthManager.register(body: registerBody) { [unowned self] (response, error) in
                if error != nil {
                    self.loadingHud.dismiss()
                    let alert = UIViewHelper.connectionErrorAlert()
                    self.present(alert, animated: true, completion: nil)
                    return
                }

                self.loadingHud.dismiss()
                if response?.status?.code == 200 {
                    // Handle Success signup Here
                    //print("Signup Success")
                    let user = response!.data!
                    let defaults = UserDefaults.standard
                    defaults.set(user.token, forKey: "UserToken")
                    defaults.set(user.fullName, forKey: "UserName")
                    defaults.set(user.totalPoints, forKey: "UserPoints")
                    defaults.set(user.image, forKey: "UserImage")
                    defaults.set(user.userId, forKey: "UserID")

                    defaults.synchronize()
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "homeTabBarController") as! MainSwitchTabsViewController
                    self.present(newViewController, animated: true, completion: nil)
                    
                } else {
                    print("RESPONSE \(response)")
                    // Handle Failuer Login Here
                    let alert = UIViewHelper.errorAlert(title: "Error", message: (response?.errors![0])!)
                    self.present(alert, animated: true, completion: nil)
//                    let alert = UIViewHelper.connectionErrorAlert()
//                    self.present(alert, animated: true, completion: nil)

                }
            }
        }
    }
}
