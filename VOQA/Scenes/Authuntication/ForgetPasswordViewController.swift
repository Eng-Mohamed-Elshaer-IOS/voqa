//
//  ForgetPasswordViewController.swift
//  Elusive
//
//  Created by Graphic on 3/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import Material

class ForgetPasswordViewController: UIViewController {

    // MARK:- IBOutlets
    @IBOutlet weak var email_Textfield: ErrorTextField!
    @IBOutlet weak var verfiyBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!

    @IBOutlet weak var titleLbl: UILabel!

    let loadingHud = LoadingHud.shared.loadingHud
    
    // MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        initTextfieldsUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Constants.Settings.lang == "AR"
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.backBtn.semanticContentAttribute = .forceRightToLeft
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)

            self.titleLbl.text = "نسيت كلمة السر"
            self.verfiyBtn.setTitle("ارسال كلمة السر", for: .normal)
         //   self.email_Textfield.text = "البريد الالكتروني"
            self.email_Textfield.textAlignment = .right

            //self.email_Textfield.setTitle("ارسال كلمة السر", for: .normal)

        }

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

// MARK:- IBActions
extension ForgetPasswordViewController {
    
    @IBAction func forgetPassword(_ sender: UIButton) {
        // forget password functionality goes here
        // validate email is correct format
        loadingHud.show(in: self.view)
        guard let email = self.email_Textfield.text, email.validateEmailFormat() else {
            self.loadingHud.dismiss()
            let alert = UIViewHelper.errorAlert(title: "Incorrect Email", message: "Please enter valid email")
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let forgetPasswordBody = ForgetPasswordBody(email: email)
        
        AuthManager.forgetPassword(body: forgetPasswordBody) { [unowned self] (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                let alert = UIViewHelper.connectionErrorAlert()
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            if response?.status?.code == 200 {
                // Handle Success email sent success Here
                self.email_Textfield.text = ""
                let alert = UIViewHelper.errorAlert(title: "Successfuly ForgetPasswrod", message: "Your ForgetPassword Successfull")
                self.present(alert, animated: true, completion: nil)
            } else {
                // Handle Failuer Here
//                let alert = UIViewHelper.errorAlert(title: "Error", message: (response?.errors![0])!)
//                self.present(alert, animated: true, completion: nil)
                let alert = UIViewHelper.connectionErrorAlert()
                self.present(alert, animated: true, completion: nil)

            }
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK:- Textfield Delegate Functions
extension ForgetPasswordViewController: TextFieldDelegate {
    public func textFieldDidEndEditing(_ textField: UITextField) {
        //(textField as? ErrorTextField)?.isErrorRevealed = true
        // Make Validation Here if needed
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func initTextfieldsUI() {
        if Constants.Settings.lang == "AR"
        {
            email_Textfield.placeholder = "البريد الالكتروني"

        }
        else
        {
            email_Textfield.placeholder = "Email"

        }
        email_Textfield.detail = "Error, incorrect email"
        email_Textfield.isClearIconButtonEnabled = false
        email_Textfield.delegate = self
        email_Textfield.isPlaceholderUppercasedWhenEditing = false
        email_Textfield.textColor = UIColor.white
        email_Textfield.dividerColor = UIColor.white
        email_Textfield.dividerActiveColor = UIColor.white
        email_Textfield.placeholderNormalColor = UIColor.white
        email_Textfield.placeholderActiveColor = UIColor.white
        email_Textfield.clearIconButton?.tintColor = .black
    }
}
