//
//  ActivationCodeView.swift
//  Elusive
//
//  Created by Graphic on 5/5/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

public protocol EnterDigits {
    func didFinishEnterDigits(code: String)
}

class ActivationCodeView: UIView, UIKeyInput {
    
    open var delegate: EnterDigits?
    var nextTag = 1
    
    // MARK: - UIResponder
    open override var canBecomeFirstResponder : Bool {
        return true
    }
    
    open func insertText(_ text: String) {
        if nextTag < 7 {
            (self.viewWithTag(nextTag)! as! UILabel).text = String(describing: text.last!)
            nextTag += 1
            
            if nextTag == 7 {
                var code = ""
                for index in 1..<nextTag {
                    code += (self.viewWithTag(index)! as! UILabel).text!
                }
                delegate?.didFinishEnterDigits(code: code)
                //print("Finshed 6 digits")
            }
        }
    }
    
    open func deleteBackward() {
        if nextTag > 1 {
            nextTag -= 1
            (self.viewWithTag(nextTag)! as! UILabel).text = ""
        }
    }
    
    public var hasText : Bool {
        return nextTag > 1 ? true : false
    }
    
    // MARK: - UITextInputTraits
    open var keyboardType: UIKeyboardType { get { return .numberPad } set { } }
    
}
