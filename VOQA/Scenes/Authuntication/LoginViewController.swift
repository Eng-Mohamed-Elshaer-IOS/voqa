//
//  LoginViewController.swift
//  Elusive
//
//  Created by Graphic on 3/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import Material
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAuth
import SwiftyJSON

class LoginViewController: UIViewController {

    // MARK:- IBOutlets
    @IBOutlet weak var email_Textfield: ErrorTextField!
    @IBOutlet weak var password_Textfield: ErrorTextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var forgetPassword: UIButton!
    @IBOutlet weak var createNewAccount: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var orLoginBy: UILabel!

    let loadingHud = LoadingHud.shared.loadingHud

    var user = [String: String]()
    var signupType = "normal"
    
    // MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        initTextfieldsUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
   //     UIView.appearance().semanticContentAttribute = .forceLeftToRight

        if Constants.Settings.lang == "AR"
        {
//          self.email_Textfield.clearIconButton?.frame.origin.x = self.email_Textfield.frame.origin.x+50
            self.email_Textfield.leftView = self.email_Textfield.clearIconButton
            self.email_Textfield.leftViewMode = .whileEditing
            self.titleLbl.text = "الدخول"
            self.orLoginBy.text = "او الدخول مع"

            self.email_Textfield.placeholder = "البريد الالكتروني"
            self.password_Textfield.placeholder = "كلمة السر"
            self.email_Textfield.textAlignment = .right
            self.password_Textfield.textAlignment = .right
            
            self.loginBtn.setTitle("الدخول", for: .normal)
            self.createNewAccount.setTitle("تسجيل اسم مستخدم", for: .normal)
            self.forgetPassword.setTitle("نسيت كلمةالسر", for: .normal)
      //      initTextfieldsUI()

        }
        else
        {
            
            self.email_Textfield.placeholder = "Email Address"
            self.password_Textfield.placeholder = "Password"

        }
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

// MARK:- IBActions
extension LoginViewController {
    
    @IBAction func signin(_ sender: UIButton) {
        // Signin functionality goes here
        //print("Sign in Method")
        loadingHud.show(in: self.view)
        guard let loginBody = validateLoginData() else {
            return
        }
        
        AuthManager.login(body: loginBody) { [unowned self] (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                let alert = UIViewHelper.connectionErrorAlert()
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            if response?.status?.code == 200 {
                // Handle Success Login Here
                if Constants.Message.invalidToken == "Invalid Token"
                {
                    self.present(UIViewHelper.invalidToken(), animated: true, completion: nil)
                    
                    return
                }
                let user = response!.data!
                let defaults = UserDefaults.standard
                defaults.set(user.token, forKey: "UserToken")
                defaults.set(user.fullName, forKey: "UserName")
                defaults.set(user.totalPoints, forKey: "UserPoints")
                defaults.set(user.image, forKey: "UserImage")
                defaults.set(user.userId, forKey: "UserID")

                defaults.synchronize()
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "homeTabBarController") as! MainSwitchTabsViewController
                self.present(newViewController, animated: true, completion: nil)
            } else {
                // Handle Failuer Login Here
//                let alert = UIViewHelper.errorAlert(title: "Error", message: (response?.errors![0])!)
//                self.present(alert, animated: true, completion: nil)
                let alert = UIViewHelper.connectionErrorAlert()
                self.present(alert, animated: true, completion: nil)

            }
        }
    }
    
    private func validateLoginData() -> LoginBody? {
        // validate email is correct format
        guard let email = self.email_Textfield.text, email.validateEmailFormat() else {
            self.loadingHud.dismiss()
            let alert = UIViewHelper.errorAlert(title: "Incorrect Email", message: "Please enter valid email")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate password is correct format
        guard let password = self.password_Textfield.text, password.count >= 6 else {
            self.loadingHud.dismiss()
            let alert = UIViewHelper.errorAlert(title: "Incorrect Password", message: "Please enter valid password. (Password must be 6 characters at least)")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        let loginBody = LoginBody(email: email, password: password)
        return loginBody
    }
    
    @IBAction func signinWithFacebook(_ sender: UIButton) {
        // Signin functionality with facebook goes here
        //print("Sign in Method with facebook")
        FBSDKLoginManager().logOut()
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if error != nil {
                //print("Can't Login with Facebook")
                //print(error?.localizedDescription)
                let alert = UIViewHelper.errorAlert(title: "Connection Error", message: "Can't Login with Facebook")
                self.present(alert, animated: true, completion: nil)
                return
                
            }
            
            if result != nil {
                if let token = result?.token? .tokenString {
                    self.user["socialToken"] = token
                    self.showEmail()
                } else {
                    //print("you canceled login with facebook")
                }
            }
        }
    }
    
    func showEmail() {
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, first_name, last_name, email"]).start { (connection, result, error) in
            if error != nil {
                let alert = UIViewHelper.errorAlert(title: "Connection Error", message: "Connection error, please make sure you are connected to network")
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            if let data = result as? [String: String] {
                let json = JSON(data)
                self.user["email"] = json["email"].string
                self.user["name"] = json["first_name"].string! + " " + json["last_name"].string!
                self.user["socialId"] = json["id"].string
//                self.performSegue(withIdentifier: "SignupSocial", sender: self)
                self.checksocialAccount()
            }
        }
    }
    
    private func checksocialAccount() {
        loadingHud.show(in: self.view)
        let body = CheckSocialBody(socialId: self.user["socialId"]!, socialName: "facebook")
        AuthManager.checkSocial(body: body) { (response, error) in
            if error != nil {
                self.loadingHud.dismiss()
                let alert = UIViewHelper.connectionErrorAlert()
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.loadingHud.dismiss()
            if response?.data?.token != nil && response?.data?.fullName != nil && response?.data?.totalPoints != nil {
                // Handle Success Login Here
                let user = response!.data!
                let defaults = UserDefaults.standard
                defaults.set(user.token, forKey: "UserToken")
                defaults.set(user.fullName, forKey: "UserName")
                defaults.set(user.totalPoints, forKey: "UserPoints")
                defaults.set(user.image, forKey: "UserImage")
                defaults.set(user.userId, forKey: "UserID")

                defaults.synchronize()
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "homeTabBarController") as! MainSwitchTabsViewController
                self.present(newViewController, animated: true, completion: nil)
            } else {
                // Handle Failuer Login Here
                self.signupType = "social"
                self.performSegue(withIdentifier: "SignupSocial", sender: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SignupSocial" {
            let dist = segue.destination as! SignupViewController
            dist.socialDataSignup = self.user
            dist.signupType = self.signupType
        }
    }
    
}

// MARK:- Textfield Delegate Functions
extension LoginViewController: TextFieldDelegate {
    public func textFieldDidEndEditing(_ textField: UITextField) {
        //(textField as? ErrorTextField)?.isErrorRevealed = true
        // Make Validation Here if needed
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func initTextfieldsUI() {
       // email_Textfield.placeholder = "Email"
        email_Textfield.detail = "Error, incorrect email"
        email_Textfield.isClearIconButtonEnabled = false
        email_Textfield.delegate = self
        email_Textfield.isPlaceholderUppercasedWhenEditing = false
        email_Textfield.textColor = UIColor.white
        email_Textfield.dividerColor = UIColor.white
        if #available(iOS 10.0, *) {
            email_Textfield.dividerActiveColor = UIColor.init(displayP3Red: 98.0, green: 235.0, blue: 224.0, alpha: 1)
        } else {
            // Fallback on earlier versions
        }
        email_Textfield.placeholderNormalColor = UIColor.white
        email_Textfield.placeholderActiveColor = UIColor.white
        email_Textfield.clearIconButton?.tintColor = .black
        
     //   password_Textfield.placeholder = "Password"
        password_Textfield.isClearIconButtonEnabled = false
        password_Textfield.delegate = self
        password_Textfield.isPlaceholderUppercasedWhenEditing = false
        password_Textfield.textColor = UIColor.white
        password_Textfield.dividerColor = UIColor.white
        if #available(iOS 10.0, *) {
            password_Textfield.dividerActiveColor = UIColor.init(displayP3Red: 98.0, green: 235.0, blue: 224.0, alpha: 1)
        } else {
            // Fallback on earlier versions
        }
        password_Textfield.placeholderNormalColor = UIColor.white
        password_Textfield.placeholderActiveColor = UIColor.white
        password_Textfield.clearIconButton?.tintColor = .black
    }
}
