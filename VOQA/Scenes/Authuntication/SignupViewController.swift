//
//  SignupViewController.swift
//  Elusive
//
//  Created by Graphic on 3/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit
import Material
import TCPickerView
import FirebaseAuth

class SignupViewController: UIViewController {

    // MARK:- IBOutlets
    @IBOutlet weak var fullName_TextField: ErrorTextField!
    @IBOutlet weak var age_TextField: UITextField!
    @IBOutlet weak var gender_TextField: UITextField!
    @IBOutlet weak var city_TextField: UITextField!
    @IBOutlet weak var area_TextField: UITextField!
    @IBOutlet weak var email_TextField: ErrorTextField!
    @IBOutlet weak var phoneNumber_TextField: ErrorTextField!
    @IBOutlet weak var password_TextField: ErrorTextField!
    @IBOutlet weak var signinBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!

    @IBOutlet weak var titleLbl: UILabel!

    // birthdate picker outlets
    @IBOutlet weak var datePopupView: UIView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    var effect:UIVisualEffect!
    var backGroundBlurView: UIVisualEffectView!
    
    let loadingHud = LoadingHud.shared.loadingHud
    var socialDataSignup = [String: String]()
    var socialBody: CreateSocialBody!
    var normalBody: RegisterBody!
    var signupType = "normal"
    var selectedGender = ""
    var selectedCity = ""
    var selectedArea = ""
    var selectedCityId = -1
    var selectedAreaId = -1
    var selectedDate = ""
    
    var cities = [City]()
    
    // MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backGroundBlurView = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
        effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        backGroundBlurView.effect = nil
        
        AuthManager.getMetaData { [unowned self] (response, error) in
            if error != nil {
                
            } else {
                self.cities = (response?.data?.cities)!
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        initTextfieldsUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Constants.Settings.lang == "AR"
        {
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.backBtn.semanticContentAttribute = .forceRightToLeft
            self.backBtn.rotate(byAngle: 180, ofType: AngleUnit.degrees)

     //       UITextField.appearance().semanticContentAttribute = .forceRightToLeft
            self.fullName_TextField.placeholder = "الاسم الكامل"
            self.fullName_TextField.placeholderLabel.text = "الاسم الكامل"
            self.fullName_TextField.textAlignment = .right
            
            self.gender_TextField.placeholder = "(اختياري) الجنس"
            self.gender_TextField.textAlignment = .right

            self.age_TextField.placeholder = "سنة الميلاد (اختياري)"
            self.age_TextField.textAlignment = .right

            self.area_TextField.placeholder = "المنطقة"
            self.area_TextField.textAlignment = .right

            self.city_TextField.placeholder = "المدينة"
            self.city_TextField.textAlignment = .right

            self.email_TextField.placeholder = "البريد الالكتروني"
            self.email_TextField.placeholderLabel.text = "البريد الالكتروني"
            self.email_TextField.textAlignment = .right

            self.password_TextField.placeholder = "كلمة السر"
            self.password_TextField.placeholderLabel.text = "كلمة السر"
            self.password_TextField.textAlignment = .right

            self.phoneNumber_TextField.placeholder = "رقم المحمول"
            self.phoneNumber_TextField.placeholderLabel.text = "رقم المحمول"
            self.phoneNumber_TextField.textAlignment = .right
            self.signinBtn.setTitle("تسجيل", for: .normal)
            self.titleLbl.text = "التسجيل"

        }
        else
        {
            
            email_TextField.placeholder = "Email"
            fullName_TextField.placeholder = "Full Name"
            password_TextField.placeholder = "Password"
            phoneNumber_TextField.placeholder = "Phone Number"

        }
        
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if signupType == "social" {
            self.password_TextField.isHidden = true
            self.email_TextField.text = self.socialDataSignup["email"]
            self.fullName_TextField.text = self.socialDataSignup["name"]
            self.email_TextField.isEnabled = false
            self.fullName_TextField.isEnabled = false
        } else if signupType == "normal" {
            self.email_TextField.isEnabled = true
            self.fullName_TextField.isEnabled = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    private func animateIn() {
        self.view.addSubview(backGroundBlurView)
        self.view.addSubview(datePopupView)
        datePopupView.center = self.view.center
        
        datePopupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        datePopupView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.backGroundBlurView.effect = self.effect
            self.datePopupView.alpha = 1
            self.datePopupView.transform = CGAffineTransform.identity
        }
        
    }
    
    
    private func animateOut () {
        UIView.animate(withDuration: 0.3, animations: {
            self.datePopupView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.datePopupView.alpha = 0
            
            self.backGroundBlurView.effect = nil
            
        }) { (success:Bool) in
            self.datePopupView.removeFromSuperview()
            self.backGroundBlurView.removeFromSuperview()
        }
    }
    
}

// MARK:- IBActions
extension SignupViewController {
    
    @IBAction func signup(_ sender: UIButton) {
        // Signup functionality goes here
        //print("sign up Method")
        if signupType == "social" {
            guard let registerBody = validateSocialSignupData() else {
                return
            }
            self.socialBody = registerBody
            self.sendVerifyCodeToPhoneNumber(phoneNumber: ("+20" + registerBody.mobileNumber!))
        } else {
            guard let registerBody = validateSignupData() else {
                return
            }
            self.normalBody = registerBody
            self.sendVerifyCodeToPhoneNumber(phoneNumber: ("+20" + registerBody.mobile_number!))
        }
        
    }
    
    private func sendVerifyCodeToPhoneNumber(phoneNumber: String) {
        //print(phoneNumber)
        self.loadingHud.show(in: self.view)
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if error != nil {
                print("FFFFFF \(error.debugDescription)")
                self.loadingHud.dismiss()
                let alert = UIViewHelper.connectionErrorAlert()
                self.present(alert, animated: true, completion: nil)
                return
            } else {
                self.loadingHud.dismiss()
                let defaults = UserDefaults.standard
                defaults.set(verificationID, forKey: "authVID")
                self.performSegue(withIdentifier: "verifyPhone_Segue", sender: self)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verifyPhone_Segue" {
            let dist = segue.destination as! VerifyPhoneViewController
            if signupType == "social" {
                dist.signupType = "social"
                dist.registerSocialBody = self.socialBody
            } else if signupType == "normal" {
                dist.signupType = "normal"
                dist.registerBody = self.normalBody
            }
        }
    }
    
    private func validateSocialSignupData() -> CreateSocialBody? {
        // validate name is correct format
        guard let firstName = self.fullName_TextField.text, firstName.count > 3 else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Name", message: "Please enter valid name")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate age is correct format
        guard selectedDate != "" else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect birthdate", message: "Please select valid birthdate")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate gender is correct format
    /*    guard selectedGender != "" else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Gender", message: "Please select valid gender")
            self.present(alert, animated: true, completion: nil)
            return nil
        }*/
        
        // validate city is correct format
        guard selectedCity != "" else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect City", message: "Please select valid city")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate area is correct format
        guard selectedArea != "" else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Area", message: "Please select valid area")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate email is correct format
        guard let email = self.email_TextField.text, email.validateEmailFormat() else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Email", message: "Please enter valid email")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate phone number is correct format
        guard let mobileNumber = self.phoneNumber_TextField.text, mobileNumber.validatePhoneNumber(), mobileNumber.count == 11 else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Mobile Number", message: "Please enter valid mobile number")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate password is correct format
//        guard let password = self.password_TextField.text, password.count >= 8 else {
//            let alert = UIViewHelper.errorAlert(title: "Incorrect Password", message: "Please enter valid password. (Password must be 6 characters at least)")
//            self.present(alert, animated: true, completion: nil)
//            return nil
//        }
        
        let socialBody = CreateSocialBody(mobileNumber: mobileNumber, email: self.socialDataSignup["email"]!, userName: self.socialDataSignup["name"]!, socialId: self.socialDataSignup["socialId"]!, socialName: "facebook", socialToken: self.socialDataSignup["socialToken"]!, birth_date: selectedDate, gender: selectedGender, city_id: selectedCityId, area_id: selectedAreaId)
        
//        let registerBody = RegisterBody(name: firstName, birth_date: selectedDate, gender: selectedGender, email: email, mobile_number: mobileNumber, password: password, city_id: selectedCityId, area_id: selectedAreaId)
        return socialBody
    }
    
    private func validateSignupData() -> RegisterBody? {
        // validate name is correct format
        guard let firstName = self.fullName_TextField.text, firstName.count > 3 else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Name", message: "Please enter valid name")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate age is correct format
        guard selectedDate != "" else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect birthdate", message: "Please select valid birthdate")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate gender is correct format
//        guard selectedGender != "" else {
//            let alert = UIViewHelper.errorAlert(title: "Incorrect Gender", message: "Please select valid gender")
//            self.present(alert, animated: true, completion: nil)
//            return nil
//        }
        
        // validate city is correct format
        guard selectedCity != "" else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect City", message: "Please select valid city")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate area is correct format
        guard selectedArea != "" else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Area", message: "Please select valid area")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate email is correct format
        guard let email = self.email_TextField.text, email.validateEmailFormat() else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Email", message: "Please enter valid email")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate phone number is correct format
        guard let mobileNumber = self.phoneNumber_TextField.text, mobileNumber.validatePhoneNumber(), mobileNumber.count == 11 else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Mobile Number", message: "Please enter valid mobile number")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        // validate password is correct format
        guard let password = self.password_TextField.text, password.count >= 8 else {
            let alert = UIViewHelper.errorAlert(title: "Incorrect Password", message: "Please enter valid password. (Password must be 6 characters at least)")
            self.present(alert, animated: true, completion: nil)
            return nil
        }
        
        let registerBody = RegisterBody(name: firstName, birth_date: selectedDate, gender: selectedGender, email: email, mobile_number: mobileNumber, password: password, city_id: selectedCityId, area_id: selectedAreaId)
        return registerBody
    }
    
    @IBAction func genderSelect(_ sender: UIButton) {
        // Signup functionality goes here
        let picker = TCPickerView()
        picker.title = "Gender"
        let cars = [
            "Male",
            "Female"
        ]
        let values = cars.map { TCPickerView.Value(title: $0) }
        picker.values = values
        picker.delegate = self
        picker.selection = .single
        picker.itemsFont = UIFont.systemFont(ofSize: 15, weight: .bold)
        picker.completion = { (selectedIndexes) in
            for i in selectedIndexes {
                print(values[i].title)
            }
        }
        picker.show()
    }
    
    @IBAction func ageSelect(_ sender: UIButton) {
        animateIn()
    }
    
    @IBAction func back(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        //self.navigationController?.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func donePickBirthdate(_ sender: UIButton) {
        datePickerView.datePickerMode = UIDatePickerMode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let selectedDate = dateFormatter.string(from: datePickerView.date)
        self.selectedDate = selectedDate
        self.age_TextField.text = self.selectedDate
        animateOut()
    }
    
    @IBAction func cancelPickBirthdate(_ sender: UIButton) {
        animateOut()
    }
    
    @IBAction func citySelect(_ sender: UIButton) {
        // Signup functionality goes here
        let picker = TCPickerView()
        picker.title = "City"
        let cars = self.cities.map { (city) -> String in
            return city.cityName!
        }
        let values = cars.map { TCPickerView.Value(title: $0) }
        picker.values = values
        picker.delegate = self
        picker.selection = .single
        picker.itemsFont = UIFont.systemFont(ofSize: 15, weight: .bold)
        picker.completion = { (selectedIndexes) in
            for i in selectedIndexes {
                print(values[i].title)
            }
        }
        picker.show()
    }
    
    @IBAction func areaSelect(_ sender: UIButton) {
        // Signup functionality goes here
        let picker = TCPickerView()
        picker.title = "Area"
        var cars = [String]()
        let _ = self.cities.map { (city) in
            if city.cityName == self.selectedCity {
                let _ = city.areas?.map({ (area) in
                    cars.append(area.areaName!)
                })
            }
        }
        let values = cars.map { TCPickerView.Value(title: $0) }
        picker.values = values
        picker.delegate = self
        picker.selection = .single
        picker.itemsFont = UIFont.systemFont(ofSize: 15, weight: .bold)
        picker.completion = { (selectedIndexes) in
            for i in selectedIndexes {
                print(values[i].title)
            }
        }
        picker.show()
    }
    
}

// MARK:- TCPickerView Delegate
extension SignupViewController: TCPickerViewDelegate {
    func pickerView(_ pickerView: TCPickerView, didSelectRowAtIndex index: Int) {
        if pickerView.title == "Gender" {
            self.selectedGender = pickerView.values[index].title
            self.gender_TextField.text = self.selectedGender
        } else if pickerView.title == "City" {
            self.selectedCity = pickerView.values[index].title
            let _ = self.cities.map({ (city) in
                if city.cityName == self.selectedCity {
                    self.selectedCityId = city.cityId!
                }
            })
            self.city_TextField.text = self.selectedCity
        } else if pickerView.title == "Area" {
            self.selectedArea = pickerView.values[index].title
            let _ = self.cities.map { (city) in
                if city.cityName == self.selectedCity {
                    let _ = city.areas?.map({ (area) in
                        if area.areaName == self.selectedArea {
                            self.selectedAreaId = area.areaId!
                        }
                    })
                }
            }
            self.area_TextField.text = self.selectedArea
        }
    }
}

// MARK:- Textfield Delegate Functions
extension SignupViewController: TextFieldDelegate {
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        //(textField as? ErrorTextField)?.isErrorRevealed = true
        // Make Validation Here if needed
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    private func initTextfieldsUI() {
        setupEmailTextField()
        setupFullNameTextField()
        setupPasswordTextField()
        setupPhoneNumberTextField()
    }
    
    private func setupEmailTextField() {
       // email_TextField.placeholder = "Email"
        email_TextField.detail = "Error, incorrect email"
        email_TextField.isClearIconButtonEnabled = false
        email_TextField.delegate = self
        email_TextField.isPlaceholderUppercasedWhenEditing = false
        email_TextField.textColor = UIColor.white
        email_TextField.dividerColor = UIColor.white
        email_TextField.dividerActiveColor = UIColor.white
        email_TextField.placeholderNormalColor = UIColor.white
        email_TextField.placeholderActiveColor = UIColor.white
        email_TextField.clearIconButton?.tintColor = .black
    }
    
    private func setupFullNameTextField() {
    //    fullName_TextField.placeholder = "Full Name"
        fullName_TextField.detail = "Error, incorrect name"
        fullName_TextField.isClearIconButtonEnabled = false
        fullName_TextField.delegate = self
        fullName_TextField.isPlaceholderUppercasedWhenEditing = false
        fullName_TextField.textColor = UIColor.white
        fullName_TextField.dividerColor = UIColor.white
        fullName_TextField.dividerActiveColor = UIColor.white
        fullName_TextField.placeholderNormalColor = UIColor.white
        fullName_TextField.placeholderActiveColor = UIColor.white
        fullName_TextField.clearIconButton?.tintColor = .black
    }
    
    private func setupPhoneNumberTextField() {
     //   phoneNumber_TextField.placeholder = "Phone Number"
        phoneNumber_TextField.detail = "Error, incorrect phone number"
        phoneNumber_TextField.isClearIconButtonEnabled = false
        phoneNumber_TextField.delegate = self
        phoneNumber_TextField.isPlaceholderUppercasedWhenEditing = false
        phoneNumber_TextField.textColor = UIColor.white
        phoneNumber_TextField.dividerColor = UIColor.white
        phoneNumber_TextField.dividerActiveColor = UIColor.white
        phoneNumber_TextField.placeholderNormalColor = UIColor.white
        phoneNumber_TextField.placeholderActiveColor = UIColor.white
        phoneNumber_TextField.clearIconButton?.tintColor = .black
    }
    
    private func setupPasswordTextField() {
    //    password_TextField.placeholder = "Password"
        password_TextField.detail = "Error, incorrect passwrod"
        password_TextField.isClearIconButtonEnabled = false
        password_TextField.delegate = self
        password_TextField.isPlaceholderUppercasedWhenEditing = false
        password_TextField.textColor = UIColor.white
        password_TextField.dividerColor = UIColor.white
        password_TextField.dividerActiveColor = UIColor.white
        password_TextField.placeholderNormalColor = UIColor.white
        password_TextField.placeholderActiveColor = UIColor.white
        password_TextField.clearIconButton?.tintColor = .black
    }
}
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
