//
//  UIColorExtensions.swift
//  Elusive
//
//  Created by Graphic on 3/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

public extension UIColor {
    
    public class var black48: UIColor {
        get {
            return UIColor(red: 48.0/255.0, green: 48.0/255.0, blue: 48.0/255.0, alpha: 1.0)
        }
    }
    
    public class var gray139: UIColor {
        get {
            return UIColor(red: 193.0/255.0, green: 193.0/255.0, blue: 193.0/255.0, alpha: 1.0)
        }
    }
    public class var black13: UIColor {
        get {
            return UIColor(red: 11.0/255.0, green: 12.0/255.0, blue: 27.0/255.0, alpha: 1.0)
        }
    }
    
    public class var elusiveGreen: UIColor {
        get {
            return UIColor(red: 98.0/255.0, green: 235.0/255.0, blue: 224.0/255.0, alpha: 1.0)
        }
    }
    
}
