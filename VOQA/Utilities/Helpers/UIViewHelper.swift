//
//  UIViewHelper.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class UIViewHelper {
    
    static func errorAlert(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Okay", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
    
    static func connectionErrorAlert() -> UIAlertController {
        let alert = UIAlertController(title: "Connection Error", message: "Error while connecting to the server. Please connect to internet and try again.", preferredStyle: .alert)
        let action = UIAlertAction(title: "Okay", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
    static func invalidToken() -> UIAlertController {
        let alert = UIAlertController(title:  "Error", message: "Invalid Token", preferredStyle: .alert)
        let action = UIAlertAction(title: "Okay", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }

    
}
