//
//  AudioHelper.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

var appHasMicAccess = false

enum AudioStatus: Int, CustomStringConvertible {
    case Stopped = 0,
    Playing,
    Recording
    
    var audioName: String {
        let audioNames = [
            "Audio: Stopped",
            "Audio:Playing",
            "Audio:Recording"]
        return audioNames[rawValue]
    }
    
    var description: String {
        return audioName
    }
}
