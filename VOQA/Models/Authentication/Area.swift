//
//  Area.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Area: Mappable {
    
    public var areaId: Int?
    public var areaName: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        areaId           <- map["areaId"]
        areaName           <- map["areaName"]
    }
}
