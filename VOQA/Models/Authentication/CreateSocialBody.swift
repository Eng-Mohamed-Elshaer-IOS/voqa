//
//  CreateSocialBody.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

open class CreateSocialBody: JSONEncodable {
    
    public var birth_date: String?
    public var gender: String?
    public var city_id: Int?
    public var area_id: Int?
    public var mobileNumber: String?
    public var email: String?
    public var userName: String?
    public var socialId: String?
    public var socialName: String?
    public var socialToken: String?
    
    public init(mobileNumber: String, email: String, userName: String, socialId: String, socialName: String, socialToken: String, birth_date: String, gender: String, city_id: Int, area_id: Int) {
        self.mobileNumber = mobileNumber
        self.email = email
        self.userName = userName
        self.socialId = socialId
        self.socialName = socialName
        self.socialToken = socialToken
        self.birth_date = birth_date
        self.gender = gender
        self.city_id = city_id
        self.area_id = area_id
    }
    
    // MARK: JSONEncodable
    open func encodeToJSON() -> [String: Any] {
        var nillableDictionary = [String: Any?]()
        nillableDictionary["birth_date"] = self.birth_date
        nillableDictionary["gender"] = self.gender
        nillableDictionary["city_id"] = self.city_id
        nillableDictionary["area_id"] = self.area_id
        nillableDictionary["mobileNumber"] = self.mobileNumber
        nillableDictionary["email"] = self.email
        nillableDictionary["userName"] = self.userName
        nillableDictionary["socialId"] = self.socialId
        nillableDictionary["socialName"] = self.socialName
        nillableDictionary["socialToken"] = self.socialToken
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
