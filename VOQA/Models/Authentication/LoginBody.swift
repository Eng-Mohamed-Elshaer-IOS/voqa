//
//  LoginBody.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

open class LoginBody: JSONEncodable {
    
    public var email: String?
    public var password: String?
    
    public init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    // MARK: JSONEncodable
    open func encodeToJSON() -> [String: Any] {
        var nillableDictionary = [String: Any?]()
        nillableDictionary["email"] = self.email
        nillableDictionary["password"] = self.password
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
