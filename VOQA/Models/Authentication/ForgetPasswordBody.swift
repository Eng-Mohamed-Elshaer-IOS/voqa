//
//  ForgetPasswordBody.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

open class ForgetPasswordBody: JSONEncodable {
    
    public var email: String?
    
    public init(email: String) {
        self.email = email
    }
    
    // MARK: JSONEncodable
    open func encodeToJSON() -> [String: Any] {
        var nillableDictionary = [String: Any?]()
        nillableDictionary["email"] = self.email
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
