//
//  CheckSocialBody.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

open class CheckSocialBody: JSONEncodable {
    
    public var socialId: String?
    public var socialName: String?
    
    public init(socialId: String, socialName: String) {
        self.socialId = socialId
        self.socialName = socialName
    }
    
    // MARK: JSONEncodable
    open func encodeToJSON() -> [String: Any] {
        var nillableDictionary = [String: Any?]()
        nillableDictionary["socialId"] = self.socialId
        nillableDictionary["socialName"] = self.socialName
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
