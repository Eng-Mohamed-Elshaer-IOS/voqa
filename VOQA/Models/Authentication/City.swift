//
//  City.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class City: Mappable {
    
    public var cityId: Int?
    public var cityName: String?
    public var areas: [Area]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        cityId           <- map["cityId"]
        cityName           <- map["cityName"]
        areas           <- map["areas"]
    }
}
