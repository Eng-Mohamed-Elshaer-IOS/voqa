//
//  User.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class User: Mappable {
    
    public var userId: Int?
    public var fullName: String?
    public var email: String?
    public var gender: String?
    public var mobileNumber: String?
    public var token: String?
    public var birthDate: String?
    public var cityId: Int?
    public var areaId: Int?
    public var totalPoints: Int?
    public var image: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        image           <- map["image"]
        birthDate           <- map["birthDate"]
        cityId           <- map["cityId"]
        areaId        <- map["areaId"]
        totalPoints           <- map["totalPoints"]
        userId           <- map["userId"]
        fullName        <- map["fullName"]
        email           <- map["email"]
        gender           <- map["gender"]
        mobileNumber        <- map["mobileNumber"]
        token           <- map["token"]
    }
}
