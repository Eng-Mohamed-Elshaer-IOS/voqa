//
//  RegisterBody.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

open class RegisterBody: JSONEncodable {
    
    public var name: String?
    public var birth_date: String?
    public var gender: String?
    public var email: String?
    public var mobile_number: String?
    public var password: String?
    public var city_id: Int?
    public var area_id: Int?
    
    public init(name: String, birth_date: String, gender: String, email: String, mobile_number: String, password: String, city_id: Int, area_id: Int) {
        self.name = name
        self.birth_date = birth_date
        self.gender = gender
        self.email = email
        self.mobile_number = mobile_number
        self.password = password
        self.city_id = city_id
        self.area_id = area_id
    }
    
    // MARK: JSONEncodable
    open func encodeToJSON() -> [String: Any] {
        var nillableDictionary = [String: Any?]()
        nillableDictionary["name"] = self.name
        nillableDictionary["birth_date"] = self.birth_date
        nillableDictionary["gender"] = self.gender
        nillableDictionary["email"] = self.email
        nillableDictionary["mobile_number"] = self.mobile_number
        nillableDictionary["password"] = self.password
        nillableDictionary["city_id"] = self.city_id
        nillableDictionary["area_id"] = self.area_id
        let dictionary: [String:Any] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
