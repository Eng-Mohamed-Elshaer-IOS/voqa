//
//  MatchHistory.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/20/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class MatchHistoryObjectt: Mappable {
    
    public var historyId: Int?
    public var matchId: Int?

    public var userId: Int?
    public var localTeamScore: Int?
    public var visitorTeamScore: Int?
    public var checked: String?
    public var userMatchHistory: NewMatchHistory?

    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        historyId         <- map["id"]
        userId         <- map["user_id"]
        matchId         <- map["match_id"]
        localTeamScore         <- map["local_team_score"]
        visitorTeamScore         <- map["visitor_team_score"]
        userMatchHistory         <- map["match_history"]

        checked         <- map["checked"]
      //  visitorTeam         <- map["visitorTeam"]
    }
}
