//
//  MatchResult.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/21/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import UIKit

class MatchResult: NSObject {
    public var localTeamName: String?
    public var visitorTeamName: String?
    public var localLogo: String?
    public var visitorLogo: String?
    public var localRealScore: String?
    public var visitorRealScore: String?

}
