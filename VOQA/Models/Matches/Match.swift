//
//  Match.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Match: Mappable {
    
    public var matchId: Int?
    public var leagueName: String?
    public var status: String?
    public var startingAt: String?
    public var localTeam: Team?
    public var visitorTeam: Team?
    public var myExpect: Expect?

    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        matchId         <- map["matchId"]
        leagueName         <- map["leagueName"]
        status         <- map["status"]
        startingAt         <- map["startingAt"]
        localTeam         <- map["localTeam"]
        visitorTeam         <- map["visitorTeam"]
        myExpect         <- map["my_expect"]

    }
}
