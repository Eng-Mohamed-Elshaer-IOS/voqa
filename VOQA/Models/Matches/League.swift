//
//  League.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class League: Mappable {
    
    public var leagueName: String?
    public var matches: [Match]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        leagueName         <- map["leagueName"]
        matches         <- map["matches"]
    }
}
