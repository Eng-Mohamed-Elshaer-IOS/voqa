//
//  Expect.swift
//  VOQA
//
//  Created by Amr El-Hagry on 12/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Expect: Mappable {
    /*
     "id": 0,
     "user_id": "0",
     "match_id": "0",
     "local_team_score": "0",
     "visitor_team_score": "0",
     "checked": ""
     */
    public var id: Int?
    public var userId: String?
    public var matchId: String?
    public var localScore: Int?
    public var visitorScore: Int?
    public var checked: String?


    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        id         <- map["id"]
        userId         <- map["user_id"]
        matchId         <- map["match_id"]
        visitorScore         <- map["visitor_team_score"]
        localScore         <- map["local_team_score"]
        checked         <- map["checked"]

    }
}
