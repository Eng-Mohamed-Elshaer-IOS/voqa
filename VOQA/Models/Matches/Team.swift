//
//  Team.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Team: Mappable {
    
    public var name: String?
    public var score: Int?
    public var logo: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        name         <- map["name"]
        score         <- map["score"]
        logo         <- map["logo"]
    }
}
