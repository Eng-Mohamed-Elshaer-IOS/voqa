//
//  NewMatchHistory.swift
//  VOQA
//
//  Created by Amr El-Hagry on 8/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class NewMatchHistory: Mappable {
    /*
     "id": 3,
     "match_id": 10359019,
     "local_team_score": 2,
     "visitor_team_score": 0,
     "local_team_logo": "https://cdn.sportmonks.com/images/soccer/teams/23/18743.png",
     "visitor_team_logo": "https://cdn.sportmonks.com/images/soccer/teams/21/18645.png",
     "local_team": "Belgium",
     "visitor_team": "England",
     "created_at": "2018-07-14 15:53:02",
     "updated_at": "2018-07-14 15:53:02"
     */
    public var historyId: Int?
    public var matchId: Int?
    
    public var userId: Int?
    public var localTeamScore: Int?
    public var visitorTeamScore: Int?
    public var localTeamLogo: String?
    public var visitorTeamLogo: String?
    public var localTeam: String?
    public var visitorTeam: String?

    public var checked: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        historyId         <- map["id"]
       // userId         <- map["user_id"]
        matchId         <- map["match_id"]
        localTeamScore         <- map["local_team_score"]
        visitorTeamScore         <- map["visitor_team_score"]
        localTeamLogo         <- map["local_team_logo"]
        visitorTeamLogo         <- map["visitor_team_logo"]
        localTeam         <- map["local_team"]
        visitorTeam         <- map["visitor_team"]

       // checked         <- map["checked"]
        //  visitorTeam         <- map["visitorTeam"]
    }
}
