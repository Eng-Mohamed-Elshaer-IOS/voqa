//
//  NewsObject.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class NewsObject: Mappable {
    
    public var id: Int?
    public var title: String?
    public var link: String?
    public var description: String?
    public var image: String?
    public var source: String?
    public var created_at: String?
    public var updated_at: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        id           <- map["id"]
        title           <- map["title"]
        link           <- map["link"]
        source           <- map["source"]
        description           <- map["description"]
        image           <- map["image"]
        created_at           <- map["created_at"]
        updated_at           <- map["updated_at"]
    }
}

