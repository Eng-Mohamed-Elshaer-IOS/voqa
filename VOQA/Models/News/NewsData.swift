//
//  NewsData.swift
//  VOQA
//
//  Created by Amr El-Hagry on 7/7/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class NewsData: Mappable {
    
    public var data: News?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
}
