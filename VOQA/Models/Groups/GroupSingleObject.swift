//
//  GroupSingleObject.swift
//  VOQA
//
//  Created by Amr El-Hagry on 11/4/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

open class GroupSingleObject: Mappable {
    public var data: GroupsObject?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
    
    
}
