//
//  InvitationObject.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/19/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//
/*
 "id": 7,
 "user_id": 701,
 "group_id": 3,
 "role": 3,
 "approved": 0,
 "created_at": "2018-10-17 09:10:35",
 "updated_at": "2018-10-17 09:10:35"
 
 */
import ObjectMapper

open class InvitationObject: Mappable {
    public var invitationId: Int?
    public var userId: Int?
    public var groupId: Int?
    public var role: Int?
    public var approved: Int?
    
    public var createdAt: String?
    public var updatedAt: String?
    public var group: GroupsObject?

    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        invitationId         <- map["id"]
        userId         <- map["user_id"]
        groupId         <- map["group_id"]
        role         <- map["role"]
        approved         <- map["approved"]
        createdAt         <- map["created_at"]
        updatedAt         <- map["updated_at"]
        group         <- map["group"]

        
    }
    
}
