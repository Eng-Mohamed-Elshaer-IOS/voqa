//
//  MemberObject.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
/*
 "city_id": 1,
 "area_id": 43,
 "age": null,
 "mobile_number": "01114590092",
 
 */

import ObjectMapper

open class MemberObject: Mappable {
    public var memberId: Int?
    public var cityId: Int?
    public var areaId: Int?

    public var name: String?
    public var email: String?
    public var mobile: String?

    public var status: String?
    public var type: String?
    public var gender: String?
    public var birthDate: String?

    public var image: String?
    public var totalPoints: Int?
    public var actualPoints: Int?
    
    public var createdAt: String?
    public var updatedAt: String?
    
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        memberId         <- map["id"]
        cityId         <- map["city_id"]
        areaId         <- map["area_id"]

        name         <- map["name"]
        email         <- map["email"]
        status         <- map["status"]
        gender         <- map["gender"]
        type         <- map["type"]
        birthDate         <- map["birth_date"]
        mobile         <- map["mobile_number"]

        image         <- map["image"]
        totalPoints         <- map["total_points"]
        actualPoints         <- map["actual_points"]
        createdAt         <- map["created_at"]
        updatedAt         <- map["updated_at"]
        
        
    }
    
}
