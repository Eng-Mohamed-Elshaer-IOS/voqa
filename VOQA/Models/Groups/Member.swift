//
//  Member.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/21/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import ObjectMapper

open class Member: Mappable {
    public var data: [MemberObject]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
    
    
}
