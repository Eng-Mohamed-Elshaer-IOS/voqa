//
//  Invitation.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/19/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Invitation: Mappable {
    public var data: [InvitationObject]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
    
    
}
