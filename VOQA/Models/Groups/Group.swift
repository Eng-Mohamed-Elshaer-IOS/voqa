//
//  Group.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/8/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Group: Mappable {
    public var data: [GroupsObject]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
    
    
}
