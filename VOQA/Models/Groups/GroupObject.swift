//
//  GroupObject.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/8/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
/*
 "id": 3,
 "name": "Test group3",
 "image": "none",
 "description": null,
 "total_points": 10,
 "actual_points": 10,
 "created_at": "2018-10-01 02:04:39",
 "updated_at": "2018-10-01 02:04:39"
 */


import ObjectMapper

open class GroupsObject: Mappable {
    public var groupId: Int?
    public var name: String?
   // public var description: String?
    public var image: String?
    public var totalPoints: Int?
    public var actualPoints: Int?

    public var createdAt: String?
    public var updatedAt: String?
    public var users: [MemberObject]?

    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        groupId         <- map["id"]
        name         <- map["name"]
        image         <- map["image"]
        totalPoints         <- map["total_points"]
        actualPoints         <- map["actual_points"]
        createdAt         <- map["created_at"]
        updatedAt         <- map["updated_at"]
        users         <- map["users"]

    //    description         <- map["description"]

    }
    
}
