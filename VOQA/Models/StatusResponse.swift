//
//  StatusResponse.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class StatusResponse: Mappable {
    
    public var code: Int?
    public var message: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        code           <- map["code"]
        message        <- map["message"]
    }
}
