//
//  Award.swift
//  Elusive
//
//  Created by Graphic on 4/3/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Award: Mappable {
    
    public var id: Int?
    public var name: String?
    public var description: String?
    public var nameAr: String?
    public var descriptionAr: String?

    public var image: String?
    public var price: Int?
    public var isAvailable: Int?

    public var created_at: String?
    public var updated_at: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        id           <- map["id"]
        name           <- map["name"]
        description           <- map["description"]
        nameAr           <- map["name_ar"]
        descriptionAr           <- map["description_ar"]

        image           <- map["image"]
        price           <- map["price"]
        isAvailable           <- map["available"]

        created_at           <- map["created_at"]
        updated_at           <- map["updated_at"]
    }
}

