//
//  Awards.swift
//  Elusive
//
//  Created by Graphic on 4/3/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Awards: Mappable {
    
    public var data: [Award]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
}
