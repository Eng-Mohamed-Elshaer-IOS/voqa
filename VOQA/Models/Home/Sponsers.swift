//
//  Sponsers.swift
//  Elusive
//
//  Created by Graphic on 4/2/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Sponsers: Mappable {
    
    public var sponsors: [Sponser]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        sponsors           <- map["sponsors"]
    }
}
