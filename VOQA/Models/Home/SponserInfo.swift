//
//  SponserInfo.swift
//  Elusive
//
//  Created by Graphic on 4/2/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class SponserInfo: Mappable {
    
    public var sponsor: Sponser?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        sponsor           <- map["sponsor"]
    }
}
