//
//  Club.swift
//  Elusive
//
//  Created by Graphic on 4/2/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Club: Mappable {
    
    public var id: Int?
    public var name: String?
    public var short_name: String?
    public var logo: String?
    public var created_at: String?
    public var updated_at: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        id           <- map["id"]
        name        <- map["name"]
        short_name           <- map["short_name"]
        logo        <- map["logo"]
        created_at           <- map["created_at"]
        updated_at        <- map["updated_at"]
    }
}
