//
//  Sponser.swift
//  Elusive
//
//  Created by Graphic on 4/2/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Sponser: Mappable {
    
    public var id: Int?
    public var name: String?
    public var image: String?
    public var url: String?
    public var created_at: String?
    public var updated_at: String?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        id           <- map["id"]
        name        <- map["name"]
        image           <- map["image"]
        url        <- map["url"]
        created_at           <- map["created_at"]
        updated_at        <- map["updated_at"]
    }
}
