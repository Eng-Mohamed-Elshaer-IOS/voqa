//
//  Player.swift
//  Elusive
//
//  Created by Graphic on 4/2/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Player: Mappable {
    
    public var id: Int?
    public var name: String?
    public var details: String?
    public var date_of_birth: String?
    public var image: String?
    public var position: String?
    public var club_id: Int?
    public var created_at: String?
    public var updated_at: String?
    public var club: Club?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        id           <- map["id"]
        name           <- map["name"]
        details           <- map["details"]
        date_of_birth           <- map["date_of_birth"]
        image           <- map["image"]
        position           <- map["position"]
        club_id           <- map["club_id"]
        created_at           <- map["created_at"]
        updated_at           <- map["updated_at"]
        club           <- map["club"]
    }
}
