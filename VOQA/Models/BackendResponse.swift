//
//  BackendResponse.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class BackendResponse<T: Mappable>: Mappable {
    
    public var status: StatusResponse?
    public var data: T?
    public var errors: [String]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        status        <- map["status"]
        data          <- map["data"]
        errors        <- map["errors"]
    }
}
