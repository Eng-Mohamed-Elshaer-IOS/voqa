//
//  Restaurant.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Restaurant: Mappable {
    public var data: [RestaurantsObject]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
    

}
