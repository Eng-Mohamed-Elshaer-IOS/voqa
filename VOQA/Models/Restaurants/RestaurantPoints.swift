//
//  RestaurantPoints.swift
//  VOQA
//
//  Created by Amr El-Hagry on 8/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class RestaurantPoints: Mappable {
    //public var data: [RestaurantAward]?
    public var points: Int?

    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        points           <- map["points"]
    }
    
    
}
