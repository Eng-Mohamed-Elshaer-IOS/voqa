//
//  RestaurantAward.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/20/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//
/*
 "id": 7,
 "user_id": 139,
 "award_id": 6,
 "code": "TQBM2S",
 "is_used": 0,
 "created_at": "2018-06-15 21:25:53",
 "updated_at": "2018-06-15 21:25:53",
 "restaurant_id": 3
 
 
 */
import Foundation
import ObjectMapper

open class RestaurantAward: Mappable {
    public var codeId: Int?

    public var restaurantId: Int?
    public var awardId: Int?
    public var userId: Int?

    public var code: String?
    public var isUsed: Int?
    public var description: String?
    
    public var image: String?
    public var totalRating: Int?
    public var actualRating: Float?
    public var ratersNo: Int?
    public var createdDate: String?

 //   public var awards: [Restaurantaw]?
    
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        codeId         <- map["id"]

        restaurantId         <- map["restaurant_id"]
        awardId         <- map["award_id"]
        userId         <- map["user_id"]

        code         <- map["code"]
        isUsed         <- map["is_used"]
        description         <- map["description"]
        image         <- map["image"]
        createdDate         <- map["created_at"]

       // addresses         <- map["user_awards"]
        
        
    }
    
}
