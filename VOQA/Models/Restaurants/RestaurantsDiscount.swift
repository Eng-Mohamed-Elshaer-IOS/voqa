//
//  RestaurantsDiscount.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/20/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class RestaurantsDiscount: Mappable {
    public var data: [RestaurantDiscount]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
    
    
}
