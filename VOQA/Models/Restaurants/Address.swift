//
//  Address.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Address: Mappable {
//    "id": 25,
//    "restaurant_id": "3",
//    "address": "to",
    public var addressId: Int?
    public var restaurantId: String?
    public var address: String?

    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        addressId         <- map["id"]
        restaurantId         <- map["restaurant_id"]
        address         <- map["address"]

    }
}
