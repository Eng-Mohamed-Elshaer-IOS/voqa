//
//  RestaurantsObject.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class RestaurantsObject: Mappable {
    public var restaurantId: Int?
    public var name: String?
    public var nameAr: String?

    public var phone: String?
    public var description: String?
    public var descriptionAr: String?

    public var image: String?
    public var totalRating: Int?
    public var actualRating: Float?
    public var ratersNo: Int?
      public var addresses: [Address]?
    
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        restaurantId         <- map["id"]
        name         <- map["name"]
        nameAr         <- map["name_ar"]

        phone         <- map["phone"]
        description         <- map["description"]
        descriptionAr         <- map["description_ar"]

        image         <- map["image"]
        totalRating         <- map["total_rating"]
        actualRating         <- map["actual_rating"]
        ratersNo         <- map["raters_number"]
         addresses         <- map["addresses"]
        
        
    }
  
}
