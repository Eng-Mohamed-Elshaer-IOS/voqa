//
//  RankProfile.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import ObjectMapper

open class Rankings: Mappable {
    
    public var data: RankInformation?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        data           <- map["data"]
    }
}

open class RankInformation: Mappable {
    
    public var user: UserRankInfo?
    public var top: [RankProfile]?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        user           <- map["user"]
        top           <- map["top"]
    }
}

open class UserRankInfo: Mappable {
    
    public var before: RankProfile?
    public var after: RankProfile?
    public var user: RankProfile?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        before           <- map["before"]
        after           <- map["after"]
        user           <- map["user"]
    }
}

open class RankProfile: Mappable {
    
    public var id: Int?
    public var name: String?
    public var image: String?
    public var points: Int?
    public var rank: Int?
    
    required public init?(map: Map) {
        
    }
    
    // Mappable
    public func mapping(map: Map) {
        id           <- map["id"]
        name           <- map["name"]
        image           <- map["image"]
        points           <- map["points"]
        rank           <- map["rank"]
    }
}
