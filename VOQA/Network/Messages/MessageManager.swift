//
//  MessageManager.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class MessageManager {
    
    static func getURLforMemo() -> NSURL {
        let tempDir = NSTemporaryDirectory()
        let filePath = tempDir + "/UsiveChat.caf"
        
        return NSURL.fileURL(withPath: filePath) as NSURL
    }
    
    static func uploadFile(completion:  @escaping (_ :News?,_ :Error?)->Void)
    {
        let url = Constants.URLs.baseUrl + "api/messages/send?to=1"
        let urlString = URL(string: url)
        let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(getURLforMemo() as URL, withName: "file")
        }, usingThreshold: UInt64.init(), to: urlString!, method: HTTPMethod.post, headers: [Constants.headers.Authorization: token]) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    switch(response.result) {
                    case .success(let data):
                        print(data)
                    case .failure(let error):
                        print(error)
                    }
                })
            case .failure(let encodingError):
                print(encodingError)
            }
        }
        
//        Alamofire.request(NewsRouter.getAllNews())
//            .responseJSON {
//                response in
//
//                switch(response.result) {
//
//                case .success(let data):
//                    let rep = News(JSON: data as! [String:Any])
//                    completion(rep, nil)
//
//                case .failure(let error):
//                    completion(nil, error)
//                }
//        }
    }
}
