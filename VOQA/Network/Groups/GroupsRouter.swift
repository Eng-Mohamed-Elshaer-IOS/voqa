//
//  GroupsRouter.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/8/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum GroupsRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl
    
    // Home
    case getAllGroups()
    case searchUser(([String:Any]))
    case getGroupsAsAdmin(([String:Any]))
    case getGroupsAsAny(([String:Any]))
    case getMembersInGroup(([String:Any]))

    case getPendingInvitations(([String:Any]))
    case accpetInvite(([String:Any]))
    case rejectInvite(([String:Any]))
    case inviteUsersToGroup(([String:Any]))
    case createGroup(([String:Any]))
    case editGroup(([String:Any]))
    case leaveGroup(([String:Any]))
    case deleteGroup(([String:Any]))

    case generateLink(([String:Any]))

    func asURLRequest() throws -> URLRequest {
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .getAllGroups:
                relativePath = Constants.URLs.getAllGroups
            case .searchUser:
                relativePath = Constants.URLs.searchUser
            case .getGroupsAsAdmin:
                relativePath = Constants.URLs.adminInGroups
            case .getGroupsAsAny:
                relativePath = Constants.URLs.userInGroups
            case .getPendingInvitations:
                relativePath = Constants.URLs.pendingInvitation
            case .getMembersInGroup:
                relativePath = Constants.URLs.membersInGroup
            case .accpetInvite:
                relativePath = Constants.URLs.acceptInvitation
            case .rejectInvite:
                relativePath = Constants.URLs.rejectInvitation
            case .inviteUsersToGroup:
                relativePath = Constants.URLs.inviteUsersToGtoup
            case .createGroup:
                relativePath = Constants.URLs.createGroup
            case .editGroup:
                relativePath = Constants.URLs.editGroup
            case .leaveGroup:
                relativePath = Constants.URLs.leaveGroup
            case .generateLink:
                relativePath = Constants.URLs.generateLink
            case .deleteGroup:
                relativePath = Constants.URLs.deleteGroup

                
            }
            var url = URL(string: GroupsRouter.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            print("GET ALL REST \(url)")
            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .getAllGroups:
                return .get
            case .searchUser:
                return .post
            case .getGroupsAsAdmin:
                return .post
            case .getGroupsAsAny:
                return .post
            case .getPendingInvitations:
                return .post
            case .getMembersInGroup:
                return .post
            case .accpetInvite:
                return .post
            case .rejectInvite:
                return .post
            case .inviteUsersToGroup:
                return .post
            case .createGroup:
                return .post
            case .editGroup:
                return .post
            case .leaveGroup:
                return .post
            case .generateLink:
                return .post
            case .deleteGroup:
                return .post

            }
        }
        let params: ([String: Any]?) = {
            switch self
            {
            case .getAllGroups:
                return nil
            case .searchUser(let data):
                return data
            case .getGroupsAsAdmin(let data):
                return data
            case .getGroupsAsAny(let data):
                return data
            case .getPendingInvitations(let data):
                return data
            case .getMembersInGroup(let data):
                return data
            case .accpetInvite(let data):
                return data
            case .rejectInvite(let data):
                return data
            case .inviteUsersToGroup(let data):
                return data
            case .createGroup(let data):
                return data
            case .editGroup(let data):
                return data
            case .leaveGroup(let data):
                return data
            case .generateLink(let data):
                return data
            case .deleteGroup(let data):
                return data

            }
        }()
        
        var urlRequest = URLRequest(url: url)
        
        switch self {
        case .getAllGroups,.searchUser,.getGroupsAsAdmin,.getGroupsAsAny,.getPendingInvitations,.getMembersInGroup,.rejectInvite,.accpetInvite,.inviteUsersToGroup,.createGroup,.editGroup,.leaveGroup,.generateLink,.deleteGroup:
            let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
            print("TOKEn \(token)")
            urlRequest.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60
        
        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
