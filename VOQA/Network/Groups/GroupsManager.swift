//
//  GroupsManager.swift
//  VOQA
//
//  Created by Amr El-Hagry on 10/8/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON

class GroupsManager
{
    static func getAllGroups(completion:  @escaping (_ :Group?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.getAllGroups())
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let groupsResponse = Group(JSON: data as! [String:Any])
                    completion(groupsResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func getAllGroupsAsAdmin( body: [String: Any],completion:  @escaping (_ :Group?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.getGroupsAsAdmin(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data ):
                    //print("Data \(data)")               //   var  array = (data! as! NSArray).mutableCopy() as! NSMutableArray
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let restaurantsResponse = Group(JSON: data as! [String:Any])
                    completion(restaurantsResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func getAllGroupsAsAny( body: [String: Any],completion:  @escaping (_ :Group?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.getGroupsAsAny(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data ):
                    //print("Data \(data)")               //   var  array = (data! as! NSArray).mutableCopy() as! NSMutableArray
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let groupResponse = Group(JSON: data as! [String:Any])
                    completion(groupResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func getAllPendingNotification( body: [String: Any],completion:  @escaping (_ :Invitation?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.getPendingInvitations(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data ):
                    //print("Data \(data)")               //   var  array = (data! as! NSArray).mutableCopy() as! NSMutableArray
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let invitationResponse = Invitation(JSON: data as! [String:Any])
                    completion(invitationResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func getMembersInGroup( body: [String: Any],completion:  @escaping (_ :Member?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.getMembersInGroup(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data ):
                    //print("Data \(data)")               //   var  array = (data! as! NSArray).mutableCopy() as! NSMutableArray
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let memberResponse = Member(JSON: data as! [String:Any])
                    completion(memberResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func accpetInvite(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.accpetInvite(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["status"]["message"].string!), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func rejectInvite(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.rejectInvite(body))
            .responseJSON {
                response in
                
                switch(response.result)
                {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["status"]["message"].string!), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func deleteGroup(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.deleteGroup(body))
            .responseJSON {
                response in
                
                switch(response.result)
                {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["status"]["message"].string!), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func searchMemeber( body: [String: Any],completion:  @escaping (_ :Member?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.searchUser(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data ):
                    //print("Data \(data)")               //   var  array = (data! as! NSArray).mutableCopy() as! NSMutableArray
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let memberResponse = Member(JSON: data as! [String:Any])
                    completion(memberResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }

    
    static func createGroup(imgData:Data,body: [String: String], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
         let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
        Alamofire.upload(multipartFormData: { multipartFormData in
       //     multipartFormData.append(imgData, withName: "image")
            print("DATA \(imgData)")
            multipartFormData.append(imgData, withName: "image", fileName: "file.png", mimeType: "image/png")
            for (key, value) in body {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            } //Optional for extra parameters
        },
                         to:Constants.URLs.baseUrl+Constants.URLs.createGroup,       method:.post,
                         headers:["Authorization": "\(token)",Constants.headers.applicationTypeKey:Constants.headers.applicationTypeValue],
                         encodingCompletion: { result in
       
            switch result
            {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response.result.value)
                    let json = JSON(response.result.value!)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, "\(json["data"]["id"])"), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                }
                
            case .failure(let encodingError):
                completion(nil, encodingError)
            }
        })

    }
    static func editGroup(imgData:Data,body: [String: String], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
        Alamofire.upload(multipartFormData: { multipartFormData in
            //     multipartFormData.append(imgData, withName: "image")
            print("DATA \(imgData)")
            multipartFormData.append(imgData, withName: "image", fileName: "file.png", mimeType: "image/png")
            for (key, value) in body {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            } //Optional for extra parameters
        },
                         to:Constants.URLs.baseUrl+Constants.URLs.editGroup,       method:.post,
                         headers:["Authorization": "\(token)",Constants.headers.applicationTypeKey:Constants.headers.applicationTypeValue],
                         encodingCompletion: { result in
                            
                            switch result
                            {
                            case .success(let upload, _, _):
                                
                                upload.uploadProgress(closure: { (progress) in
                                    print("Upload Progress: \(progress.fractionCompleted)")
                                })
                                
                                upload.responseJSON { response in
                                    print(response.result.value)
                                    let json = JSON(response.result.value!)
                                    let status = json["status"]["code"].int
                                    if status == 200 {
                                        completion((true, "\(json["data"]["id"])"), nil)
                                    } else {
                                        completion((false, json["status"]["message"].string!), nil)
                                    }
                                }
                                
                            case .failure(let encodingError):
                                completion(nil, encodingError)
                            }
        })
    }
    static func leaveGroup(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.leaveGroup(body))
            .responseJSON {
                response in
                
                switch(response.result)
                {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["status"]["message"].string!), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func inviteMembersToGroup(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.inviteUsersToGroup(body))
            .responseJSON {
                response in
                
                switch(response.result)
                {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["status"]["message"].string!), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func generateLink(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(GroupsRouter.generateLink(body))
            .responseJSON {
                response in
                
                switch(response.result)
                {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["data"].string!), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
}
