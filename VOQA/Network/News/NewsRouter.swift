//
//  NewsRouter.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum NewsRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl

    // Home
    case getAllNews([String: Any])
    
    func asURLRequest() throws -> URLRequest {
        
        let url: URL? = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .getAllNews:
                relativePath = Constants.URLs.getAllNews + "/" + "\(Constants.headers.pageIndex)"
            }
            var url = URL(string: AuthRouter.baseURL)!
            if let relativePath = relativePath {
                //var url = URL(string: NewsRouter.baseURL + relativePath + "?page=2")
                
                url = url.appendingPathComponent(relativePath)
                
         //       url = url.appendingPathComponent("?page=2")
                print("URL \(url)")
         
            }
            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .getAllNews:
                return .get
            }
        }
        let params: ([String: Any]?) = {
            switch self {
            case .getAllNews:
                return nil
            }
        }()
        
        var urlRequest = URLRequest(url: url!)
        
        switch self {
        case .getAllNews:
            let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
            print("TOKEN \(token) \(Constants.headers.pageIndex) ")
            urlRequest.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            urlRequest.addValue("\(Constants.headers.pageIndex)", forHTTPHeaderField: Constants.headers.page)
            

            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
    ///urlRequest.url = URL(string: NewsRouter.baseURL + relativePath + "?page=2")
       // print("URL REQ \(urlRequest.url.encodin)")
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60

        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
