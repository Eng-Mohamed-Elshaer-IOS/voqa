//
//  NewsManager.swift
//  Elusive
//
//  Created by Graphic on 4/10/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class NewsManager {
    
    static func getAllNews(body: [String: Any],completion:  @escaping (_ :News?,_ :Error?)->Void)
    {
        Alamofire.request(NewsRouter.getAllNews(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = News(JSON: data as! [String:Any])
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
}
