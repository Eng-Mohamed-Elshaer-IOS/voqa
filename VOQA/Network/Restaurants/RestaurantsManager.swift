//
//  RestaurantsManager.swift
//  VOQA
//
//  Created by Amr El-Hagry on 6/11/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON

class RestaurantsManager
{
    static func getMyPoints( userId:String,completion:  @escaping (_ :RestaurantPoints?,_ :Error?)->Void)
    {
        Alamofire.request(RestaurantsRouter.getPoints(userId))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""
                    
                    let matchesResponse = RestaurantPoints(JSON: data as! [String:Any])
                    completion(matchesResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func getAllRestaurants(completion:  @escaping (_ :Restaurant?,_ :Error?)->Void)
    {
        Alamofire.request(RestaurantsRouter.getAllRestaurants())
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]

                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let restaurantsResponse = Restaurant(JSON: data as! [String:Any])
                    completion(restaurantsResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func getAllRestaurantDiscounts( body: [String: Any],completion:  @escaping (_ :RestaurantsDiscount?,_ :Error?)->Void)
    {
        Alamofire.request(RestaurantsRouter.getDiscountsByUserId(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data ):
                    //print("Data \(data)")               //   var  array = (data! as! NSArray).mutableCopy() as! NSMutableArray
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let restaurantsResponse = RestaurantsDiscount(JSON: data as! [String:Any])
                    completion(restaurantsResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
}
