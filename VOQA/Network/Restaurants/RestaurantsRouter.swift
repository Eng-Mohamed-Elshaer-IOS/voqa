
//
//  MatchesRouter.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum RestaurantsRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl
    
    // Home
    case getAllRestaurants()
    case winTenPoints()
    case expectMatch([String: Any])
    case getDiscountsByUserId([String:Any])
    case getPoints(String)

    func asURLRequest() throws -> URLRequest {
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .getAllRestaurants():
                relativePath = Constants.URLs.getAllRestaurants
            case .winTenPoints:
                relativePath = Constants.URLs.winTenPoints
            case .expectMatch:
                relativePath = Constants.URLs.expectMatch
            case .getDiscountsByUserId:
                relativePath = "\(Constants.URLs.restaurantDiscounts)"
                //\(id)"
            case .getPoints(let id):
                relativePath = "\(Constants.URLs.myPoints)\(id)"

                
            }
            var url = URL(string: MatchesRouter.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            print("GET ALL REST \(url)")
            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .getAllRestaurants:
                return .get
            case .expectMatch:
                return .post
            case .winTenPoints:
                return .get
            case .getDiscountsByUserId:
                return .post
            case .getPoints:
                return .get
            }
        }
        let params: ([String: Any]?) = {
            switch self
            {
            case .getAllRestaurants:
                return nil
            case .expectMatch(let data):
                return data
            case .winTenPoints:
                return nil
            case .getDiscountsByUserId(let data):
                return data
            case .getPoints:
                return nil

            }
        }()
        
        var urlRequest = URLRequest(url: url)
        
        switch self {
        case .getAllRestaurants, .winTenPoints, .expectMatch,.getDiscountsByUserId,.getPoints:
            let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
            urlRequest.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60

        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
