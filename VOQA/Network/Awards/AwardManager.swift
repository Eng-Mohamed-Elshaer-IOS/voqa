//
//  AwardManager.swift
//  Elusive
//
//  Created by Graphic on 4/3/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON
class AwardManager {
    
    static func getAllAwards(body: (String, String), completion:  @escaping (_ :Awards?,_ :Error?)->Void)
    {
        Alamofire.request(AwardRouter.getAllAwards(body.0, body.1))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = Awards(JSON: data as! [String:Any])
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func getAllAwardsByRestaurant(body: (String), completion:  @escaping (_ :Awards?,_ :Error?)->Void)
    {
//        Alamofire.request(AwardRouter.getAllAwardsPerRestaurant(body))
//            .responseJSON {
//                response in
//                
//                switch(response.result) {
//                    
//                case .success(let data):
//                    let rep = Awards(JSON: data as! [String:Any])
//                    completion(rep, nil)
//                    
//                case .failure(let error):
//                    completion(nil, error)
//                }
//        }
    }
    static func getAllAwardsByRestaurant(body: [String:Any], completion:  @escaping (_ :Awards?,_ :Error?)->Void)
    {
        Alamofire.request(AwardRouter.getAllAwardsPerRestaurant(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = Awards(JSON: data as! [String:Any])
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func buyAward(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(AwardRouter.userBuyAward(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["status"]["award_code"].string!), nil)
                    } else {
                        completion((false, json["status"]["message"].string!), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
//    static func buyAward(body: [String: Any], completion:  @escaping (_ :Awards?,_ :Error?)->Void)
//    {
//        Alamofire.request(AwardRouter.getAllAwards(body.0, body.1))
//            .responseJSON {
//                response in
//                
//                switch(response.result) {
//                    
//                case .success(let data):
//                    let rep = Awards(JSON: data as! [String:Any])
//                    completion(rep, nil)
//                    
//                case .failure(let error):
//                    completion(nil, error)
//                }
//        }
//    }
    
}
