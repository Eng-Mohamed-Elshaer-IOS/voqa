//
//  AwardRouter.swift
//  Elusive
//
//  Created by Graphic on 4/3/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum AwardRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl
    
    // Home
    case getAllAwards(String, String)
    case getAllAwardsPerRestaurant([String:Any])

    case userBuyAward([String: Any])
    
    func asURLRequest() throws -> URLRequest {
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .getAllAwards(let start, let end):
                relativePath = "\(Constants.URLs.getAllAwards)\(start)/\(end)"
            case .getAllAwardsPerRestaurant(let id):
                relativePath = "\(Constants.URLs.getAllAwardsByRestaurant)"

            case .userBuyAward:
                relativePath = Constants.URLs.userBuyAward
            }
            var url = URL(string: AuthRouter.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .getAllAwards:
                return .get
            case .userBuyAward:
                return .post
            case .getAllAwardsPerRestaurant:
                return .post

            }
        }
        let params: ([String: Any]?) = {
            switch self {
            case .getAllAwards:
                return nil
            case .userBuyAward(let data):
                return data
            case .getAllAwardsPerRestaurant(let data):
                return data
            }
        }()
        
        var urlRequest = URLRequest(url: url)
        
        switch self {
        case .getAllAwards, .userBuyAward ,.getAllAwardsPerRestaurant:
            let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
            urlRequest.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60

        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
