//
//  HomeManager.swift
//  Elusive
//
//  Created by Graphic on 4/2/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class HomeManager {
    
    static func getAllPlayers(body: (String, String), completion:  @escaping (_ :Players?,_ :Error?)->Void)
    {
        Alamofire.request(HomeRouter.getAllPlayers(body.0, body.1))
            .responseJSON {
                response in

                switch(response.result) {

                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = Players(JSON: data as! [String:Any])
                    completion(rep, nil)

                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func getAllSponsers(completion:  @escaping (_ :Sponsers?,_ :Error?)->Void)
    {
        Alamofire.request(HomeRouter.getAllSponsers())
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = Sponsers(JSON: data as! [String:Any])
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func getSponserInfo(body: String, completion:  @escaping (_ :SponserInfo?,_ :Error?)->Void)
    {
        Alamofire.request(HomeRouter.getSponserInfo(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = SponserInfo(JSON: data as! [String:Any])
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func getPlayerInfo(body: String, completion:  @escaping (_ :PlayerInfo?,_ :Error?)->Void)
    {
        Alamofire.request(HomeRouter.getPlayerInfo(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = PlayerInfo(JSON: data as! [String:Any])
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
}
