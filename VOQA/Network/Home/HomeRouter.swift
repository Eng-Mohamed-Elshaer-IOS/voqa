//
//  HomeRouter.swift
//  Elusive
//
//  Created by Graphic on 4/2/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum HomeRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl
    
    // Home
    case getAllPlayers(String, String)
    case getPlayerInfo(String)
    case getAllSponsers()
    case getSponserInfo(String)
    
    func asURLRequest() throws -> URLRequest {
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .getAllPlayers(let start, let end):
                relativePath = "\(Constants.URLs.getAllPlayers)\(start)/\(end)"
            case .getPlayerInfo(let id):
                relativePath = "\(Constants.URLs.getPlayerInfo)\(id)"
            case .getAllSponsers:
                relativePath = Constants.URLs.getAllSponsers
            case .getSponserInfo(let id):
                relativePath = "\(Constants.URLs.getSponserInfo)\(id)"
            }
            var url = URL(string: AuthRouter.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .getAllPlayers, .getAllSponsers, .getSponserInfo, .getPlayerInfo:
                return .get
            }
        }
        let params: ([String: Any]?) = {
            switch self {
            case .getAllPlayers, .getAllSponsers, .getSponserInfo, .getPlayerInfo:
                return nil
            }
        }()
        
        var urlRequest = URLRequest(url: url)
        
        switch self {
        case .getAllPlayers, .getAllSponsers, .getSponserInfo, .getPlayerInfo:
            let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
            urlRequest.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60

        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
