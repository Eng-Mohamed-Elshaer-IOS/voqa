//
//  Constants.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation

class Constants {
    struct Settings {
        static var lang = "EN"

    }
    struct Message {
        static var invalidToken = ""
        
    }
    struct URLs {
        // Auth
        // http://34.230.172.63/players_app/public/
        // https://bosat.4hoste.com/players_app/public/
        //http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public
        static var baseUrl = "http://ec2-34-230-172-63.compute-1.amazonaws.com/new/players_app/public/"
        //"http://34.230.172.63/players_app/public/"
        static let login = "api/login"
        static let register = "api/register"
        static let forgetPassword = "api/forget_password"
        static let createSocail = "api/social_auth/create"
        static let checkSocial = "api/social_auth/check"
        static let metaData = "api/meta_data"
        // Home
        static let getAllSponsers = "api/sponsors/all"
        static let getSponserInfo = "api/sponsors/"
        static let getAllPlayers = "api/players/all/"
        static let getPlayerInfo = "api/players/"
        // Awards
        static let getAllAwards = "/api/awards/fetch/"
        static let userBuyAward = "api/awards/buy"
        static let getAllAwardsByRestaurant = "/api/awards/restaurant"
        static let getAllCode = "/api/awards//allcode/"

        // News
        static let getAllNews = "/api/news/all"
        // User
        static let updateImage = "api/user/update/image"
        static let getUserRanks = "api/user/ranking"
        static let rankPrize = "api/prizes"
        // Matches
        static let getMatchesByDate = "api/match/"
        static let winTenPoints = "api/user/daily_win"
        static let expectMatch = "api/match/expect"
        static let getHistory = "/api/match/gethistory/from/"
        // Restaurants
        static let getAllRestaurants = "/api/restaurants/all"
        static let getRestaurantInfo = "/api/restaurants/find"
        static let getAllRestaurantsOrders = "/api/restaurants/orders/"
        static let rateRestaurant = "/api/restaurants/rate"
        static let restaurantDiscounts = "/api/awards/myrestaurantcodes"
        static let myPoints = "api/user/mypoints/"
        //"/api/restaurants/mycodes/"
        // Groups
        static let getAllGroups = "/api/groups/allgroups"
        static let editGroup = "/api/groups/edit"
        static let groupPart = "/api/groups/group_part"
        static let userInGroups = "/api/groups/user_in_groups"
        static let userRoles = "/api/groups/user_roles"
        static let deleteGroup = "/api/groups/delete"
        static let createGroup = "/api/groups/create"
        static let adminInGroups = "/api/groups/admin_in_groups"
        static let pendingInvitation = "/api/share/pending_invitations"
        static let searchUser = "/api/share/user_search"
        static let rejectInvitation = "/api/share/reject_group_invitation"
        static let acceptInvitation = "/api/share/accept_group_invitation"
        static let invites = "/api/share/group_invite"
        static let inviteUser = "/api/share/group_user_invite"
        static let membersInGroup = "/api/groups/user_list"
///api/share/group_user_array_invite
        static let inviteUsersToGtoup = "/api/share/group_user_array_invite"
        static let leaveGroup = "/api/share/leave_group"
        static let generateLink = "/api/share/group_url_invite"

    }
    
    struct headers {
        static let applicationTypeKey = "Content-Type"
        static let applicationTypeValue = "application/json"
        static let Authorization = "Authorization"
        static let page = "page"
        static var pageIndex = 1

        static let platformKey = "platform"
        static let platformValue = "ios"
    }
    
}
