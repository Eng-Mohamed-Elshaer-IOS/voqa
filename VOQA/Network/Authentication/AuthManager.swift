//
//  AuthManager.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class AuthManager {
    
    static func getMetaData(completion:  @escaping (_ :BackendResponse<Cities>?,_ :Error?)->Void)
    {
        Alamofire.request(AuthRouter.metaData())
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let metaResponse = Mapper<BackendResponse
                        <Cities>>().map(JSON: data as! [String:Any])
                    completion(metaResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func login(body: LoginBody, completion:  @escaping (_ :BackendResponse<User>?,_ :Error?)->Void)
    {
        Alamofire.request(AuthRouter.login((body.encodeToJSON())))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let loginResponse = Mapper<BackendResponse
                        <User>>().map(JSON: data as! [String:Any])
                    completion(loginResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func register(body: RegisterBody, completion:  @escaping (_ :BackendResponse<User>?,_ :Error?)->Void)
    {
        Alamofire.request(AuthRouter.register((body.encodeToJSON())))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
//                    if let token = tokenData["errors"]
//                    {
//                        Constants.Message.invalidToken = "Invalid Token"
//                        
//                        completion(nil, nil)
//                        return
//                    }
                    Constants.Message.invalidToken = ""

                    let registerResponse = Mapper<BackendResponse
                        <User>>().map(JSON: data as! [String:Any])
                    completion(registerResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func forgetPassword(body: ForgetPasswordBody, completion:  @escaping (_ :BackendResponse<User>?,_ :Error?)->Void)
    {
        Alamofire.request(AuthRouter.forgetPassword((body.encodeToJSON())))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let forgetPasswordResponse = Mapper<BackendResponse
                        <User>>().map(JSON: data as! [String:Any])
                    completion(forgetPasswordResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func createSocial(body: CreateSocialBody, completion:  @escaping (_ :BackendResponse<User>?,_ :Error?)->Void)
    {
        Alamofire.request(AuthRouter.createSocial((body.encodeToJSON())))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let forgetPasswordResponse = Mapper<BackendResponse
                        <User>>().map(JSON: data as! [String:Any])
                    completion(forgetPasswordResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func checkSocial(body: CheckSocialBody, completion:  @escaping (_ :BackendResponse<User>?,_ :Error?)->Void)
    {
        Alamofire.request(AuthRouter.checkSocial((body.encodeToJSON())))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let forgetPasswordResponse = Mapper<BackendResponse
                        <User>>().map(JSON: data as! [String:Any])
                    completion(forgetPasswordResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
}

