//
//  AuthRouter.swift
//  Elusive
//
//  Created by Graphic on 3/31/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum AuthRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl
    
    // Authentication
    case login([String: Any])
    case register([String: Any])
    case forgetPassword([String: Any])
    case createSocial([String: Any])
    case checkSocial([String: Any])
    case metaData()
    
    func asURLRequest() throws -> URLRequest {
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .login:
                relativePath = Constants.URLs.login
            case .register:
                relativePath = Constants.URLs.register
            case .forgetPassword:
                relativePath = Constants.URLs.forgetPassword
            case .createSocial:
                relativePath = Constants.URLs.createSocail
            case .checkSocial:
                relativePath = Constants.URLs.checkSocial
            case .metaData:
                relativePath = Constants.URLs.metaData
            }
            var url = URL(string: AuthRouter.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .login, .register, .forgetPassword, .createSocial, .checkSocial:
                return .post
            case .metaData:
                return .get
            }
        }
        let params: ([String: Any]?) = {
            switch self {
            case .login(let loginData):
                return (loginData)
            case .register(let registerData):
                return (registerData)
            case .forgetPassword(let forgetPasswordData):
                return (forgetPasswordData)
            case .createSocial(let data):
                return (data)
            case .checkSocial(let data):
                return (data)
            case .metaData:
                return nil
            }
        }()
        
        var urlRequest = URLRequest(url: url)
        
        switch self {
        case .login, .register, .forgetPassword, .createSocial, .checkSocial, .metaData:
            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60

        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
