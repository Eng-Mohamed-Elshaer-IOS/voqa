//
//  MatchesRouter.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum MatchesRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl
    
    // Home
    case getMatchesByDate(String)
    case winTenPoints()
    case expectMatch([String: Any])
    case getHistory(String)

    func asURLRequest() throws -> URLRequest {
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .getMatchesByDate(let date):
                let userId = (UserDefaults.standard.value(forKey: "UserID") as! NSNumber)
                let userIdStr = "\(userId)"
                relativePath = Constants.URLs.getMatchesByDate + date + "/" + date + "/" + userIdStr
            case .winTenPoints:
                relativePath = Constants.URLs.winTenPoints
            case .expectMatch:
                relativePath = Constants.URLs.expectMatch
            case .getHistory(let id):
                relativePath = "\(Constants.URLs.getHistory)\(id)"

            }
            var url = URL(string: MatchesRouter.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            print("GET MATCHES \(url)")

            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .getMatchesByDate, .winTenPoints:
                return .get
            case .expectMatch:
                return .post
            case .getHistory:
                return .get
            }
        }
        let params: ([String: Any]?) = {
            switch self {
            case .getMatchesByDate, .winTenPoints:
                return nil
            case .expectMatch(let data):
                return data
            case .getHistory:
                return nil
            }
        }()
        
        var urlRequest = URLRequest(url: url)
        
        switch self {
        case .getMatchesByDate, .winTenPoints, .expectMatch,.getHistory:
            let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)

         //   print("TOKENNNN \(token) \(Constants.)")
            urlRequest.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60
        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
