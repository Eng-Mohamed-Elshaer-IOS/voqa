//
//  MatchesManager.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON

class MatchesManager {
    static func getAllHistoryMatches( userId:String,completion:  @escaping (_ :MatchHistory?,_ :Error?)->Void)
    {
        Alamofire.request(MatchesRouter.getHistory(userId))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let matchesResponse = MatchHistory(JSON: data as! [String:Any])
                    completion(matchesResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    static func expectMatch(body: [String: Any], completion:  @escaping (_ :BackendResponse<Leagues>?,_ :Error?)->Void)
    {
        Alamofire.request(MatchesRouter.expectMatch(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let matchesResponse = Mapper<BackendResponse
                        <Leagues>>().map(JSON: data as! [String:Any])
                    completion(matchesResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func winTenPoints(completion:  @escaping (_ :BackendResponse<Leagues>?,_ :Error?)->Void)
    {
        Alamofire.request(MatchesRouter.winTenPoints())
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let matchesResponse = Mapper<BackendResponse
                        <Leagues>>().map(JSON: data as! [String:Any])
                    completion(matchesResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func getMatchesByDate(body: String, completion:  @escaping (_ :BackendResponse<Leagues>?,_ :Error?)->Void)
    {
//        let configuration = URLSessionConfiguration.default
//        configuration.timeoutIntervalForRequest = 10 // seconds
//        configuration.timeoutIntervalForResource = 10

        Alamofire.request(MatchesRouter.getMatchesByDate(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let matchesResponse = Mapper<BackendResponse
                        <Leagues>>().map(JSON: data as! [String:Any])
                    completion(matchesResponse, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
}
