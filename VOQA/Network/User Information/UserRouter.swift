//
//  UserRouter.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire

enum UserRouter: URLRequestConvertible {
    
    static let baseURL = Constants.URLs.baseUrl
    
    // Home
    case updateImage([String: Any])
    case getUserRanks()
    case getRankPrize()
    
    func asURLRequest() throws -> URLRequest {
        
        let url: URL = {
            // build up and return the URL for each endpoint
            let relativePath: String?
            switch self {
            case .updateImage:
                relativePath = Constants.URLs.updateImage
            case .getUserRanks:
                relativePath = Constants.URLs.getUserRanks
            case .getRankPrize:
                relativePath = Constants.URLs.rankPrize
            }
            var url = URL(string: UserRouter.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            return url
        }()
        
        var method: HTTPMethod {
            switch self {
            case .updateImage:
                return .post
            case .getUserRanks, .getRankPrize:
                return .get
            }
        }
        let params: ([String: Any]?) = {
            switch self {
            case .updateImage(let data):
                return data
            case .getUserRanks(), .getRankPrize:
                return nil
            }
        }()
        
        var urlRequest = URLRequest(url: url)
        
        switch self {
        case .updateImage, .getUserRanks, .getRankPrize:
            let token = "Bearer " + (UserDefaults.standard.value(forKey: "UserToken") as! String)
            urlRequest.addValue(token, forHTTPHeaderField: Constants.headers.Authorization)
            urlRequest.addValue(Constants.headers.applicationTypeValue, forHTTPHeaderField: Constants.headers.applicationTypeKey)
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 60

        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
