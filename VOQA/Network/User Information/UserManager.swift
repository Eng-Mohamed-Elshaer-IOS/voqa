//
//  UserManager.swift
//  Elusive
//
//  Created by Graphic on 4/22/18.
//  Copyright © 2018 KarimEbrahem. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON

class UserManager {
    
    static func getRankPrize(completion:  @escaping (_ :String?,_ :Error?)->Void)
    {
        Alamofire.request(UserRouter.getRankPrize())
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let json = JSON(data)
                    let rep = json["data"]["prizes"].string
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func updateUserImage(body: [String: Any], completion:  @escaping (_ :(Bool?, String?)?,_ :Error?)->Void)
    {
        Alamofire.request(UserRouter.updateImage(body))
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let json = JSON(data)
                    let status = json["status"]["code"].int
                    if status == 200 {
                        completion((true, json["data"]["imageUrl"].string!), nil)
                    } else {
                        completion((false, nil), nil)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
    static func getUserRanks(completion:  @escaping (_ :Rankings?,_ :Error?)->Void)
    {
        Alamofire.request(UserRouter.getUserRanks())
            .responseJSON {
                response in
                
                switch(response.result) {
                    
                case .success(let data):
                    //print("Data \(data)")
                    var tokenData = data as! [String:Any]
                    
                    if let token = tokenData["errors"]
                    {
                        Constants.Message.invalidToken = "Invalid Token"
                        
                        completion(nil, nil)
                        return
                    }
                    Constants.Message.invalidToken = ""

                    let rep = Rankings(JSON: data as! [String:Any])
                    completion(rep, nil)
                    
                case .failure(let error):
                    completion(nil, error)
                }
        }
    }
    
}
